QLibrary
========
General:
Windows SDK Version: 10.0.17763.0 or Latest 10.0
Output Directory: $(SolutionDir)$(Platform)\$(Configuration)\
Intermediate Directory: $(Platform)\$(Configuration)\
Platform Toolset: Visual Studio 2019 (v142)
Configuration Type: Static library (.lib)
Character Set: Use Unicode Character Set

C/C++ General:
Additional Include Directories: $(SolutionDir)Inc\AssImpRev5

Game
====
General:
Windows SDK Version: 10.0.17763.0 or Latest 10.0
Output Directory: $(SolutionDir)$(Platform)\$(Configuration)\
Intermediate Directory: $(Platform)\$(Configuration)\
Platform Toolset: Visual Studio 2019 (v142)
Configuration Type: Application (.exe)
Character Set: Use Unicode Character Set

C/C++ General:
Additional Include Directories: $(SolutionDir)QLibrary;$(SolutionDir)inc\AssImpRev5
Debug Information Format: Program Database (/Zi)

Linker General:
Additional Library Directories: $(SolutionDir)$(Platform)\$(Configuration);$(SolutionDir)Lib\AssImp\x64


#pragma comment(lib, "d3dcompiler.lib")

#pragma comment(lib, "D3D12.lib")

#pragma comment(lib, "QLibrary.lib")

#pragma comment(lib, "assimp-vc141-mtd.lib")



