// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_MESH_QLIBRARY_GEOMETRY_H_
#define FLATBUFFERS_GENERATED_MESH_QLIBRARY_GEOMETRY_H_

#include "flatbuffers/flatbuffers.h"

#include "base_types_generated.h"

namespace QLibrary {
namespace Geometry {

struct SkinnedVertex;

struct SkinnedMesh;
struct SkinnedMeshBuilder;

FLATBUFFERS_MANUALLY_ALIGNED_STRUCT(4) SkinnedVertex FLATBUFFERS_FINAL_CLASS {
 private:
  QLibrary::Geometry::Point3 position_;
  QLibrary::Geometry::UV uv_;
  QLibrary::Geometry::Vector3 normal_;
  QLibrary::Geometry::Vector3 tangent_;
  QLibrary::Geometry::Weight4 weights_;
  QLibrary::Geometry::Byte4 indices_;

 public:
  SkinnedVertex()
      : position_(),
        uv_(),
        normal_(),
        tangent_(),
        weights_(),
        indices_() {
  }
  SkinnedVertex(const QLibrary::Geometry::Point3 &_position, const QLibrary::Geometry::UV &_uv, const QLibrary::Geometry::Vector3 &_normal, const QLibrary::Geometry::Vector3 &_tangent, const QLibrary::Geometry::Weight4 &_weights, const QLibrary::Geometry::Byte4 &_indices)
      : position_(_position),
        uv_(_uv),
        normal_(_normal),
        tangent_(_tangent),
        weights_(_weights),
        indices_(_indices) {
  }
  const QLibrary::Geometry::Point3 &position() const {
    return position_;
  }
  const QLibrary::Geometry::UV &uv() const {
    return uv_;
  }
  const QLibrary::Geometry::Vector3 &normal() const {
    return normal_;
  }
  const QLibrary::Geometry::Vector3 &tangent() const {
    return tangent_;
  }
  const QLibrary::Geometry::Weight4 &weights() const {
    return weights_;
  }
  const QLibrary::Geometry::Byte4 &indices() const {
    return indices_;
  }
};
FLATBUFFERS_STRUCT_END(SkinnedVertex, 64);

struct SkinnedMesh FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef SkinnedMeshBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_VERTICES = 4,
    VT_TRIANGLES = 6
  };
  const flatbuffers::Vector<const QLibrary::Geometry::SkinnedVertex *> *vertices() const {
    return GetPointer<const flatbuffers::Vector<const QLibrary::Geometry::SkinnedVertex *> *>(VT_VERTICES);
  }
  const flatbuffers::Vector<const QLibrary::Geometry::Triangle *> *triangles() const {
    return GetPointer<const flatbuffers::Vector<const QLibrary::Geometry::Triangle *> *>(VT_TRIANGLES);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_VERTICES) &&
           verifier.VerifyVector(vertices()) &&
           VerifyOffset(verifier, VT_TRIANGLES) &&
           verifier.VerifyVector(triangles()) &&
           verifier.EndTable();
  }
};

struct SkinnedMeshBuilder {
  typedef SkinnedMesh Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_vertices(flatbuffers::Offset<flatbuffers::Vector<const QLibrary::Geometry::SkinnedVertex *>> vertices) {
    fbb_.AddOffset(SkinnedMesh::VT_VERTICES, vertices);
  }
  void add_triangles(flatbuffers::Offset<flatbuffers::Vector<const QLibrary::Geometry::Triangle *>> triangles) {
    fbb_.AddOffset(SkinnedMesh::VT_TRIANGLES, triangles);
  }
  explicit SkinnedMeshBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<SkinnedMesh> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<SkinnedMesh>(end);
    return o;
  }
};

inline flatbuffers::Offset<SkinnedMesh> CreateSkinnedMesh(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::Vector<const QLibrary::Geometry::SkinnedVertex *>> vertices = 0,
    flatbuffers::Offset<flatbuffers::Vector<const QLibrary::Geometry::Triangle *>> triangles = 0) {
  SkinnedMeshBuilder builder_(_fbb);
  builder_.add_triangles(triangles);
  builder_.add_vertices(vertices);
  return builder_.Finish();
}

inline flatbuffers::Offset<SkinnedMesh> CreateSkinnedMeshDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const std::vector<QLibrary::Geometry::SkinnedVertex> *vertices = nullptr,
    const std::vector<QLibrary::Geometry::Triangle> *triangles = nullptr) {
  auto vertices__ = vertices ? _fbb.CreateVectorOfStructs<QLibrary::Geometry::SkinnedVertex>(*vertices) : 0;
  auto triangles__ = triangles ? _fbb.CreateVectorOfStructs<QLibrary::Geometry::Triangle>(*triangles) : 0;
  return QLibrary::Geometry::CreateSkinnedMesh(
      _fbb,
      vertices__,
      triangles__);
}

inline const QLibrary::Geometry::SkinnedMesh *GetSkinnedMesh(const void *buf) {
  return flatbuffers::GetRoot<QLibrary::Geometry::SkinnedMesh>(buf);
}

inline const QLibrary::Geometry::SkinnedMesh *GetSizePrefixedSkinnedMesh(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<QLibrary::Geometry::SkinnedMesh>(buf);
}

inline bool VerifySkinnedMeshBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<QLibrary::Geometry::SkinnedMesh>(nullptr);
}

inline bool VerifySizePrefixedSkinnedMeshBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<QLibrary::Geometry::SkinnedMesh>(nullptr);
}

inline void FinishSkinnedMeshBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<QLibrary::Geometry::SkinnedMesh> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedSkinnedMeshBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<QLibrary::Geometry::SkinnedMesh> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Geometry
}  // namespace QLibrary

#endif  // FLATBUFFERS_GENERATED_MESH_QLIBRARY_GEOMETRY_H_
