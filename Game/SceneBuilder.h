#pragma once

#include <d3d12.h>
#include <wrl.h>
#include <string>
#include <vector>
#include <memory>
#include <cstdint>
#include <fstream>
#include <DirectXMath.h>
#include <TGATextureLoader.h>

#include "FrameResource.h"
#include "RenderItem.h"
#include "ModelData.h"


// ASSIMP min/max macros used by assimp conflict with like-named member functions in Material.inl so do this before importing Importer.hpp
#if defined(min)
#undef min
#endif
#if defined(max)
#undef max
#endif

#include <assimp/config.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/DefaultLogger.hpp>
#include <assimp/postprocess.h>

#include "Common.h"
#include "GameObject.h"
#include "Player.h"
#include "GpuWaves.h"

class SceneBuilder
{
public:
	SceneBuilder(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCmdList, CD3DX12_CPU_DESCRIPTOR_HANDLE hDesc, CD3DX12_GPU_DESCRIPTOR_HANDLE hVid, UINT descSize, UINT descriptorCount ); 
	~SceneBuilder();

	bool				LoadGeometryResources( const std::wstring& textFile, GameObject* pObj, Player& player );
	bool				LoadSceneModels( const std::wstring& textFile, Player& player );
	UINT				LoadSkyCube( const std::wstring& textFile);
	UINT				LoadWaterWaves(const std::wstring& textFile );
	
	const std::vector<RenderItem*>									  GetRenderItems( RenderLayer layer )	const { return m_RenderItems[(int)layer];  }
	const std::vector<std::unique_ptr<RenderItem>>&					  GetAllRenderItems()					const { return m_AllRenderItems; }
	const std::unordered_map<std::string, std::unique_ptr<Material>>& GetMaterials()						const { return m_Materials; }
	const std::unordered_map<std::string, std::unique_ptr<Texture>>&  GetTextures()							const {	return m_Textures; }
	std::unique_ptr<SkinnedModelInstance>&							  GetSkinnedModelInstance()         { return m_SkinnedModelInst; }
	Material*														  GetMaterial( const std::string s) { return m_Materials[s].get(); }
	GpuWaves*														  GetWaterWaves()                   { return m_Waves.get(); }
	 
private:
	void				BuildTextureResource(const char* name, const char* fileName, TextureType type = TextureType::Tex);
	void				BuildUAVWaveDescriptors();
	void				UploadResourceData(Texture* pTex, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs);
	
	void				BuildStaticRenderObjects(std::string modelName, std::vector<StaticMesh>& meshes, Player& player );
	void			    AddRenderItem(const std::string& geoName, const std::string& drawArgsName, const std::string& materialName, DirectX::XMMATRIX& objScale, DirectX::XMMATRIX& objRot, DirectX::XMMATRIX& objPos, UINT alphaFumc = TexturedNoClip, float tileU = 1.0f, float tileV = 1.0f);
	void				AddMaterial(const std::string& matName, Mat& materialProperties, UINT diffSlot, UINT bumpSlot );
	
	void				BuildSkinnedModelGeometry( std::string modelName, std::vector<SkinnedMesh>& meshes);
	void				BuildSkinnedRenderObjects( std::string modelName, std::vector<SkinnedMesh>& meshes,  Player& player );
	void				ProcessAnimationClips( std::string modelName );


	// Models ASSIMP
	bool				LoadStaticQlblModel(const std::string& fileDir, const std::string& filePath, std::vector<StaticMesh>& meshes, const std::string& theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool isClipped);
	bool				LoadStaticAssImpModel(std::string filePath, std::vector<StaticMesh>& meshes, std::string theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool isClipped);
	void				ProcessStaticNode(aiNode* node, std::vector<StaticMesh>& meshes, std::string theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool alphaClipped);
	void				ProcessStaticMesh(UINT meshId, aiMesh* pMesh, StaticMesh& theMesh);
	std::string			LoadMaterialTextures(UINT meshId, aiMaterial* mat, UINT& textureCount, bool& isTransparent);
	bool				BuildMeshMaterial(std::string matName, aiMaterial* pMat, Material* pOurMat, bool hasDiff, bool hasBump, bool hasSpecular = false);
	bool				BuildMeshSharedMaterial(std::string matName, aiMaterial* pMat, Material* pOurMat, UINT sharedTextureIndex);
	std::string			DetermineTextureType(aiMaterial* mat);
	void				CreateTextureFromModel(int textureindex);
	int					GetTextureIndex(aiString* str);
	void				ConvertMatrix4x4(DirectX::XMFLOAT4X4& ourMatrix, aiMatrix4x4& theirMatrix);
	int					FormatFromFilename(const char* filename);
	bool                TextureExists(std::string fileName);

	// Skinned
	bool				LoadSkinnedAssImpModel( std::string filePath, std::vector<SkinnedMesh>& meshes, std::string theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool isClipped );
	void				BuildSceneGlobalTransforms();
	void				BuildJoints(aiMesh* aMesh, SkinnedMesh* mesh);
	void				BuildSceneNodes(aiNode* sceneRoot);
	void				BuildSceneHierarhcy(aiNode* sceneRoot, int32_t parentIndex);
	void				BuildSceneHierarhcy(aiNode* sceneRoot, int32_t parentIndex, int32_t* childIndex);
	void				ProcessSkinnedNode( aiNode* pNode, std::vector<SkinnedMesh>& meshes, std::string theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool alphaClipped );
	void				ProcessSkinnedMesh( UINT meshIndex, aiMesh* aMesh, SkinnedMesh& mesh );

	// Geometry helpers
	void				BuildWallsWithWindows(float xPos, UINT rightCount, UINT southCount, float w, std::string wallTexfile, std::string bumpFile, std::string windowTexfile, float transparency);
	void				BuildFloor(float posX, float width, float depth, std::string texFile, std::string bumpFile);
	void				BuildCeiling(float posX, float height, float width, float depth, std::string texFile, std::string bumpFile);
	void				BuildQuad(const std::string& geoName, const std::string& type, float x, float y, float wide, float high, float depth);
	void				BuildGrid(const std::string& geoName, const std::string& type, float width, float depth, UINT m, UINT n);
	void				BuildBox(const std::string& geoName, const std::string& type, float width, float height, float depth, UINT subdivisions);
	void				BuildSphere(const std::string& geoName, float radius, UINT sliceCount, UINT stackCount);
	void				BuildCylinder(const std::string& geoName, const std::string& type, float bottomRadius, float topRadius, float height, UINT sliceCount, UINT stackCount);

	// Water helpers
	void				BuildLandGeometry();
	void				BuildWavesGeometry( std::string geoName,float width, float depth );

	// Fake terrain 
	float					GetHillsHeight(float x, float z) const;
	DirectX::XMFLOAT3		GetHillsNormal(float x, float z) const;

public:
		std::unordered_map<std::string, std::unique_ptr<Material>>	m_Materials;
		std::vector<RenderItem*>									m_RenderItems[(int)RenderLayer::Count];
		std::vector<std::unique_ptr<RenderItem>>					m_AllRenderItems;
		
private:
	ID3D12Device*				    m_pDevice;
	ID3D12GraphicsCommandList*	    m_pCommandList;
	CD3DX12_CPU_DESCRIPTOR_HANDLE   m_hCpu;
	CD3DX12_GPU_DESCRIPTOR_HANDLE   m_hVid;
	D3D12_SHADER_RESOURCE_VIEW_DESC m_srvDesc = {};
	TGATextureLoader*               m_pTGALoader = nullptr;
	UINT							m_MaterialCBIndex = 0;
	UINT							m_ObjectCBIndex = 0;
	UINT							m_NextSrvHeapSlot = 0;
	UINT							m_MaxDescriptors;
	UINT						    m_SizeOfDescriptors;
	std::string						m_LocalPath;

	const aiScene*			m_pAiScene;
	std::vector<aiNode*>	m_Scene; // Scene nodes, including bones
	UINT					m_AiNode = 0;
	std::string				m_TexStr;

	std::unordered_map<std::string, std::unique_ptr<MeshGeometry>> m_Geometries;
	std::unordered_map<std::string, std::unique_ptr<Texture>>	   m_Textures;
	std::unordered_map<std::string, UINT>                          m_SrvDebugHeapSlots;

	// Skinned
	std::unordered_map<std::string, UINT>			m_SceneNodes;
	std::vector<int>				 				m_SceneHierarchy;
	std::vector<DirectX::XMFLOAT4X4>				m_SceneLocalTransforms;
	std::vector<DirectX::XMFLOAT4X4>				m_SceneGlobalTransforms;
	Skeleton										m_skeleton;
	SkeletonLoadingData								m_skeletonLoading;//valid only during loading.

	std::unique_ptr<SkinnedModelInstance>			m_SkinnedModelInst = nullptr;
	SkinnedData										m_SkinnedInfo;
	std::unordered_map<std::string, AnimationClip>	m_AnimationClips;
	std::vector<std::string>						m_ClipNames;
	float											m_TicksPerSecond = 25.0f;

	// water
	std::unique_ptr<GpuWaves> m_Waves;
};





















