#pragma once

#include "RenderItem.h"

#define WORLD_RIGHT XMVectorSet(1,0,0,1)
#define WORLD_UP XMVectorSet(0,1,0,1)
#define WORLD_FORWARDS XMVectorSet(0,0,1,1)

class Player
{
public:
						Player();
						~Player();

	void				AssignRenderItem( RenderItem* pPlayerRenderItem );
	void				SetOriginalScale( DirectX::XMFLOAT3& scale );
	void				WalkFacing ( float d, DirectX::XMVECTOR& targetLook );
	void				LookStrafe( DirectX::XMVECTOR& look,  const DirectX::XMVECTOR& direction, float d );
	void				UpdateWorld( DirectX::XMFLOAT3& scale, DirectX::XMFLOAT3& position, DirectX::XMFLOAT4& rotationQuat );

	DirectX::XMVECTOR	GetPosition() const;	
		
private:
	//RenderItem*			m_PlayerRitem = nullptr;
	std::vector<RenderItem*> m_PlayerRitems;
	DirectX::XMFLOAT3   m_OriginalScale = { 1, 1, 1 };
	DirectX::XMFLOAT4X4 m_PlayerWorld = Maths::Identity4x4();
	DirectX::XMFLOAT3	m_Position	= { 0.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3	m_Velocity	= { 0.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3	m_Right		= { 1.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3	m_Up		= { 0.0f, 1.0f, 0.0f };
	DirectX::XMFLOAT3	m_Look		= { 0.0f, 0.0f, 1.0f };
};