#pragma once

#undef ENABLE_ASSIMP_LOGGER
#undef USE_BITANGENTS_FOR_STATIC_MODELS

#define TEXTURE_PATH_WIDE L"../Assets/Textures/"
#define MODEL_PATH_WIDE L"../Assets/Models/"
#define MODEL_PATH "../Assets/Models/"
#define PLAYER_HEIGHT 2.0f
#define CAM_WALK_SPEED 9.0f
#define MEDIEVAL_MODELS_BUMPED true

const int gNumColorPresets = 13;

const std::string WATER_GEO = "waterGeo";
const std::string WATER_MAT = "waterMat";
const std::string SKY_GEO = "skyGeo";
const std::string SKY_MAT = "skyMat";
const std::string LAND_GEO = "landGeo";


static wstring ColourNames[gNumColorPresets] = 
{
	L"White",
	L"LemonChiffon",
	L"LightYellow",
	L"Wheat",
	L"PaleGoldenrod",
	L"PeachPuff",
	L"Goldenrod",
	L"Coral",
	L"IndianRed",
	L"RoyalBlue",
	L"PowderBlue",
	L"DimGray",
	L"Black"
};

static XMFLOAT3 ColourPresets[gNumColorPresets] =
{
	XMFLOAT3(Colors::White),
	XMFLOAT3(Colors::LemonChiffon),
	XMFLOAT3(Colors::LightYellow),
	XMFLOAT3(Colors::Wheat),
	XMFLOAT3(Colors::PaleGoldenrod),
	XMFLOAT3(Colors::PeachPuff),
	XMFLOAT3(Colors::Goldenrod),
	XMFLOAT3(Colors::Coral),
	XMFLOAT3(Colors::IndianRed),
	XMFLOAT3(Colors::RoyalBlue),
	XMFLOAT3(Colors::PowderBlue),
	XMFLOAT3(Colors::DimGray),
	XMFLOAT3(Colors::Black)
};

static unordered_map< wstring, XMFLOAT3 > ColourMap =
{
	{ ColourNames[0], XMFLOAT3(Colors::White) },
	{ ColourNames[1], XMFLOAT3(Colors::LemonChiffon) },
	{ ColourNames[2], XMFLOAT3(Colors::LightYellow) },
	{ ColourNames[3], XMFLOAT3(Colors::Wheat) },
	{ ColourNames[4], XMFLOAT3(Colors::PaleGoldenrod) },
	{ ColourNames[5], XMFLOAT3(Colors::PeachPuff) },
	{ ColourNames[6], XMFLOAT3(Colors::Goldenrod) },
	{ ColourNames[7], XMFLOAT3(Colors::Coral) },
	{ ColourNames[8], XMFLOAT3(Colors::IndianRed) },
	{ ColourNames[9], XMFLOAT3(Colors::RoyalBlue) },
	{ ColourNames[10], XMFLOAT3(Colors::PowderBlue) },
	{ ColourNames[11], XMFLOAT3(Colors::DimGray) },
	{ ColourNames[12], XMFLOAT3(Colors::Black) }
};
