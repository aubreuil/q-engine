#pragma once

#include <DirectXMath.h>

struct GameObject
{
	GameObject(std::string name)
	{
		m_Name = name;
		m_CBIndex = 0;
		m_Pos = DirectX::XMVectorSet(0, 0, 0, 0);
		m_Rot = DirectX::XMVectorSet(0, 0, 0, 1.0f);
		m_TargetPos = DirectX::XMFLOAT3(0, 0, 0);
		m_TargetRot = DirectX::XMFLOAT3(0, 0, 0);
		
		m_IsIdle = true;
	}
	GameObject( std::string name, DirectX::XMVECTOR pos, DirectX::XMVECTOR rot )
	{
		m_Name = name;
		m_CBIndex = 0;
		m_Pos = pos;
		m_Rot = rot;
		m_TargetPos = DirectX::XMFLOAT3(0, 8.2f, 0);
		m_TargetRot = DirectX::XMFLOAT3(0, 0, 0);
		m_IsIdle = true;
	}
	~GameObject()
	{
	};

	std::string              m_Name;
	UINT					 m_CBIndex;
	DirectX::XMVECTOR		 m_Pos;
	DirectX::XMVECTOR	 	 m_Rot;
	DirectX::XMFLOAT3		 m_TargetPos;
	DirectX::XMFLOAT3		 m_TargetRot;
	bool					 m_IsIdle;
};