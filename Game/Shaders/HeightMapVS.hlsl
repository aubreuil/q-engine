#include "Common.hlsli"

struct VertexIn
{
	float3 PosL        : POSITION;
    float3 NormalL     : NORMAL;
	float2 TexC        : TEXCOORD;
};

struct VertexOut
{
	float4 PosH     : SV_POSITION;
    float3 PosW     : POSITION;
    float3 NormalW  : NORMAL;
	float2 TexC     : TEXCOORD;
};

// Vertex
VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;
	
	MaterialData matData = gMaterialData[gMaterialIndex];		// Fetch the material data

 	// Sample the displacement map using non-transformed [0,1]^2 tex-coords.
	int index = matData.HeightMapIndex; // TODO
	vin.PosL.y += matData.HeightFactor * gTextureMaps[index].SampleLevel( gsamLinearWrap, vin.TexC, 1.0f ).r;

	//// Estimate normal using finite difference.
	float du = gDisplacementMapTexelSize.x;
	float dv = gDisplacementMapTexelSize.y;
	float l = gTextureMaps[index].SampleLevel( gsamPointClamp, vin.TexC-float2(du, 0.0f), 0.0f ).r;
	float r = gTextureMaps[index].SampleLevel( gsamPointClamp, vin.TexC - float2(du, 0.0f), 0.0f ).r;
	float t = gTextureMaps[index].SampleLevel( gsamPointClamp, vin.TexC-float2(0.0f, dv), 0.0f ).r;
	float b = gTextureMaps[index].SampleLevel( gsamPointClamp, vin.TexC+float2(0.0f, dv), 0.0f ).r;
	vin.NormalL = normalize( float3(-r + l, 2.0f * gGridSpatialStep, b - t) );

    float4 posW = mul( float4( vin.PosL, 1.0f), gWorld );		// Transform to world space
    vout.PosW = posW.xyz;
    
    vout.NormalW = mul( vin.NormalL, (float3x3)gWorld );		// Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix
	

    vout.PosH = mul( posW, gViewProj );						    // Transform to homogeneous clip space
	
	float4 texC = mul( float4( vin.TexC, 0.0f, 1.0f ), gTexTransform );		// Output vertex attributes for interpolation across triangle
	vout.TexC = mul( texC, matData.MatTransform ).xy;
	
    return vout;
}
