#include "Common.hlsli"

struct VertexIn
{
	float3 PosL        : POSITION;
    float3 NormalL     : NORMAL;
	float2 TexC        : TEXCOORD;
};

struct VertexOut
{
	float4 PosH     : SV_POSITION;
    float3 PosW     : POSITION;
    float3 NormalW  : NORMAL;
	float2 TexC     : TEXCOORD;
};

// Vertex
VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;

	MaterialData matData = gMaterialData[gMaterialIndex];	

#ifdef DISPLACEMENT_MAP

	// Sample the displacement map using non-transformed [0,1]^2 tex-coords.
	vin.PosL.y +=  matData.HeightFactor * gDisplacementMap.SampleLevel(gsamLinearWrap, vin.TexC, 1.0f).r;
	
	// Estimate normal using finite difference.
	float du = gDisplacementMapTexelSize.x;
	float dv = gDisplacementMapTexelSize.y;
	float l = gDisplacementMap.SampleLevel( gsamPointClamp, vin.TexC-float2(du, 0.0f), 0.0f ).r;
	float r = gDisplacementMap.SampleLevel( gsamPointClamp, vin.TexC+float2(du, 0.0f), 0.0f ).r;
	float t = gDisplacementMap.SampleLevel( gsamPointClamp, vin.TexC-float2(0.0f, dv), 0.0f ).r;
	float b = gDisplacementMap.SampleLevel( gsamPointClamp, vin.TexC+float2(0.0f, dv), 0.0f ).r;
	vin.NormalL = normalize( float3(-r+l, 2.0f*gGridSpatialStep, b-t) );
	
#endif

	float4 posW = mul( float4( vin.PosL, 1.0f), gWorld );		// Transform to world space
    vout.PosW = posW.xyz;
    
    vout.NormalW = mul( vin.NormalL, (float3x3)gWorld );		// Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix
	

    vout.PosH = mul( posW, gViewProj );						    // Transform to homogeneous clip space
	
	float4 texC = mul( float4( vin.TexC, 0.0f, 1.0f ), gTexTransform );		// Output vertex attributes for interpolation across triangle
		
	vout.TexC = mul( texC, matData.MatTransform ).xy;
	
    return vout;
}

// Pixel
float4 PS(VertexOut pin) : SV_Target
{
	MaterialData matData = gMaterialData[gMaterialIndex];		// Fetch the material data
	float4 diffuseAlbedo = matData.DiffuseAlbedo;
	float3 fresnelR0 = matData.FresnelR0;
	float  roughness = matData.Roughness;
	uint diffuseMapIndex = matData.DiffuseMapIndex;
		
	diffuseAlbedo *= gTextureMaps[diffuseMapIndex].Sample( gsamAnisotropicWrap, pin.TexC );	// Dynamically look up the texture in the array

#ifdef ALPHA_TEST					// Discard pixel if texture alpha < 0.1.  We do this test as soon as possible in the shader so that 
		clip( diffuseAlbedo.a - 0.1f) ; // we can potentially exit the shader early, thereby skipping the rest of the shader code
#endif

    pin.NormalW = normalize( pin.NormalW );			// Interpolating normal can unnormalize it, so renormalize it

	float3 toEyeW = gEyePosW - pin.PosW;	// Vector from point being lit to eye
	float distToEye = length(toEyeW);		// distance to eye for FOG calculation		 
	toEyeW /= distToEye;					// normalize it

    float4 ambient = gAmbientLight * diffuseAlbedo;			// Lighting terms...

    const float shininess = 1.0f - roughness;
    Material mat = { diffuseAlbedo, fresnelR0, shininess };

	float3 shadowFactor = SHADOW_FACTOR;
	float4 directLight = ComputeLighting( gLights, mat, pin.PosW, pin.NormalW, toEyeW, shadowFactor );

    float4 litColor = ambient + directLight;
	
#ifdef SPECULAR_REFLECTIONS
	float3 r = reflect(-toEyeW, pin.NormalW);				// Add in specular reflections
	float4 reflectionColor = gCubeMap.Sample( gsamLinearWrap, r );
	float3 fresnelFactor = SchlickFresnel( fresnelR0, pin.NormalW, r );
    litColor.rgb += shininess * fresnelFactor * reflectionColor.rgb;
#endif
	
#ifdef FOG
	float fogAmount = saturate((distToEye - gFogStart) / gFogRange);
	litColor = lerp(litColor, gFogColor, fogAmount);
#endif

    litColor.a = diffuseAlbedo.a;			// Common convention to take alpha from diffuse albedo

    return litColor;
}
