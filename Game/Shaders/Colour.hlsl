#include "Common.hlsli"

struct VertexIn
{
	float3 PosL        : POSITION;
    float3 NormalL     : NORMAL;
};

struct VertexOut
{
	float4 PosH     : SV_POSITION;
    float3 PosW     : POSITION;
    float3 NormalW  : NORMAL;
};


// Vertex
VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;
	
	float4 posW = mul( float4( vin.PosL, 1.0f), gWorld );		// Transform to world space
    vout.PosW = posW.xyz;
    
    vout.NormalW = mul( vin.NormalL, (float3x3)gWorld );		// Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix
	

    vout.PosH = mul( posW, gViewProj );						    // Transform to homogeneous clip space
	
	return vout;
}

// Pixel
float4 PS(VertexOut pin) : SV_Target
{
	MaterialData matData = gMaterialData[gMaterialIndex];		// Fetch the material data
	float4 diffuseAlbedo = matData.DiffuseAlbedo;
	float3 fresnelR0 = matData.FresnelR0;
	float  roughness = matData.Roughness;

    pin.NormalW = normalize( pin.NormalW );			// Interpolating normal can unnormalize it, so renormalize it

	float3 toEyeW = normalize(gEyePosW - pin.PosW);

	float4 ambient = gAmbientLight * diffuseAlbedo;			// Lighting terms...

    const float shininess = 1.0f - roughness;
    Material mat = { diffuseAlbedo, fresnelR0, shininess };

	float3 shadowFactor = SHADOW_FACTOR;
	float4 directLight = ComputeLighting( gLights, mat, pin.PosW, pin.NormalW, toEyeW, shadowFactor );

    float4 litColor = ambient + directLight;
	
    litColor.a = diffuseAlbedo.a;			// Common convention to take alpha from diffuse albedo

    return litColor;
}

