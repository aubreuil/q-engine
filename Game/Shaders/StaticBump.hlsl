#include "Common.hlsli"

struct VertexIn
{
	float3 PosL        : POSITION;
    float3 NormalL     : NORMAL;
	float2 TexC        : TEXCOORD;
	float3 TangentL    : TANGENT;
};

struct VertexOut
{
	float4 PosH     : SV_POSITION;
    float3 PosW     : POSITION;
    float3 NormalW  : NORMAL;
	float2 TexC     : TEXCOORD;
	float3 TangentW : TANGENT;
};

float checker_board_pattern(float2 uv)
{
	float2 c = floor(uv) / 2;
	return frac(c.x + c.y) * 2;
}

float4 checker_board(float2 uv)
{
	float2 uv_scaled = uv * float2(32.0f, 32.0f);
	float checker = checker_board_pattern(uv_scaled);
	return float4(checker, checker, checker, 1.0f);
}

// Vertex
VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;
	
	MaterialData matData = gMaterialData[gMaterialIndex];		// Fetch the material data

    float4 posW = mul( float4( vin.PosL, 1.0f), gWorld );		// Transform to world space
    vout.PosW = posW.xyz;
    
    vout.NormalW = mul( vin.NormalL, (float3x3)gWorld );		// Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix
	vout.TangentW = mul( vin.TangentL, (float3x3)gWorld );		// World Tangent required for Normal Mapping

    vout.PosH = mul( posW, gViewProj );						    // Transform to homogeneous clip space
	
	float4 texC = mul( float4( vin.TexC, 0.0f, 1.0f ), gTexTransform );		// Output vertex attributes for interpolation across triangle
	vout.TexC = mul( texC, matData.MatTransform ).xy;
	
    return vout;
}

// Pixel
float4 PS(VertexOut pin) : SV_Target
{
	MaterialData matData = gMaterialData[gMaterialIndex];		// Fetch the material data
	float4 diffuseAlbedo = matData.DiffuseAlbedo;
	float3 fresnelR0 = matData.FresnelR0;
	float  roughness = matData.Roughness;
	uint diffuseMapIndex = matData.DiffuseMapIndex;
	uint normalMapIndex = matData.NormalMapIndex;
	
	diffuseAlbedo *= gTextureMaps[diffuseMapIndex].Sample( gsamAnisotropicWrap, pin.TexC );	// Dynamically look up the texture in the array

#ifdef ALPHA_TEST					// Discard pixel if texture alpha < 0.1.  We do this test as soon as possible in the shader so that 
		clip(diffuseAlbedo.a - 0.1f) ; // we can potentially exit the shader early, thereby skipping the rest of the shader code
#endif

    pin.NormalW = normalize( pin.NormalW );			// Interpolating normal can unnormalize it, so renormalize it

	float4 normalMapSample = gTextureMaps[normalMapIndex].Sample( gsamAnisotropicWrap, pin.TexC );
	float3 bumpedNormalW = NormalSampleToWorldSpace( normalMapSample.rgb, pin.NormalW, pin.TangentW );

	float3 toEyeW = gEyePosW - pin.PosW;	// Vector from point being lit to eye
	float distToEye = length( toEyeW );		// distance to eye for FOG calculation		 
	toEyeW /= distToEye;					// normalize it

    float4 ambient = gAmbientLight * diffuseAlbedo;			// Lighting terms...

    const float shininess = (1.0f - roughness) * normalMapSample.a;
    Material mat = { diffuseAlbedo, fresnelR0, shininess };

	float3 shadowFactor = SHADOW_FACTOR;
	float4 directLight = ComputeLighting( gLights, mat, pin.PosW, bumpedNormalW, toEyeW, shadowFactor );

    float4 litColor = ambient + directLight;
	
#ifdef SPECULAR_REFLECTIONS
	float3 r = reflect( -toEyeW, bumpedNormalW );				// Add in specular reflections
	float4 reflectionColor = gCubeMap.Sample( gsamLinearWrap, r );
	float3 fresnelFactor = SchlickFresnel( fresnelR0, bumpedNormalW, r );
    litColor.rgb += shininess * fresnelFactor * reflectionColor.rgb;
#endif
	
#ifdef FOG
	float fogAmount = saturate( (distToEye - gFogStart) / gFogRange );
	litColor = lerp( litColor, gFogColor, fogAmount );
#endif

    litColor.a = diffuseAlbedo.a;			// Common convention to take alpha from diffuse albedo

    return litColor;
}
