#include "Lighting.hlsli"	

#define DESCRIPTOR_COUNT 48
#define SHADOW_FACTOR 1.0f

struct MaterialData
{
	float4   DiffuseAlbedo;
	float3   FresnelR0;
	float    Roughness;
	float4x4 MatTransform;
	uint     DiffuseMapIndex;
	uint     NormalMapIndex;
	uint     SpecularMapIndex;
	float    HeightFactor;
};

TextureCube						gCubeMap						: register(t0);

Texture2D    gDisplacementMap : register(t1);


// An array of textures, which is only supported in shader model 5.1+.  Unlike Texture2DArray, the textures
// in this array can be different sizes and formats, making it more flexible than texture arrays.
Texture2D						gTextureMaps[DESCRIPTOR_COUNT]	: register(t2);

// Put in space1, so the texture array does not overlap with these resources.  
// The texture array will occupy registers t0, t1, ..., t3 in space0. 
StructuredBuffer<MaterialData>  gMaterialData					: register(t0, space1);

SamplerState					gsamPointWrap					: register(s0);
SamplerState					gsamPointClamp					: register(s1);
SamplerState					gsamLinearWrap					: register(s2);
SamplerState					gsamLinearClamp					: register(s3);
SamplerState					gsamAnisotropicWrap				: register(s4);
SamplerState					gsamAnisotropicClamp			: register(s5);

cbuffer cbPerObject : register(b0)
{
    float4x4 gWorld;
	float4x4 gTexTransform;
	uint	 gMaterialIndex;
	float 	 gGridSpatialStep;
	float2	 gDisplacementMapTexelSize;
};

cbuffer cbSkinned : register(b1)
{
    float4x4 gBoneTransforms[128];
};

cbuffer cbPass : register(b2)
{
	float4x4 gView;
	float4x4 gInvView;
	float4x4 gProj;
	float4x4 gInvProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float3	 gEyePosW;
	float    cbPerObjectPad1;
	float2   gRenderTargetSIze;
	float    gInvRenderTargetSize;
	float    gNearZ;
	float    gFarZ;
	float    gTotalTime;
	float    gDeltaTime;
	float4	 gAmbientLight;
	float4	 gFogColor;
	float	 gFogStart;
	float	 gFogRange;
	float2	 cbPerObjectPad2;
	Light	 gLights[MaxLights];
};

// Transforms a normal map sample to world space
float3 NormalSampleToWorldSpace( float3 normalMapSample, float3 unitNormalW, float3 tangentW )
{
	float3 normalT = 2.0f * normalMapSample - 1.0f;	// Uncompress each component from [0,1] to [-1,1]

	
	float3 N = unitNormalW;			// Build orthonormal basis
	float3 T = normalize( tangentW - dot( tangentW, N ) * N );
	float3 B = cross( N, T );

	float3x3 TBN = float3x3( T, B, N );
	
	float3 bumpedNormalW = mul( normalT, TBN );		// Transform from tangent space to world space

	return bumpedNormalW;
}
