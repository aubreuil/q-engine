#include "Common.hlsli"

struct VertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC    : TEXCOORD;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
    float3 PosL : POSITION;
};
 
// Vertex
VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	vout.PosL = vin.PosL;	// Use local vertex position as cubemap lookup vector
	
	float4 posW = mul( float4( vin.PosL, 1.0f ), gWorld ); // Transform to world space

	posW.xyz += gEyePosW;	// Always center sky about camera

	vout.PosH = mul( posW, gViewProj ).xyww;		// Set z = w so that z/w = 1 (i.e., skydome always on far plane)
	
	return vout;
}

// Pixel
float4 PS(VertexOut pin) : SV_Target
{
	return gCubeMap.Sample( gsamLinearWrap, pin.PosL );
}