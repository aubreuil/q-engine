#pragma warning (push)
#pragma warning (disable : 4005) // Windows Kits\10\Include\10.0.18362.0\ucrt\crtdbg.h >>>>>> warning C4005 : '_malloca' : macro redefinition
#include "FrameResource.h"
#pragma warning (pop)

FrameResource::FrameResource(ID3D12Device* device, UINT passCount, UINT objectCount,  UINT skinnedObjectCount, UINT materialCount )
{
	ThrowIfFailed(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(CmdListAlloc.GetAddressOf())));

	PassCB = std::make_unique<UploadBuffer<PassConstants>>( device, passCount, true );
	MaterialBuffer = std::make_unique<UploadBuffer<MaterialData>>( device, materialCount, false );
	ObjectCB = std::make_unique<UploadBuffer<ObjectConstants>>( device, objectCount, true );
	if ( skinnedObjectCount != 0 )
		SkinnedCB = std::make_unique<UploadBuffer<SkinnedConstants>>( device, skinnedObjectCount, true );
	else
		SkinnedCB = nullptr;
}

FrameResource::~FrameResource()
{

}
