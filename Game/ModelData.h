#pragma once

#include <wrl.h>
#include <d3d12.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>

#include <map>
#include <vector>
#include <string>

enum AlphaFunc
{
	NoTextures = 0,
	TexturedNoClip,
	TexturedAlphaClip,
	TexturedTransparent
};

struct Mat
{
	Mat( DirectX::XMFLOAT4& diffuse, DirectX::XMFLOAT3& fresnel, float roughness) : DiffuseAlbedo( diffuse ), FresnelR0( fresnel ), Roughness( roughness )
	{
	
	}
	DirectX::XMFLOAT4	DiffuseAlbedo		= { 1.0f, 1.0f, 1.0f, 1.0f };
	DirectX::XMFLOAT3	FresnelR0			= { 0.01f, 0.01f, 0.01f };
	float				Roughness			= 0.25f;
};

struct Texture
{
	std::string								Name;
	std::wstring							Filename;
	Microsoft::WRL::ComPtr<ID3D12Resource>	Resource = nullptr;
	Microsoft::WRL::ComPtr<ID3D12Resource>	UploadHeap = nullptr;
};

struct Vertex
{
	Vertex() = default;
	Vertex(float x, float y, float z, float nx, float ny, float nz, float u, float v) :
		Pos(x, y, z),
		Normal(nx, ny, nz),
		TexC(u, v),
		TangentU(1,0,0)
	{ }

	DirectX::XMFLOAT3 Pos;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 TexC;
	DirectX::XMFLOAT3 TangentU;
};

struct StaticMesh
{
	std::string					mMeshName;
	std::string					mMaterialName;
	DirectX::XMFLOAT4X4			mModelWorld;
	std::vector<Vertex>			mVertices;
	std::vector<UINT>			mIndices;
	DirectX::BoundingBox		mBounds;
	AlphaFunc					mAlpha;
};

struct Skeleton
{
	std::vector<  int8_t >				m_hierarchy;
	std::vector< DirectX::XMFLOAT4X4 >	m_offsetMatrix;
};

struct SkeletonLoadingData
{
	std::map<int32_t, int32_t>			m_sceneToJoint;
	std::map<int8_t, int8_t>			m_jointToScene;
};
