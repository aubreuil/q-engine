#pragma once

#include <DirectXMath.h>
#include <UploadBuffer.h>
#include <Maths.h>

struct Light
{
    DirectX::XMFLOAT3	Strength	 = { 0.5f, 0.5f, 0.5f };
    float				FalloffStart = 1.0f;                    // point/spot light only
    DirectX::XMFLOAT3	Direction	 = { 0.0f, -1.0f, 0.0f };	// directional/spot light only
    float				FalloffEnd	 = 10.0f;                   // point/spot light only
    DirectX::XMFLOAT3	Position	 = { 0.0f, 0.0f, 0.0f };	// point/spot light only
    float				SpotPower	 = 64.0f;                   // spot light only
};

struct MaterialData
{
	DirectX::XMFLOAT4	DiffuseAlbedo	= { 1.0f, 1.0f, 1.0f, 1.0f };
	DirectX::XMFLOAT3	FresnelR0		= { 0.01f, 0.01f, 0.01f };
	float				Roughness		= 0.5f;
	DirectX::XMFLOAT4X4 MatTransform	= Maths::Identity4x4();
	UINT				DiffuseMapIndex = 0;
	UINT				NormalMapIndex	= -1;
    UINT                SpecularMapIndex = 1;
    float				HeightFactor = 1.0f;
};

struct ObjectConstants
{
    DirectX::XMFLOAT4X4 World = Maths::Identity4x4();
	DirectX::XMFLOAT4X4 TexTransform = Maths::Identity4x4();
	UINT				MaterialIndex;
	float				GridSpatialStep = 1.0f;
	DirectX::XMFLOAT2	DisplacementMapTexelSize = { 1.0f, 1.0f };
};

const int gMaxLights = 8;

struct PassConstants
{
    DirectX::XMFLOAT4X4 View		= Maths::Identity4x4();
    DirectX::XMFLOAT4X4 InvView		= Maths::Identity4x4();
    DirectX::XMFLOAT4X4 Proj		= Maths::Identity4x4();
    DirectX::XMFLOAT4X4 InvProj		= Maths::Identity4x4();
    DirectX::XMFLOAT4X4 ViewProj	= Maths::Identity4x4();
    DirectX::XMFLOAT4X4 InvViewProj = Maths::Identity4x4();
    DirectX::XMFLOAT3	EyePosW				= { 0.0f, 0.0f, 0.0f };
    float				cbPerObjectPad1		= 0.0f;
    DirectX::XMFLOAT2	RenderTargetSize	= { 0.0f, 0.0f };
    DirectX::XMFLOAT2	InvRenderTargetSize = { 0.0f, 0.0f };
    float				NearZ				= 0.0f;
    float				FarZ				= 0.0f;
    float				TotalTime			= 0.0f;
    float				DeltaTime			= 0.0f;
    DirectX::XMFLOAT4	AmbientLight		= { 0.0f, 0.0f, 0.0f, 1.0f };
	DirectX::XMFLOAT4	FogColor			= { 0.7f, 0.7f, 0.7f, 1.0f };
	float				FogStart			= 5.0f;
	float				FogRange			= 150.0f;
	DirectX::XMFLOAT2	cbPerObjectPad2;

	Light				Lights[gMaxLights];

};

struct SkinnedConstants
{
    DirectX::XMFLOAT4X4 BoneTransforms[128];
};

struct FrameResource
{
public:
    
    FrameResource( ID3D12Device* device, UINT passCount, UINT objectCount, UINT skinnedObjectCount, UINT materialCount );
    FrameResource(const FrameResource& rhs) = delete;
    FrameResource& operator=(const FrameResource& rhs) = delete;
    ~FrameResource();

    Microsoft::WRL::ComPtr<ID3D12CommandAllocator>		CmdListAlloc;
    std::unique_ptr<UploadBuffer<PassConstants>>		PassCB = nullptr;
    std::unique_ptr<UploadBuffer<ObjectConstants>>		ObjectCB = nullptr;
	std::unique_ptr<UploadBuffer<MaterialData>>			MaterialBuffer = nullptr; 
	std::unique_ptr<UploadBuffer<SkinnedConstants>>		SkinnedCB = nullptr;
    UINT64												Fence = 0;
};

// FrameResource
// Stores the resources needed for the CPU to build the command lists for a frame
//
//	CmdListAlloc
//	We cannot reset the allocator until the GPU is done processing the commands.
//	So each frame needs their own allocator.
//
//	PassCB, ObjectCB and MaterialBuffer
//	We cannot update a cbuffer until the GPU is done processing the commands
//	that reference it.  So each frame needs their own cbuffers.
//
//	Fence
//	Fence value to mark commands up to this fence point.  This lets us
//	check if these frame resources are still in use by the GPU
//
//
// Note
// Indices [0, NUM_DIR_LIGHTS) are directional lights;
// indices [NUM_DIR_LIGHTS, NUM_DIR_LIGHTS+NUM_POINT_LIGHTS) are point lights;
// indices [NUM_DIR_LIGHTS+NUM_POINT_LIGHTS, NUM_DIR_LIGHTS+NUM_POINT_LIGHT+NUM_SPOT_LIGHTS)
// are spot lights for a maximum of MaxLights per object.
