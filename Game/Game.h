
#include <QApp.h>
#include <TGATextureLoader.h>
#include "FrameResource.h"
#include "RenderItem.h"
#include "Player.h"
#include "ModelData.h"
#include "SceneBuilder.h"
//#include "GpuWaves.h"
#include "Common.h"

class Game : public QApp
{
public:
    Game( HINSTANCE hInstance );
    Game( const Game& rhs ) = delete;
    Game& operator=(const Game& rhs) = delete;
    ~Game();

    virtual bool		Initialize() override;

private:
//	virtual void		CreateRtvAndDsvDescriptorHeaps() override;
    virtual void		OnResize() override;
    virtual void		Update( const GameTimer& gt ) override;
    virtual void		Draw( const GameTimer& gt ) override;
	
	virtual void		OnKeyUp( WPARAM vKey ) override;
    virtual void		OnMouseDown( WPARAM btnState, int x, int y ) override;
    virtual void		OnMouseUp( WPARAM btnState, int x, int y ) override;
    virtual void		OnMouseMove( WPARAM btnState, int x, int y ) override;
	virtual void		OnMouseWheel( short delta ) override;

	// Initialisation
	void				LoadCameraAndLights( const std::wstring& textFile );


	void				BuildRootSignature();
	void			    BuildWavesRootSignature();
	void				BuildShadersAndInputLayout();
	void 				BuildFrameResources();
    void 				BuildPSOs();

	std::array<const CD3DX12_STATIC_SAMPLER_DESC, 6>
						GetStaticSamplers();

	// Update
	void				UpdatePlayer(const GameTimer& gt);
	void				UpdateFirstPersonCamera( const GameTimer& gt );
	void				UpdateFollowCamera( const GameTimer& gt );
	void				UpdateTheObject( const GameTimer& gt );
	void				UpdateCameraAndLights( const GameTimer& gt );
	void				UpdateMaterials( const GameTimer& gt );
	void				UpdateObjectCBs( const GameTimer& gt );
	void				UpdateSkinnedCBs( const GameTimer& gt );
	void				UpdateMaterialBuffer(const GameTimer& gt);
	void 				UpdateMainPassCB( const GameTimer& gt );
	void				UpdateWavesGPU( const GameTimer& gt );
	float				ClampAngle( float angle, float min, float max );
	void				DebounceAsyncKey( const char key );
	void				PickTriangle( int sx, int sy );
//	void				PerformStaticsFrustumCulling();
	void 				DrawRenderItems( ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& ritems );
	
private:
	// GPU
	Microsoft::WRL::ComPtr<ID3D12RootSignature>									 m_RootSignature = nullptr;
	Microsoft::WRL::ComPtr<ID3D12RootSignature>									 mWavesRootSignature = nullptr;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>								 m_SRVHeap = nullptr;
	std::vector<D3D12_INPUT_ELEMENT_DESC>										 m_InputLayout;
	std::vector<D3D12_INPUT_ELEMENT_DESC>										 m_BumpedInputLayout;
	std::vector<D3D12_INPUT_ELEMENT_DESC>										 m_SkinnedInputLayout;
	std::unordered_map<std::string, Microsoft::WRL::ComPtr<ID3DBlob>>			 m_Shaders;
	std::unordered_map<std::string, Microsoft::WRL::ComPtr<ID3D12PipelineState>> m_PSOs;
	std::vector<std::unique_ptr<FrameResource>>									 m_FrameResources;
    FrameResource*																 m_CurrFrameResource = nullptr;
    PassConstants																 m_MainPassCB;
	UINT																		 m_DescriptorCount;
	UINT																		 m_SRVDescriptorSize;
	int																			 m_CurrFrameResourceIndex = 0;
		
	// Game
	UINT					  m_SkySrvOffset;

	UINT					  m_WavesSrvOffset;
	
	SceneBuilder*			  m_pSceneBuilder = nullptr;
	
	GameObject*				  m_pGameObject = nullptr;


	Player					 m_Player;
	DirectX::XMVECTOR		 m_PlayerLook;
	DirectX::BoundingFrustum m_CamFrustum;
	float					 m_DistanceUp = 9;
	float					 m_DistanceBack = 12;
	bool					 mFrustumCullingEnabled = true;
	POINT					 m_LastMousePos;
	int						 m_OurDownKey = 0;
	short					 m_ScrollWheelValue = 0;
	bool					 m_IsFollowCamera = false;
	bool					 m_WalkCamMode;
	
	// Lighting
	int						 m_NumLights;
	int						 m_NumDirLights;
	int						 m_NumPointLights;
	int						 m_NumSpotLights;
	DirectX::XMFLOAT4		 m_AmbientLight;
	DirectX::XMFLOAT4        m_OriginalAmbience;
	std::wstring			 m_LightName[gMaxLights]		= {};
	DirectX::XMFLOAT3		 m_LightColour[gMaxLights]		= {};
	std::wstring             m_LightColourName[gMaxLights]  = {};
	DirectX::XMFLOAT3		 m_LightPosition[gMaxLights]	= {};
	DirectX::XMFLOAT3		 m_LightDirection[gMaxLights]	= {};
	float					 m_FallOffStart[gMaxLights]		= {};
	float					 m_FallOffEnd[gMaxLights]		= {};
	float					 m_SpotPower[gMaxLights]		= {};
	bool                     m_SpecularReflections	= false;
	bool					 m_HeadLampOn			= false;
	bool					 m_LightingDirty		= true;

	bool m_bDrawWaterWaves = true;

	
};