#include "Player.h"

using namespace DirectX;

Player::Player()
{

}

Player::~Player()
{
	
}

void Player::AssignRenderItem( RenderItem* pPlayerRenderItem )
{
	//m_PlayerRitem = pPlayerRenderItem;
	m_PlayerRitems.push_back(pPlayerRenderItem);
	//m_PlayerWorld = pPlayerRenderItem->mWorld;
}

void Player::SetOriginalScale( XMFLOAT3& scale )
{
	m_OriginalScale = scale;
}

void Player::WalkFacing( float d, XMVECTOR& look )
{
	// Change look
	XMStoreFloat3( &m_Look, look );

	// Set position
	XMVECTOR s = XMVectorReplicate( d );
	XMVECTOR l = XMLoadFloat3( &m_Look );
	XMVECTOR p = XMLoadFloat3( &m_Position );
	XMStoreFloat3( &m_Position, XMVectorMultiplyAdd( s, l, p ) ); 

	// Set rotation
	XMVECTOR angle = XMVector3AngleBetweenVectors( WORLD_FORWARDS, look );
	float lookAngle = XMVectorGetY( angle );

	XMFLOAT4 rotQuat;
	if ( m_Look.x < 0 )
		lookAngle = -lookAngle;

	XMVECTOR Q = XMQuaternionRotationAxis( WORLD_UP, lookAngle );
	XMStoreFloat4( &rotQuat, Q );

	// Set World
	UpdateWorld( m_OriginalScale, m_Position, rotQuat );
	for ( UINT i = 0 ; i < m_PlayerRitems.size() ; ++i )
    {
		m_PlayerRitems[i]->mWorld = m_PlayerWorld;
		m_PlayerRitems[i]->mNumFramesDirty = gNumFrameResources;
	}
}

void Player::LookStrafe( XMVECTOR& look,  const XMVECTOR& direction, float d )
{
	// Change look
	XMStoreFloat3( &m_Look, look );

	// Set rotation
	XMVECTOR angle = XMVector3AngleBetweenVectors( WORLD_FORWARDS, look );
	float lookAngle = XMVectorGetY( angle );

	XMFLOAT4 rotQuat;
	if ( m_Look.x < 0 )
		lookAngle = -lookAngle;

	XMVECTOR Q = XMQuaternionRotationAxis( WORLD_UP, lookAngle );
	XMStoreFloat4( &rotQuat, Q );
	
	// Set Position
	XMVECTOR Distance = XMVectorReplicate( d );
	XMVECTOR Pos = XMLoadFloat3( &m_Position);
	XMStoreFloat3( &m_Position, XMVectorMultiplyAdd( Distance, direction, Pos ) ); 

	// Set World
	UpdateWorld( m_OriginalScale, m_Position, rotQuat );
	for ( UINT i = 0 ; i < m_PlayerRitems.size() ; ++i )
    {
		m_PlayerRitems[i]->mWorld = m_PlayerWorld;
		m_PlayerRitems[i]->mNumFramesDirty = gNumFrameResources;
	}
}
	

void Player::UpdateWorld(  DirectX::XMFLOAT3& scale, DirectX::XMFLOAT3& position, DirectX::XMFLOAT4& rotationQuat )
{
	const XMVECTOR rotOriginZero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

	m_Position = position;
	XMVECTOR pos = XMLoadFloat3( &position );
	XMVECTOR rotQuat = XMLoadFloat4( &rotationQuat );
	
	// Change World
	XMStoreFloat4x4( &m_PlayerWorld, XMMatrixAffineTransformation( XMLoadFloat3( &scale ), rotOriginZero, rotQuat, pos ) );

	// Change Look
	XMStoreFloat3( &m_Look, XMVector3TransformNormal( XMLoadFloat3( &m_Look ), XMMatrixRotationQuaternion( rotQuat ) ) );

	// Update Orthogonals
	XMVECTOR Right = XMLoadFloat3(&m_Right);
	XMVECTOR Up = XMLoadFloat3(&m_Up);
	XMVECTOR Look = XMLoadFloat3(&m_Look);
	
	Up = XMVector3Normalize( XMVector3Cross( Look, Right ) );
	XMStoreFloat3( &m_Up, Up );

	Right = XMVector3Cross( Up, Look );
	XMStoreFloat3( &m_Right, Right );
}

XMVECTOR Player::GetPosition() const
{
	return XMLoadFloat3( &m_Position );
}

