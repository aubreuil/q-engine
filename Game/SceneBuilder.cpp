#include "SceneBuilder.h"
#include <GeometryGenerator.h>
#include <DDSTextureLoader.h>
#include <WICTextureLoader.h>
#include <DirectXColors.h>
#include <array>
#include <static_model_generated.h>
#include <iostream>

using namespace std;
using namespace DirectX;
using Microsoft::WRL::ComPtr;

#include "GameConstants.h"

SceneBuilder::SceneBuilder( ID3D12Device* pD, 
				ID3D12GraphicsCommandList* pC, 
				CD3DX12_CPU_DESCRIPTOR_HANDLE hDesc, 
				CD3DX12_GPU_DESCRIPTOR_HANDLE hVid, 
				UINT descSize, UINT maxDescriptors ) : m_pDevice(pD), m_pCommandList(pC)
{
	m_hCpu = hDesc;
	m_hVid = hVid;
	m_SizeOfDescriptors = descSize;
	
	m_MaxDescriptors = maxDescriptors;

	// Resource Heap slot index
	m_NextSrvHeapSlot = 0;

	// Object constant buffer index
	m_ObjectCBIndex = 0;

	// Material constant buffer index
	m_MaterialCBIndex = 0;

	// Default texture map and bump map slots
	BuildTextureResource( "DefaultDiff", "white1x1.dds" );
	BuildTextureResource( "DefaultBump", "default_nmap.dds" );
	m_pTGALoader = new TGATextureLoader();
}

SceneBuilder::~SceneBuilder()
{
	if (m_pTGALoader != nullptr)
	{
		delete m_pTGALoader;
		m_pTGALoader = nullptr;
	}

	if ( !Assimp::DefaultLogger::isNullLogger() )
        Assimp::DefaultLogger::kill();
}

bool SceneBuilder::LoadGeometryResources( const wstring& textFile, GameObject* pObj, Player& player )
{
	ifstream fin(textFile);

	if ( fin.bad() )
	{
		wstring err = L"File Not Found ~ " + textFile;
		OutputDebugString( err.c_str());
		return false;
	}

	// Read buffers
	string  ignore;
	string  type;
	string  textureFile;
	string  bumpFile;
	string  windowTextureFile;
	FLOAT        height, width, depth, radius;
	FLOAT        bottomRadius, topRadius;
	FLOAT        tileX, tileY, tileZ;
	FLOAT        red, green, blue, alpha;
	FLOAT        roughness;
	FLOAT        transparency;
	FLOAT        posX, posY, posZ;
	FLOAT        rotX, rotY, rotZ;
	FLOAT        scaleX, scaleY, scaleZ;
	UINT         divs;
	UINT         numObjects;
	UINT         instances;
	UINT		 rows, cols, slices, stacks;
	UINT         alphaFunc;
	bool         reflect;
	bool         isPlayer = false;
	bool		 isObjectTest = false;
	XMMATRIX     objPos, objRot, objScale;
	wstring pathWide;
	pathWide.resize(MAX_PATH);

	fin >> ignore;
	fin >> ignore >> numObjects;

	for (int n = 0; n != numObjects; ++n)
	{
		fin >> ignore;
		fin >> ignore >> type;		// Unique DrawArgs[] name

		string  geoName = type + "Geo" + to_string(n);

		switch (GeoTypeNames[type])
		{
			// Geometry
		case Quad:
			fin >> ignore >> posX >> posY >> width >> height >> depth;
			BuildQuad(geoName, type, posX, posY, width, height, depth);
			break;
		case Grid:
			fin >> ignore >> width >> depth >> rows >> cols;
			BuildGrid(geoName, type, width, depth, rows, cols);
			break;
		case Box:
			fin >> ignore >> width >> height >> depth >> divs >> isObjectTest;
			BuildBox(geoName, type, width, height, depth, divs);
			break;
		case Sphere:
			fin >> ignore >> radius >> slices >> stacks;
			BuildSphere(geoName, radius, slices, stacks);
			break;
		case Cylinder:
			fin >> ignore >> bottomRadius >> topRadius >> height >> slices >> stacks >> isPlayer;
			BuildCylinder(geoName, type, bottomRadius, topRadius, height, slices, stacks);
			break;

			// Specials
		case Floor:
			fin >> ignore >> posX >> width >> depth >> textureFile >> bumpFile;
			BuildFloor(posX, width, depth, textureFile, bumpFile);
			continue;
		case Walls:
			fin >> ignore >> posX >> cols >> rows >> width >> textureFile >> bumpFile >> windowTextureFile >> transparency;
			BuildWallsWithWindows(posX, cols, rows, width, textureFile, bumpFile, windowTextureFile, transparency);
			continue;
		case Room:
			fin >> ignore >> posX >> cols >> rows >> width >> depth >> textureFile >> bumpFile >> windowTextureFile >> transparency;
			BuildWallsWithWindows(posX, cols, rows, width, textureFile, bumpFile, windowTextureFile, transparency);

			fin >> ignore >> textureFile >> bumpFile;
			BuildFloor(posX + width / 2, width * (cols + 1), depth * (rows - 1), textureFile, bumpFile);

			fin >> ignore >> textureFile >> bumpFile;
			BuildCeiling(posX + width / 2, 3.0f * width / 4.0f, width * (cols + 1), depth * (rows - 1), textureFile, bumpFile);
			continue;

		default:
			return false;
		}

		// Textures
		fin >> ignore >> textureFile;
		fin >> ignore >> bumpFile;
		fin >> ignore >> tileX >> tileY >> tileZ;

		// Diffuse
		UINT diffSlot = 0;
		string texName = "None";
		if (textureFile != "None")
		{
			texName = "GeoTex_" + to_string(n);
			diffSlot = m_NextSrvHeapSlot;
			BuildTextureResource(texName.c_str(), textureFile.c_str());
		}

		// Bump
		UINT bumpSlot = 1;
		if (bumpFile != "None")
		{
			string theNormalName = "GeoNrm_" + to_string(n);
			bumpSlot = m_NextSrvHeapSlot;
			BuildTextureResource(theNormalName.c_str(), bumpFile.c_str());
		}

		// Materiel
		string matName = "GeoMat_" + to_string(n);

		fin >> ignore >> red >> green >> blue >> alpha;
		XMFLOAT4 diffuse = XMFLOAT4(red, green, blue, alpha);
		fin >> ignore >> red >> green >> blue;
		XMFLOAT3 fresnel = XMFLOAT3(red, green, blue);
		fin >> ignore >> roughness;
		roughness = Maths::Clamp(roughness, 0.0f, 0.99f);
		Mat matProps(diffuse, fresnel, roughness);
		AddMaterial(matName, matProps, diffSlot, bumpSlot);

		// RenderItem
		fin >> ignore >> alphaFunc;
		fin >> ignore >> reflect;
		fin >> ignore >> instances;
		fin >> ignore >> posX >> posY >> posZ;
		fin >> ignore >> rotX >> rotY >> rotZ;
		fin >> ignore >> scaleX >> scaleY >> scaleZ;

		rotX = XMConvertToRadians(rotX);
		rotY = XMConvertToRadians(rotY);
		rotZ = XMConvertToRadians(rotZ);
		objScale = XMMatrixScaling(scaleX, scaleY, scaleZ);
		objRot = XMMatrixRotationRollPitchYaw(rotX, rotY, rotZ);
		objPos = XMMatrixTranslation(posX, posY, posZ);

		AddRenderItem(geoName, type, matName, objScale, objRot, objPos, alphaFunc, tileX, tileY);

		for (UINT n = 1; n < instances; ++n)
		{
			fin >> ignore >> posX >> posY >> posZ;

			objPos = XMMatrixTranslation(posX, posY, posZ);

			AddRenderItem(geoName, type, matName, objScale, objRot, objPos, alphaFunc, tileX, tileY);
		}

		if ( isPlayer )
		{
			player.AssignRenderItem( m_AllRenderItems.at(m_ObjectCBIndex - 1).get() );

			XMFLOAT3 originalScale = XMFLOAT3( scaleX, scaleY, scaleZ );
			player.SetOriginalScale( originalScale );

			XMFLOAT3 position = XMFLOAT3( 0, posY, 0);
			XMFLOAT4 rotation = XMFLOAT4(0,0,0,1);
			player.UpdateWorld( originalScale, position, rotation );
			isPlayer = false;
		}

		if ( isObjectTest )
		{
			pObj->m_CBIndex = m_ObjectCBIndex - 1;

			pObj->m_Pos = XMVectorSet(posX,posY,posZ,0);
			pObj->m_Rot = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
			pObj->m_TargetPos = XMFLOAT3( 0.0f, 8.2f, 0.0f );
			pObj->m_TargetRot = XMFLOAT3( 0.0f, 0.0f , 0.0f );
			pObj->m_IsIdle = false;

			isObjectTest = false;
		}
	}

	fin.close();

	wstring info = L"\n\nGeometry Next Slot " + to_wstring(m_NextSrvHeapSlot) + L"\n\n";
	OutputDebugString( info.c_str());

	return true;
}

bool SceneBuilder::LoadSceneModels( const wstring& textFile, Player& player )
{
	ifstream fin(textFile);

	if ( fin.bad() )
	{
		wstring err = L"File Not Found ~ " + textFile;
		OutputDebugString( err.c_str());
		return false;
	}

	string		ignore;
	string		localPath;
	string		modelFilename;
	UINT        numModels;
	bool        isSkinned;
	XMMATRIX	modelPos;
	XMMATRIX	modelRot;
	XMMATRIX	modelScale;
	UINT		alphaFunc;
	FLOAT		posX, posY, posZ;
	FLOAT		rotX, rotY, rotZ;
	FLOAT		scaleX, scaleY, scaleZ;

	fin >> ignore;
	fin >> ignore >> numModels;

	for (int n = 0; n != numModels; ++n)
	{
		fin >> ignore;
		fin >> ignore >> m_LocalPath;
		fin >> ignore >> modelFilename;
		fin >> ignore >> isSkinned;
		fin >> ignore >> posX >> posY >> posZ;
		fin >> ignore >> rotX >> rotY >> rotZ;
		fin >> ignore >> scaleX >> scaleY >> scaleZ;
		fin >> ignore >> alphaFunc;

		modelScale = XMMatrixScaling(scaleX, scaleY, scaleZ);

		rotX = XMConvertToRadians(rotX);
		rotY = XMConvertToRadians(rotY);
		rotZ = XMConvertToRadians(rotZ);

		modelRot = XMMatrixRotationRollPitchYaw(rotX, rotY, rotZ);
		modelPos = XMMatrixTranslation(posX, posY, posZ);
		XMMATRIX theWorld = modelScale * modelRot * modelPos;
		XMFLOAT4X4 modelWorld;
		XMStoreFloat4x4(&modelWorld, theWorld);

		string actualModelDir  = MODEL_PATH + m_LocalPath + "/";
		string actualModelPath = MODEL_PATH + m_LocalPath + "/" + modelFilename;
		bool isQlblModel = modelFilename.size() > 5 && modelFilename.find(".qlbl") != string::npos;

		if (!isSkinned)
		{
			vector<StaticMesh> modelMeshes;

			if (isQlblModel)
			{
				if (!LoadStaticQlblModel(actualModelDir, actualModelPath, modelMeshes, modelFilename, modelWorld, alphaFunc == TexturedAlphaClip))
				{
					wstring infoText = L"\n                                                                   QLBL was unable to load " + AnsiToWString(actualModelPath) + L" \n";
					OutputDebugString(infoText.c_str());
					return false;
				}
			}
			else
			if (!LoadStaticAssImpModel(actualModelPath, modelMeshes, modelFilename, modelWorld, alphaFunc == TexturedAlphaClip))
			{
				wstring infoText = L"\n                                                                   ASSIMP was unable to load " + AnsiToWString(actualModelPath) + L" \n";
				OutputDebugString(infoText.c_str());
				return false;
			}
			BuildStaticRenderObjects( modelFilename, modelMeshes, player );
		}
		else
		{
			vector<SkinnedMesh> meshes;
			if (!LoadSkinnedAssImpModel(actualModelPath, meshes, modelFilename, modelWorld, alphaFunc == TexturedAlphaClip))
			{
				wstring infoText = L"\nASSIMP unable to load " + AnsiToWString(actualModelPath) + L" \n";
				OutputDebugString(infoText.c_str());
				return false;
			}
			BuildSkinnedRenderObjects( modelFilename, meshes, player );
		}

		if ( m_LocalPath == "Player" )
		{
			XMFLOAT3 originalScale = XMFLOAT3(scaleX, scaleY, scaleZ);
			player.SetOriginalScale(originalScale);

			XMFLOAT3 position = XMFLOAT3(posX, posY, posZ);
			XMFLOAT4 rotation = XMFLOAT4(0, 0, 0, 1);
			player.UpdateWorld(originalScale, position, rotation);
		}
	}

	fin.close();

	wstring info = L"\n\nModels Next Slot " + to_wstring(m_NextSrvHeapSlot) + L"\n\n";
	OutputDebugString( info.c_str());

	return true;
}

/// <summary>
/// Build the sky for the scene from the given DDS cube map
/// </summary>
UINT SceneBuilder::LoadSkyCube( const wstring& textFile)
{
	// Geometry
	BuildSphere(SKY_GEO, 0.5f, 20, 20);

	// Texture
	string texFile = WStringToAnsi(textFile);
	UINT diffSlot = m_NextSrvHeapSlot;
	BuildTextureResource("SkyTex", texFile.c_str(), TextureType::SkyCube);
	
	// Materiel
	auto theMat = make_unique<Material>();

	theMat->Name = SKY_MAT;
	theMat->MatCBIndex = m_MaterialCBIndex;
	theMat->DiffuseSrvHeapIndex = diffSlot;

	theMat->NormalSrvHeapIndex = 1;
	theMat->MatTransform = Maths::Identity4x4();
	theMat->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	theMat->FresnelR0 = XMFLOAT3(0.1f, 0.1f, 0.1f);
	theMat->Roughness = 1.0f;

	m_Materials[SKY_MAT] = move(theMat);
	m_MaterialCBIndex++;

	// RenderItem
	auto rendItem = make_unique<RenderItem>();

	XMStoreFloat4x4(&rendItem->mWorld, XMMatrixScaling(5000.0f, 5000.0f, 5000.0f));
	rendItem->mTexTransform = Maths::Identity4x4();
	rendItem->mObjCBIndex = m_ObjectCBIndex;
	rendItem->mMat = m_Materials[SKY_MAT].get();
	rendItem->mGeo = m_Geometries[SKY_GEO].get();
	rendItem->mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	rendItem->mIndexCount = rendItem->mGeo->DrawArgs["sphere"].IndexCount;
	rendItem->mStartIndexLocation = rendItem->mGeo->DrawArgs["sphere"].StartIndexLocation;
	rendItem->mBaseVertexLocation = rendItem->mGeo->DrawArgs["sphere"].BaseVertexLocation;

	m_RenderItems[(int)RenderLayer::Sky].push_back(rendItem.get());
	m_AllRenderItems.push_back(move(rendItem));

	m_ObjectCBIndex++;

	wstring info = L"\n\nSky Next Slot " + to_wstring(m_NextSrvHeapSlot) + L"\n\n";
	OutputDebugString( info.c_str());

	return diffSlot;
}

UINT SceneBuilder::LoadWaterWaves(const wstring& textFile )
{
	ifstream fin(textFile);

	if ( fin.bad() )
	{
		wstring err = L"File Not Found ~ " + textFile;
		OutputDebugString( err.c_str());
		return INT_MAX;
	}
	string ignore, mainTex;
	int    numRows, numCols;
	float  heightFactor, texScaleX, texScaleY, posX, posY, posZ, width, depth, spatialStep, timeStep, speed, damping, red, green, blue, alpha, roughness;

	fin >> ignore;
	fin >> ignore >> mainTex;
	fin >> ignore >> heightFactor;
	fin >> ignore >> texScaleX >> texScaleY;
	fin >> ignore >> posX >> posY >> posZ;
	fin >> ignore >> width;
	fin >> ignore >> depth;
	fin >> ignore >> numRows;
	fin >> ignore >> numCols;
	fin >> ignore >> spatialStep;
	fin >> ignore >> timeStep;
	fin >> ignore >> speed;
	fin >> ignore >> damping;
	m_Waves = make_unique<GpuWaves>( m_pDevice, m_pCommandList, numRows, numCols, spatialStep, timeStep, speed, damping );

	BuildWavesGeometry( WATER_GEO, width, depth );
	
	fin >> ignore >> red >> green >> blue >> alpha;
	XMFLOAT4 diffuse = XMFLOAT4(red, green, blue, alpha);
	fin >> ignore >> red >> green >> blue;
	XMFLOAT3 fresnel = XMFLOAT3(red, green, blue);	
	fin >> ignore >> roughness;
	fin.close();

	roughness = Maths::Clamp(roughness, 0.0f, 0.99f);
	Mat matProps(diffuse, fresnel, roughness);

	UINT slot = m_NextSrvHeapSlot;
	BuildTextureResource( "waterTex", mainTex.c_str(), TextureType::Tex );

	BuildUAVWaveDescriptors();

	AddMaterial( WATER_MAT, matProps, slot, 1);
	m_Materials[WATER_MAT]->HeightFactor = heightFactor; 

	XMMATRIX scl = XMMatrixScaling(1, 1, 1);
	XMMATRIX rot = XMMatrixIdentity();
	XMMATRIX pos = XMMatrixTranslation(posX, posY, posZ);
	XMMATRIX scaleTex = XMMatrixScaling(texScaleX, texScaleY, 1.0f);

	auto wavesRitem = make_unique<RenderItem>();
	XMStoreFloat4x4( &wavesRitem->mWorld, scl * rot * pos);
	XMStoreFloat4x4( &wavesRitem->mTexTransform, scaleTex );
	wavesRitem->mDisplacementMapTexelSize.x = 1.0f / m_Waves->ColumnCount();
	wavesRitem->mDisplacementMapTexelSize.y = 1.0f / m_Waves->RowCount();
	wavesRitem->mGridSpatialStep = m_Waves->SpatialStep();
	wavesRitem->mObjCBIndex = m_ObjectCBIndex;
	wavesRitem->mMat = m_Materials[WATER_MAT].get();
	wavesRitem->mGeo = m_Geometries[WATER_GEO].get();
	wavesRitem->mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	wavesRitem->mIndexCount = wavesRitem->mGeo->DrawArgs["grid"].IndexCount;
	wavesRitem->mStartIndexLocation = wavesRitem->mGeo->DrawArgs["grid"].StartIndexLocation;
	wavesRitem->mBaseVertexLocation = wavesRitem->mGeo->DrawArgs["grid"].BaseVertexLocation;

	m_RenderItems[(int)RenderLayer::GpuWaves].push_back(wavesRitem.get());
	m_AllRenderItems.push_back(move(wavesRitem));

	m_ObjectCBIndex++;

	wstring info = L"\n\nWater Next Slot " + to_wstring(m_NextSrvHeapSlot) + L"\n\n";
	OutputDebugString( info.c_str());

	return slot;
}

/// <summary>
/// Create a shader resource view from a texture file in the TEXTURE_PATH_WIDE folder
/// </summary>
void SceneBuilder::BuildTextureResource( const char* name, const char* fileName, TextureType type )
{
	string  materialsPath;
	wstring pathWide;

	//todo: move this offline
	static std::unordered_map<string, string> substitution;

	if (substitution.empty())
	{
		substitution.insert(std::make_pair("Gate_Diffuse.png", "Bundle/Gate_Diffuse.DDS"));
		substitution.insert(std::make_pair("Gate_Glossiness.png", "Bundle/Gate_Glossiness.DDS"));
		substitution.insert(std::make_pair("Gate_Normal.png", "Bundle/Gate_Normal.DDS"));
		substitution.insert(std::make_pair("Gate_Specular.png", "Bundle/Gate_Specular.DDS"));

		substitution.insert(std::make_pair("Tower_01_Diffuse.png", "Bundle/Tower_01_Diffuse.DDS"));
		substitution.insert(std::make_pair("Tower_01_Glossiness.png", "Bundle/Tower_01_Glossiness.DDS"));
		substitution.insert(std::make_pair("Tower_01_Normal.png", "Bundle/Tower_01_Normal.DDS"));
		substitution.insert(std::make_pair("Tower_01_Specular.png", "Bundle/Tower_01_Specular.DDS"));

		substitution.insert(std::make_pair("Tower_02_Diffuse.png", "Bundle/Tower_02_Diffuse.DDS"));
		substitution.insert(std::make_pair("Tower_02_Glossiness.png", "Bundle/Tower_02_Glossiness.DDS"));
		substitution.insert(std::make_pair("Tower_02_Normal.png", "Bundle/Tower_02_Normal.DDS"));
		substitution.insert(std::make_pair("Tower_02_Specular.png", "Bundle/Tower_02_Specular.DDS"));

		substitution.insert(std::make_pair("Tower_03_Diffuse.png", "Bundle/Tower_03_Diffuse.DDS"));
		substitution.insert(std::make_pair("Tower_03_Glossiness.png", "Bundle/Tower_03_Glossiness.DDS"));
		substitution.insert(std::make_pair("Tower_03_Normal.png", "Bundle/Tower_03_Normal.DDS"));
		substitution.insert(std::make_pair("Tower_03_Specular.png", "Bundle/Tower_03_Specular.DDS"));

		substitution.insert(std::make_pair("Wall_Diffuse.png", "Bundle/Wall_Diffuse.DDS"));
		substitution.insert(std::make_pair("Wall_Glossiness.png", "Bundle/Wall_Glossiness.DDS"));
		substitution.insert(std::make_pair("Wall_Normal.png", "Bundle/Wall_Normal.DDS"));
		substitution.insert(std::make_pair("Wall_Specular.png", "Bundle/Wall_Specular.DDS"));
		
		substitution.insert(std::make_pair("Guivre.tga", "Bundle/Guivre.DDS"));
		substitution.insert(std::make_pair("Guivre_N.tga", "Bundle/Guivre_N.DDS"));
	}


	string fileToLoad = fileName;
	
	const auto& r = substitution.find(fileName);
	if (r != substitution.end())
	{
		fileToLoad = r->second;
	}

	switch (type)
	{
	case TextureType::Tex:
	case TextureType::SkyCube:
		pathWide = TEXTURE_PATH_WIDE + AnsiToWString(fileToLoad);
		break;

	case TextureType::Mesh:
		materialsPath = m_LocalPath + "/Materials/" + fileToLoad;
		pathWide = MODEL_PATH_WIDE + wstring(materialsPath.begin(), materialsPath.end());
		break;

	default:
		pathWide = AnsiToWString(fileToLoad);
		break;
	}

	D3D12_RESOURCE_DESC texDesc = {};
	D3D12_SUBRESOURCE_DATA subresource;

	auto aTexture = make_unique<Texture>();
	aTexture->Name = name;
	aTexture->Filename = pathWide;
		
	switch (FormatFromFilename(fileToLoad.c_str()))
		{
		case FormatNone:
			assert(false);
			break;

		case FormatDDS:
			ThrowIfFailed(CreateDDSTextureFromFile12(m_pDevice, m_pCommandList, pathWide.c_str(), aTexture->Resource, aTexture->UploadHeap));
			break;
		case FormatTGA:

			m_pTGALoader->LoadTGAFromFile(pathWide.c_str(), texDesc, subresource, false);
			UploadResourceData(aTexture.get(), texDesc, subresource);
			break;

		case FormatBMP:
		case FormatPNG:
		case FormatJPG:
			unique_ptr<uint8_t[]> imageData;
			ThrowIfFailed(LoadWICTextureFromFile(m_pDevice, pathWide.c_str(), texDesc, imageData, subresource));
			UploadResourceData(aTexture.get(), texDesc, subresource);
			break;
		}

	m_Textures[name] = move(aTexture);

	m_SrvDebugHeapSlots[name] = m_NextSrvHeapSlot;

	m_srvDesc = {};
	m_srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	m_srvDesc.Format = m_Textures[name]->Resource->GetDesc().Format;
	m_srvDesc.Texture2D.MipLevels = m_Textures[name]->Resource->GetDesc().MipLevels;
	if (type == TextureType::SkyCube)
		m_srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
	else
		m_srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	m_srvDesc.Texture2D.MostDetailedMip = 0;
	m_srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	m_pDevice->CreateShaderResourceView( m_Textures[name]->Resource.Get(), &m_srvDesc, m_hCpu );

	m_hCpu.Offset(1, m_SizeOfDescriptors);
	m_hVid.Offset(1, m_SizeOfDescriptors);

	if ( type != TextureType::Mesh )
	{
		m_NextSrvHeapSlot++;
		assert(m_NextSrvHeapSlot < m_MaxDescriptors);
	}
}

void SceneBuilder::BuildUAVWaveDescriptors()
{
	m_SrvDebugHeapSlots["uavwavedesc"] = m_NextSrvHeapSlot;

	m_srvDesc = {};

	m_srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	m_srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	m_srvDesc.Texture2D.MipLevels = 1;
	m_srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	m_srvDesc.Texture2D.MostDetailedMip = 0;
	m_srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;

	m_Waves->BuildDescriptors( m_hCpu, m_hVid, m_srvDesc, m_SizeOfDescriptors );

	m_hCpu.Offset(m_Waves->DescriptorCount(), m_SizeOfDescriptors);
	m_hVid.Offset(m_Waves->DescriptorCount(), m_SizeOfDescriptors);

	m_NextSrvHeapSlot += m_Waves->DescriptorCount();
	assert(m_NextSrvHeapSlot < m_MaxDescriptors);
}


/// <summary>
/// Upload the texture resource to the GPU
/// </summary>
void SceneBuilder::UploadResourceData(Texture* pTex, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs)
{
	const auto heap0 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	ThrowIfFailed(m_pDevice->CreateCommittedResource(&heap0, D3D12_HEAP_FLAG_NONE, &texDesc, D3D12_RESOURCE_STATE_COMMON, nullptr, IID_PPV_ARGS(pTex->Resource.GetAddressOf())));

	const UINT num2DSubresources = texDesc.DepthOrArraySize * texDesc.MipLevels;
	const UINT64 uploadBufferSize = GetRequiredIntermediateSize(pTex->Resource.Get(), 0, num2DSubresources);

	const auto heap1 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	const auto buffer = CD3DX12_RESOURCE_DESC::Buffer(uploadBufferSize);
	ThrowIfFailed(m_pDevice->CreateCommittedResource(&heap1, D3D12_HEAP_FLAG_NONE, &buffer, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(pTex->UploadHeap.GetAddressOf())));

	const auto barrier0 = CD3DX12_RESOURCE_BARRIER::Transition(pTex->Resource.Get(), D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_COPY_DEST);
	m_pCommandList->ResourceBarrier(1, &barrier0);

	UpdateSubresources(m_pCommandList, pTex->Resource.Get(), pTex->UploadHeap.Get(), 0, 0, num2DSubresources, &subs);

	const auto barrier1 = CD3DX12_RESOURCE_BARRIER::Transition(pTex->Resource.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	m_pCommandList->ResourceBarrier(1, &barrier1);
}

void SceneBuilder::BuildStaticRenderObjects(string modelName, vector<StaticMesh>& meshes, Player& player)
{
	// Create static Geometry
	auto modelGeo = make_unique<MeshGeometry>();
	modelGeo->Name = modelName;

	UINT totalVertexCount = 0;
	UINT totalIndexCount = 0;

	// Get Counts
	for (int n = 0; n < meshes.size(); ++n)
	{
		totalVertexCount += (UINT)meshes[n].mVertices.size();
		totalIndexCount += (UINT)meshes[n].mIndices.size();
	}

	// Build CPU vertex and index buffers
	const UINT vbByteSize = totalVertexCount * sizeof(Vertex);
	const UINT ibByteSize = totalIndexCount * sizeof(UINT);

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &modelGeo->VertexBufferCPU));
	BYTE* pDestVertsCPU = (BYTE*)modelGeo->VertexBufferCPU->GetBufferPointer();

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &modelGeo->IndexBufferCPU));
	BYTE* pDestIndxsCPU = (BYTE*)modelGeo->IndexBufferCPU->GetBufferPointer();

	// Insert sub mesh verts and indxs into model geometry CPU buffers
	UINT meshIndexOffset = 0;
	UINT meshVertexOffset = 0;

	for (int n = 0; n < meshes.size(); ++n)
	{
		SubmeshGeometry submesh;
		UINT indexCount = (UINT)meshes[n].mIndices.size();
		UINT vertexCount = (UINT)meshes[n].mVertices.size();

		string submeshName = meshes[n].mMeshName;
		submesh.IndexCount = indexCount;
		submesh.StartIndexLocation = meshIndexOffset;
		submesh.BaseVertexLocation = meshVertexOffset;
		submesh.Bounds = meshes[n].mBounds;

		modelGeo->DrawArgs[submeshName] = submesh;

		UINT vertsByteSize = vertexCount * sizeof(Vertex);
		UINT indxsByteSize = indexCount * sizeof(UINT);

		CopyMemory(pDestVertsCPU, meshes[n].mVertices.data(), vertsByteSize);
		CopyMemory(pDestIndxsCPU, meshes[n].mIndices.data(), indxsByteSize);
		pDestVertsCPU += vertsByteSize;
		pDestIndxsCPU += indxsByteSize;

		meshIndexOffset += indexCount;
		meshVertexOffset += vertexCount;
	}

	// Copy the whole jolly lot to the GPU
	BYTE* pSourceVertsGPU = (BYTE*)modelGeo->VertexBufferCPU->GetBufferPointer();
	BYTE* pSourceIndxsGPU = (BYTE*)modelGeo->IndexBufferCPU->GetBufferPointer();
	modelGeo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, pSourceVertsGPU, vbByteSize, modelGeo->VertexBufferUploader);
	modelGeo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, pSourceIndxsGPU, ibByteSize, modelGeo->IndexBufferUploader);
	modelGeo->VertexByteStride = sizeof(Vertex);
	modelGeo->VertexBufferByteSize = vbByteSize;
	modelGeo->IndexFormat = DXGI_FORMAT_R32_UINT;
	modelGeo->IndexBufferByteSize = ibByteSize;

	// Copy the whole jolly geometry to our m_Geometries[] for skinned render
	m_Geometries[modelName] = move(modelGeo);

	// Now Build RenderItems
	for (int n = 0; n < meshes.size(); ++n)
	{
		XMMATRIX texScale = XMMatrixScaling(1, 1, 1);
		if (meshes[n].mMeshName.find("PC_TP_Female_Red02_Lobby.") != -1)
			texScale = XMMatrixScaling(1, -1, 1); 

		//if (meshes[n].mMeshName.find("Castle.") != -1)
			//texScale = XMMatrixScaling(1, -1, 1); // Castle model has inverted V coordinate for some reason

		string submeshName = meshes[n].mMeshName;

		// Build render item for this mesh
		auto rendItem = make_unique<RenderItem>();

		rendItem->mWorld = meshes[n].mModelWorld;
		XMStoreFloat4x4(&rendItem->mTexTransform, texScale);
		rendItem->mObjCBIndex = m_ObjectCBIndex;
		rendItem->mMat = m_Materials[meshes[n].mMaterialName].get();
		rendItem->mGeo = m_Geometries[modelName].get();
		rendItem->mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		rendItem->mIndexCount = rendItem->mGeo->DrawArgs[submeshName].IndexCount;
		rendItem->mStartIndexLocation = rendItem->mGeo->DrawArgs[submeshName].StartIndexLocation;
		rendItem->mBaseVertexLocation = rendItem->mGeo->DrawArgs[submeshName].BaseVertexLocation;
		rendItem->mBounds = rendItem->mGeo->DrawArgs[submeshName].Bounds;

		if (meshes[n].mAlpha == AlphaFunc::TexturedAlphaClip)
			m_RenderItems[(int)RenderLayer::AlphaTested].push_back(rendItem.get());

		else if (meshes[n].mAlpha == AlphaFunc::TexturedTransparent)
			m_RenderItems[(int)RenderLayer::Transparent].push_back(rendItem.get());

		else if (meshes[n].mAlpha == AlphaFunc::TexturedNoClip)
		{
			if (rendItem->mMat->NormalSrvHeapIndex == 1)
				m_RenderItems[(int)RenderLayer::Static].push_back(rendItem.get());
			else
				m_RenderItems[(int)RenderLayer::StaticBump].push_back(rendItem.get());
		}
		else
			m_RenderItems[(int)RenderLayer::Colour].push_back(rendItem.get());

		m_AllRenderItems.push_back(move(rendItem));

		if (m_LocalPath == "Player")
			player.AssignRenderItem(m_AllRenderItems.at(m_ObjectCBIndex).get());

		m_ObjectCBIndex++;
	}
}

void SceneBuilder::BuildSkinnedModelGeometry( string modelName, vector<SkinnedMesh>& meshes)
{
	// Create Skinned Geometry
	auto modelGeo = make_unique<MeshGeometry>();
	modelGeo->Name = modelName;

	UINT totalVertexCount = 0;
	UINT totalIndexCount = 0;

	// Get Counts
	for ( int n = 0 ; n < meshes.size() ; ++n )
	{
		totalVertexCount += (UINT)meshes[ n ].mVertices.size();
		totalIndexCount += (UINT)meshes[ n ].mIndices.size();
	}

	// Build CPU vertex and index buffers
	const UINT vbByteSize = totalVertexCount * sizeof(SkinnedVertex);
	const UINT ibByteSize = totalIndexCount  * sizeof(uint16_t);

	ThrowIfFailed( D3DCreateBlob( vbByteSize, &modelGeo->VertexBufferCPU ) );
	BYTE* pDestVertsCPU = (BYTE*)modelGeo->VertexBufferCPU->GetBufferPointer();

	ThrowIfFailed( D3DCreateBlob( ibByteSize, &modelGeo->IndexBufferCPU ) );
	BYTE* pDestIndxsCPU = (BYTE*)modelGeo->IndexBufferCPU->GetBufferPointer();

	// Insert sub mesh verts and indxs into model geometry CPU buffers
	UINT meshIndexOffset = 0;
	UINT meshVertexOffset = 0;
		
	for ( int n = 0 ; n < meshes.size() ; ++n )
	{
		SubmeshGeometry submesh;
		UINT indexCount = (UINT)meshes[n].mIndices.size();
		UINT vertexCount = (UINT)meshes[n].mVertices.size();

		string submeshName = meshes[n].mMeshName;
		submesh.IndexCount = indexCount;
		submesh.StartIndexLocation = meshIndexOffset;
		submesh.BaseVertexLocation = meshVertexOffset;
		submesh.Bounds = meshes[n].mBounds;

		modelGeo->DrawArgs[submeshName] = submesh;

		UINT vertsByteSize = vertexCount * sizeof(SkinnedVertex);
		UINT indxsByteSize = indexCount * sizeof(uint16_t);

		CopyMemory( pDestVertsCPU, meshes[n].mVertices.data(), vertsByteSize );
		CopyMemory( pDestIndxsCPU, meshes[n].mIndices.data(), indxsByteSize );
		pDestVertsCPU += vertsByteSize;
		pDestIndxsCPU += indxsByteSize;

		meshIndexOffset += indexCount;
		meshVertexOffset += vertexCount;
	}

	// Copy the whole jolly lot to the GPU
	BYTE* pSourceVertsGPU = (BYTE*)modelGeo->VertexBufferCPU->GetBufferPointer();
	BYTE* pSourceIndxsGPU = (BYTE*)modelGeo->IndexBufferCPU->GetBufferPointer();
	modelGeo->VertexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice, m_pCommandList, pSourceVertsGPU, vbByteSize, modelGeo->VertexBufferUploader );
	modelGeo->IndexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice,  m_pCommandList, pSourceIndxsGPU, ibByteSize, modelGeo->IndexBufferUploader );
	modelGeo->VertexByteStride = sizeof(SkinnedVertex);
	modelGeo->VertexBufferByteSize = vbByteSize;
	modelGeo->IndexFormat =  DXGI_FORMAT_R16_UINT;
	modelGeo->IndexBufferByteSize = ibByteSize;

	// Copy the whole jolly geometry to our m_Geometries[] for skinned render
	m_Geometries[ modelName ] = move(modelGeo);
}

void SceneBuilder::BuildSkinnedRenderObjects( string modelName, vector<SkinnedMesh>& meshes, Player& player )
{
	// Initialise the SkinnedModelInstance

	m_SkinnedInfo.Set( m_skeleton.m_hierarchy, m_skeleton.m_offsetMatrix, m_AnimationClips );
	m_SkinnedModelInst = make_unique<SkinnedModelInstance>();
	m_SkinnedModelInst->SkinnedInfo = &m_SkinnedInfo;
	m_SkinnedModelInst->FinalTransforms.resize( m_SkinnedInfo.BoneCount() );
	m_SkinnedModelInst->ClipName = m_ClipNames[0];
	m_SkinnedModelInst->TimePos = 0.0f;


	// Now Build RenderItems
	for ( int n = 0 ; n < meshes.size() ; ++n )
	{
		string submeshName = meshes[ n ].mMeshName; 

		// Build render item for this mesh
		auto rendItem = make_unique<RenderItem>();
				
		rendItem->mWorld = meshes[n].mModelWorld;
		rendItem->mTexTransform = meshes[n].mTextureTransform;
		rendItem->mObjCBIndex = m_ObjectCBIndex;
		rendItem->mMat = m_Materials[ meshes[n].mMaterialName ].get();
		rendItem->mGeo = m_Geometries[ modelName ].get();
		rendItem->mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

		rendItem->mIndexCount = rendItem->mGeo->DrawArgs[submeshName].IndexCount;
		rendItem->mStartIndexLocation = rendItem->mGeo->DrawArgs[submeshName].StartIndexLocation;
		rendItem->mBaseVertexLocation = rendItem->mGeo->DrawArgs[submeshName].BaseVertexLocation;
		rendItem->mBounds = rendItem->mGeo->DrawArgs[ submeshName ].Bounds;
		rendItem->mSkinnedCBIndex = 0;
		rendItem->mSkinnedModelInst = m_SkinnedModelInst.get(); // // All render items for this model share the same skinned model instance

		m_RenderItems[(int)RenderLayer::Skinned].push_back(rendItem.get());
		m_AllRenderItems.push_back(move(rendItem));


		if (m_LocalPath == "Player")
			player.AssignRenderItem(m_AllRenderItems.at(m_ObjectCBIndex).get());

		m_ObjectCBIndex++; 
	}
}

void SceneBuilder::ProcessAnimationClips( string modelName ) 
{
	for ( UINT n = 0 ; n < m_pAiScene->mNumAnimations ; ++n )
	{
		// AnimationClip
		const aiAnimation* pAnimation = m_pAiScene->mAnimations[ n ];
	
		if ( pAnimation->mTicksPerSecond != 0.0 )		// Ticks per second
			m_TicksPerSecond = (float)pAnimation->mTicksPerSecond;
		else
			m_TicksPerSecond = 25.0f;
		
		float duration = (float)pAnimation->mDuration;	// Duration of the animation in ticks
		
		float totalTimeInSeconds = duration / m_TicksPerSecond;

		AnimationClip clip;
		clip.mBoneAnimations.resize(m_skeleton.m_hierarchy.size());
		vector<bool> clipInitialized(m_skeleton.m_hierarchy.size());

		string clipName = pAnimation->mName.C_Str();

		if ( clipName == "" )
			clipName = "NoName";

		UINT numMeshChannels = pAnimation->mNumMeshChannels;

		for ( UINT c = 0 ; c < pAnimation->mNumChannels ; ++c ) // number of animation channels
		{
			// Channel
			const aiNodeAnim* pNodeAnim = pAnimation->mChannels[ c ]; // animation channel for this node
			uint32_t	sceneIndex		= m_SceneNodes[pNodeAnim->mNodeName.C_Str()];
			const auto it				= m_skeletonLoading.m_sceneToJoint.find(sceneIndex);

			if (it != m_skeletonLoading.m_sceneToJoint.end())
			{
				uint32_t jointIndex = it->second;

				// Our BoneAnimation
				BoneAnimation ourBoneAnim;

				float timeDelta		= totalTimeInSeconds / pNodeAnim->mNumPositionKeys;

				UINT numScalingKeys = pNodeAnim->mNumScalingKeys;
				UINT numRotationKeys = pNodeAnim->mNumRotationKeys;

				for (UINT k = 0; k < pNodeAnim->mNumPositionKeys; ++k)
				{
					aiVector3D   aiScl(1, 1, 1);
					aiQuaternion aiRot(1, 0, 0, 0);
					aiVector3D   aiPos(0, 0, 0);

					// our keyframe
					Keyframe ourKeyframe;
					ourKeyframe.TimePos = k * timeDelta;

					// scaling
					if ( k < numScalingKeys )
						aiScl = pNodeAnim->mScalingKeys[k].mValue;

					ourKeyframe.Scale = XMFLOAT3(aiScl.x, aiScl.y, aiScl.z);

					// rotation
					if ( k < numRotationKeys )
						aiRot = pNodeAnim->mRotationKeys[k].mValue;

					ourKeyframe.RotationQuat = XMFLOAT4(aiRot.x, aiRot.y, aiRot.z, aiRot.w);

					// translation
					aiPos = pNodeAnim->mPositionKeys[k].mValue;

					ourKeyframe.Translation = XMFLOAT3(aiPos.x, aiPos.y, aiPos.z);

					// Another keyframe for this BoneAnimation
					ourBoneAnim.Keyframes.push_back(ourKeyframe);
				}

				// Another BoneAnimation for this AnimationClip
				clip.mBoneAnimations[jointIndex] = ourBoneAnim;
				clipInitialized[jointIndex]		 = true;
			}
		}

		//Insert identity animation for the root bone
		for (auto i = 0U; i < clipInitialized.size(); ++i)
		{
			if (clipInitialized[i] == false)
			{
				BoneAnimation ourBoneAnim;

				aiVector3D   aiScl(1, 1, 1);
				aiQuaternion aiRot(1, 0, 0, 0);
				aiVector3D   aiPos(0, 0, 0);

				// our keyframe
				Keyframe ourKeyframe;
				ourKeyframe.TimePos = 0;

				ourKeyframe.Scale = XMFLOAT3(aiScl.x, aiScl.y, aiScl.z);
				ourKeyframe.RotationQuat = XMFLOAT4(aiRot.x, aiRot.y, aiRot.z, aiRot.w);
				ourKeyframe.Translation = XMFLOAT3(aiPos.x, aiPos.y, aiPos.z);

				// Another keyframe for this BoneAnimation
				ourBoneAnim.Keyframes.push_back(ourKeyframe);

				clip.mBoneAnimations[i] = move(ourBoneAnim);
			}
		}

		// Another AnimationClip for the Scene
 		m_AnimationClips[clipName] = move(clip);

		m_ClipNames.push_back( clipName );
	}
}

void SceneBuilder::AddRenderItem(const string& geoName, const string& drawArgsName, const string& materialName, XMMATRIX& objScale, XMMATRIX& objRot, XMMATRIX& objPos, UINT alphaFunc, float tileU, float tileV)
{
	auto itNorth = make_unique<RenderItem>();

	XMStoreFloat4x4(&itNorth->mWorld, objScale * objRot * objPos);

	XMStoreFloat4x4(&itNorth->mTexTransform, XMMatrixScaling(tileU, tileV, 1.0f));

	itNorth->mObjCBIndex = m_ObjectCBIndex++;

	itNorth->mMat = m_Materials[materialName].get();
	itNorth->mGeo = m_Geometries[geoName].get();
	itNorth->mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	itNorth->mIndexCount = itNorth->mGeo->DrawArgs[drawArgsName].IndexCount;
	itNorth->mStartIndexLocation = itNorth->mGeo->DrawArgs[drawArgsName].StartIndexLocation;
	itNorth->mBaseVertexLocation = itNorth->mGeo->DrawArgs[drawArgsName].BaseVertexLocation;

	if (alphaFunc == TexturedAlphaClip)
		m_RenderItems[(int)RenderLayer::AlphaTested].push_back(itNorth.get());

	else if (alphaFunc == TexturedTransparent)
		m_RenderItems[(int)RenderLayer::Transparent].push_back(itNorth.get());

	else if (alphaFunc == NoTextures)
		m_RenderItems[(int)RenderLayer::Colour].push_back(itNorth.get());
	else
	{
		if (itNorth->mMat->NormalSrvHeapIndex == 1)
			m_RenderItems[(int)RenderLayer::Static].push_back(itNorth.get());
		else
			m_RenderItems[(int)RenderLayer::StaticBump].push_back(itNorth.get());
	}

	m_AllRenderItems.push_back(move(itNorth));
}

void SceneBuilder::AddMaterial(const string& matName, Mat& materialProperties, UINT diffSlot, UINT bumpSlot )
{
	auto icemirror = make_unique<Material>();
	icemirror->Name = matName;
	icemirror->MatCBIndex = m_MaterialCBIndex++;
	icemirror->DiffuseSrvHeapIndex = diffSlot;
	icemirror->NormalSrvHeapIndex = bumpSlot;
	icemirror->DiffuseAlbedo = materialProperties.DiffuseAlbedo;
	icemirror->FresnelR0 = materialProperties.FresnelR0;
	icemirror->Roughness = materialProperties.Roughness;

	m_Materials[matName] = move(icemirror);
}


int SceneBuilder::FormatFromFilename(const char* filename)
{
	const char* s_FormatString[] = { "None", "dds",	"tga", 	"bmp", 	"png", 	"jpg" };
	const char* p = strrchr(filename, '.');
	if (!p || *p == 0)
		return FormatNone;

	for (int n = 1; n < Formats; n++)
	{
		if (_stricmp(p + 1, s_FormatString[n]) == 0)
			return n;
	}

	return FormatNone;
}

#pragma region ASSIMP

constexpr unsigned int ImportFlags =
aiProcess_CalcTangentSpace |
aiProcess_Triangulate |
aiProcess_SortByPType |
aiProcess_PreTransformVertices |
aiProcess_JoinIdenticalVertices |
aiProcess_GenNormals |
aiProcess_GenUVCoords |
aiProcess_OptimizeMeshes |
aiProcess_Debone |
aiProcess_ValidateDataStructure;

constexpr unsigned int ImportFlagsSkinned =
aiProcess_CalcTangentSpace |
aiProcess_Triangulate |
aiProcess_SortByPType |
aiProcess_JoinIdenticalVertices |
aiProcess_GenNormals |
aiProcess_GenUVCoords |
aiProcess_OptimizeMeshes |
aiProcess_PopulateArmatureData |
aiProcess_ValidateDataStructure;

bool SceneBuilder::LoadStaticQlblModel(const std::string& fileDir, const std::string& filePath, std::vector<StaticMesh>& meshes_, const std::string& theModelName, DirectX::XMFLOAT4X4& theModelWorld, bool isClipped)
{
	//Use windows api here for no buffering
	HANDLE handle = CreateFileA(filePath.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING,
	FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, 0);
	if (handle != INVALID_HANDLE_VALUE)
	{
		DWORD fileSize = GetFileSize(handle, nullptr);

		if (fileSize == INVALID_FILE_SIZE)
		{
			CloseHandle(handle);
			return false;
		}

		std::vector<uint8_t> v;
		v.resize(fileSize);

		DWORD dwNumberOfBytesRead = 0;

		if (ReadFile(handle, &v[0], fileSize, &dwNumberOfBytesRead, 0) != FALSE)
		{
			const std::string materialPath					= std::string("Bundle/");
			const std::string ext							= ".DDS";
			const QLibrary::Geometry::StaticModel* model	= QLibrary::Geometry::GetStaticModel(&v[0]);
			const auto& materials							= model->material();
			std::vector<string>								material_names;
			const auto mmt									= model->mesh_material_table();

			for (const auto& m : *materials)
			{
				auto m0	= std::make_unique<Material>();

				const string dt				= m->diffuse()->c_str();
				const string normal			= m->normal()->c_str();
				const string specular		= m->specular()->c_str();
				const string glossiness		= m->glossiness()->c_str();

				BuildTextureResource(dt.c_str(),			std::string(materialPath + dt + ext).c_str(),			TextureType::Mesh);
				m0->DiffuseSrvHeapIndex = m_NextSrvHeapSlot++;

				BuildTextureResource(normal.c_str(),		std::string(materialPath + normal + ext).c_str(),		TextureType::Mesh);
				m0->NormalSrvHeapIndex = m_NextSrvHeapSlot++;
				
				BuildTextureResource(specular.c_str(),		std::string(materialPath + specular + ext).c_str(),		TextureType::Mesh);
				m0->SpecularSrvHeapIndex = m_NextSrvHeapSlot++;

				BuildTextureResource(glossiness.c_str(),	std::string(materialPath + glossiness + ext).c_str(),	TextureType::Mesh);
				m0->GlossinessSrvHeapIndex = m_NextSrvHeapSlot++;

				m0->Name					= dt + normal + specular + glossiness;
				m0->MatCBIndex				= m_MaterialCBIndex++;

				m0->DiffuseAlbedo			= XMFLOAT4(1, 1, 1, 0);
				m0->FresnelR0				= XMFLOAT3(0.04f, 0.04f, 0.04f);	//dielectrics
				m0->Roughness				= 0.99f;
				m0->MatTransform			= Maths::Identity4x4();

				material_names.push_back(m0->Name);

				m_Materials[m0->Name]		= std::move(m0);
			}

			const auto& meshes				= model->mesh();

			//interleave again, not needed actually, but refactoring for the renderer is needed, so it can work with deinterleaved data
			//one needs positions split from the other attributes, to have less cache misses on the gpu
			//when doing zprepass for example
			//also compressions will be much easier

			uint32_t i = 0;

			for (const auto& m : *meshes)
			{
				StaticMesh sm;

				sm.mMeshName		= filePath + std::to_string(i);
				sm.mModelWorld		= theModelWorld;
				sm.mAlpha			= AlphaFunc::TexturedNoClip;
				sm.mMaterialName	= material_names[mmt->Get(i)->material()];

				const QLibrary::Geometry::StaticMeshBounds* min_b = model->bounds()->Get(i);

				XMVECTOR vMin = DirectX::XMLoadFloat3(reinterpret_cast<const XMFLOAT3 *>(&min_b->min()));
				XMVECTOR vMax = DirectX::XMLoadFloat3(reinterpret_cast<const XMFLOAT3*>(&min_b->max()));

				// Bounds
				DirectX::BoundingBox bounds;
				DirectX::XMStoreFloat3(&bounds.Center, 0.5f * (vMin + vMax));
				DirectX::XMStoreFloat3(&bounds.Extents, 0.5f * (vMax - vMin));
				sm.mBounds = bounds;

				for (uint32_t i = 0; i < m->positions()->size(); ++i)
				{
					Vertex	   v;

					const QLibrary::Geometry::Point3*  p = m->positions()->Get(i);
					const QLibrary::Geometry::Point2*  u = m->uv()->Get(i);
					const QLibrary::Geometry::Vector3* t = m->tangents()->Get(i);
					const QLibrary::Geometry::Vector3* n = m->normals()->Get(i);

					v.Pos.x = p->x();
					v.Pos.y = p->y();
					v.Pos.z = p->z();

					v.Normal.x = n->x();
					v.Normal.y = n->y();
					v.Normal.z = n->z();

					v.TexC.x = u->x();
					v.TexC.y = u->y();

					v.TangentU.x = t->x();
					v.TangentU.y = t->y();
					v.TangentU.z = t->z();

					sm.mVertices.push_back(v);
				}

				for (uint32_t i = 0; i < m->triangles()->size(); ++i)
				{
					const QLibrary::Geometry::Triangle32Bit* t = m->triangles()->Get(i);

					sm.mIndices.push_back(t->i0());
					sm.mIndices.push_back(t->i1());
					sm.mIndices.push_back(t->i2());
				}

				meshes_.push_back(std::move(sm));

				i++;
			}

			CloseHandle(handle);
			return true;
		}
		else
		{
			CloseHandle(handle);
			return false;
		}

	}
		
	return true;
}

bool SceneBuilder::LoadStaticAssImpModel(string filePath, vector<StaticMesh>& meshes, string theModelName, XMFLOAT4X4& theModelWorld, bool isClipped)
{
	Assimp::Importer importer;

#ifdef ENABLE_ASSIMP_LOGGER
	if (Assimp::DefaultLogger::isNullLogger())
		Assimp::DefaultLogger::create(NULL, Assimp::Logger::NORMAL); // Assimp::Logger::VERBOSE
#endif

	m_pAiScene = importer.ReadFile(filePath, ImportFlags);

	if (m_pAiScene == NULL)
		return false;

	m_AiNode = 0;

	ProcessStaticNode(m_pAiScene->mRootNode, meshes, theModelName, theModelWorld, isClipped);

	return true;
}

void SceneBuilder::ProcessStaticNode(aiNode* node, vector<StaticMesh>& meshes, string theModelName, XMFLOAT4X4& theModelWorld, bool alphaClipped)
{
	for (UINT meshId = 0; meshId < node->mNumMeshes; meshId++)
	{
		aiMesh* pMesh = m_pAiScene->mMeshes[node->mMeshes[meshId]];

		StaticMesh thisMesh;
		string meshName = pMesh->mName.data;
		thisMesh.mMeshName = to_string(m_AiNode) + "_" + theModelName + meshName; //theModelName + "_" + meshName + "_" + to_string(m_NodeID);
		thisMesh.mModelWorld = theModelWorld;
		thisMesh.mAlpha = AlphaFunc::TexturedNoClip;

		ProcessStaticMesh(meshId, pMesh, thisMesh);

		meshes.push_back(thisMesh);

		m_AiNode++;
	}

	for (UINT i = 0; i < node->mNumChildren; i++)
	{
		ProcessStaticNode(node->mChildren[i], meshes, theModelName, theModelWorld, alphaClipped);
	}
}

void SceneBuilder::ProcessStaticMesh(UINT meshId, aiMesh* pMesh, StaticMesh& theMesh)
{
	vector<Vertex>   vertices;
	vector<UINT>     indices;

	XMFLOAT3 vMinf3(+Maths::Infinity, +Maths::Infinity, +Maths::Infinity);
	XMFLOAT3 vMaxf3(-Maths::Infinity, -Maths::Infinity, -Maths::Infinity);

	XMVECTOR vMin = XMLoadFloat3(&vMinf3);
	XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

	if (pMesh->mMaterialIndex >= 0)
	{
		aiMaterial* mat = m_pAiScene->mMaterials[pMesh->mMaterialIndex];

		if (m_TexStr.empty())
			m_TexStr = DetermineTextureType(mat);
	}

	// Vertices
	for (UINT v = 0; v < pMesh->mNumVertices; ++v)
	{
		Vertex vertex;
		// Position, Normal, TexCoord, Tangent
		vertex.Pos.x = pMesh->mVertices[v].x;
		vertex.Pos.y = pMesh->mVertices[v].y;
		vertex.Pos.z = pMesh->mVertices[v].z;
		vertex.Normal.x = pMesh->mNormals[v].x;
		vertex.Normal.y = pMesh->mNormals[v].y;
		vertex.Normal.z = pMesh->mNormals[v].z;

		if (pMesh->mTextureCoords[0])
		{
			vertex.TexC.x = (float)pMesh->mTextureCoords[0][v].x;
			vertex.TexC.y = (float)pMesh->mTextureCoords[0][v].y;
		}

#ifdef USE_BITANGENTS_FOR_STATIC_MODELS
		vertex.TangentU.x = pMesh->mBitangents[v].x;
		vertex.TangentU.y = pMesh->mBitangents[v].y;
		vertex.TangentU.z = pMesh->mBitangents[v].z;
#else
		vertex.TangentU.x = pMesh->mTangents[v].x;
		vertex.TangentU.y = pMesh->mTangents[v].y;
		vertex.TangentU.z = pMesh->mTangents[v].z;
#endif

		vertices.push_back(vertex);

		// Bounds
		XMVECTOR P = XMLoadFloat3(&vertices[v].Pos);

		vMin = XMVectorMin(vMin, P);
		vMax = XMVectorMax(vMax, P);
	}

	theMesh.mVertices = vertices;

	BoundingBox bounds;
	XMStoreFloat3(&bounds.Center, 0.5f * (vMin + vMax));
	XMStoreFloat3(&bounds.Extents, 0.5f * (vMax - vMin));
	theMesh.mBounds = bounds;

	// Indices
	for (UINT i = 0; i < pMesh->mNumFaces; ++i)
	{
		aiFace face = pMesh->mFaces[i];

		for (UINT j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}

	theMesh.mIndices = indices;

	if (pMesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = m_pAiScene->mMaterials[pMesh->mMaterialIndex];

		UINT textureCount;
		bool transparent;
		theMesh.mMaterialName = LoadMaterialTextures(meshId, material, textureCount, transparent);
		if (transparent)
			theMesh.mAlpha = AlphaFunc::TexturedTransparent;
		else
			theMesh.mAlpha = textureCount == 0 ? AlphaFunc::NoTextures : AlphaFunc::TexturedNoClip;
	}
}

string SceneBuilder::LoadMaterialTextures(UINT meshId, aiMaterial* pMat, UINT& textureCount, bool& isTransparent)
{
	aiString name = pMat->GetName();
	string matName = to_string(meshId) + "_" + name.C_Str();

	bool skipMaterial = m_Materials.find(matName) != end(m_Materials);

	if (!skipMaterial && m_TexStr != "embedded compressed texture")
	{
		string		diffuseFilename, normalFilename;
		bool		hasDiff = false;
		bool		hasBump = false;
		aiString	str;

		auto		theMat = make_unique<Material>();

		// Textures
		pMat->GetTexture(aiTextureType_DIFFUSE, 0, &str);

		//if ( m_TextureType == "embedded compressed texture" )
		//{
		//		int textureindex = GetTextureIndex(&str);
		//		CreateTextureFromModel( scene, textureindex );
		//		// etc...
		//}

		textureCount = aiGetMaterialTextureCount(pMat, aiTextureType_DIFFUSE); // UINT anotherCount = aiGetMaterialTextureCount(pMat, aiTextureType_DISPLACEMENT);
		//UINT normalTextureCount = aiGetMaterialTextureCount(pMat, aiTextureType_NORMALS);
		//UINT displacementTextureCount = aiGetMaterialTextureCount(pMat, aiTextureType_DISPLACEMENT);

		UINT	sharedTextureIndex = 0;
		size_t	start;

		if (textureCount != 0)
		{
			// Texture filename
			diffuseFilename = string(str.C_Str());
			start = diffuseFilename.find_last_of("\\") + 1;
			if (start == 0)
				start = diffuseFilename.find_last_of("/") + 1;

			diffuseFilename = diffuseFilename.substr(start, diffuseFilename.size());
			size_t ext = diffuseFilename.size() - diffuseFilename.find_last_of(".");
			string newName = diffuseFilename.substr(0, diffuseFilename.size() - ext);

			bool skipTexture = m_Textures.find(newName) != end(m_Textures);

			if (!skipTexture)
			{
				// Diffuse filename

				size_t sz = diffuseFilename.find(".tif");
				if (sz != -1)
					diffuseFilename.replace(sz, 4, ".png");

				sz = diffuseFilename.find(".psd");
				if (sz != -1)
					diffuseFilename.replace(sz, 4, ".png");

				hasDiff = true;
				if (diffuseFilename == "")		// this can happen with some meshes (e.g. the GalleryRoom ceiling mesh)
					diffuseFilename = "Ceiling_White.png"; // fudge

				// Diffuse Texture
				BuildTextureResource(newName.c_str(), diffuseFilename.c_str(), TextureType::Mesh);

				// Bump filename
				str.Clear();
				pMat->GetTexture(aiTextureType_NORMALS, 0, &str);
				normalFilename = string(str.C_Str());
				start = normalFilename.find_last_of("\\") + 1;
				if (start == 0)
					start = normalFilename.find_last_of("/") + 1;

				normalFilename = normalFilename.substr(start, normalFilename.size());
				ext = normalFilename.size() - normalFilename.find_last_of(".");
				string newBumpName = normalFilename.substr(0, normalFilename.size() - ext);

				// Bump Texture
				if (normalFilename != "")
				{
					hasBump = true;
					sz = normalFilename.find(".tif");
					if (sz != -1)
						normalFilename.replace(sz, 4, ".png");

					sz = normalFilename.find(".psd");
					if (sz != -1)
						normalFilename.replace(sz, 4, ".png");

					BuildTextureResource(newBumpName.c_str(), normalFilename.c_str(), TextureType::Mesh);
				}
				else if (MEDIEVAL_MODELS_BUMPED)
				{
					newBumpName = newName + "_N";
					string bumpFilename = newBumpName + ".tga";
					if (TextureExists(bumpFilename))
					{
						hasBump = true;
						BuildTextureResource(newBumpName.c_str(), bumpFilename.c_str(), TextureType::Mesh);
					}
				}

				// Specular filename
				str.Clear();
				pMat->GetTexture(aiTextureType_SPECULAR, 0, &str);
				string specularFilename = string(str.C_Str());
				start = specularFilename.find_last_of("\\") + 1;
				specularFilename = specularFilename.substr(start, specularFilename.size());
				string specName = specularFilename.substr(0, specularFilename.size() - ext);

				bool hasSpec = !specularFilename.empty();

				// Specular texture
				if ( hasSpec )
					BuildTextureResource(specName.c_str(), specularFilename.c_str(), TextureType::Mesh);

				// Materiel
				isTransparent = BuildMeshMaterial(matName, pMat, theMat.get(), hasDiff, hasBump, hasSpec );
			}
			else
			{
				sharedTextureIndex = m_SrvDebugHeapSlots[newName];
				isTransparent = BuildMeshSharedMaterial(matName, pMat, theMat.get(), sharedTextureIndex);
			}
		}
		else
		{
			isTransparent = BuildMeshMaterial(matName, pMat, theMat.get(), false, false);
		}

		m_Materials[matName] = move(theMat);

	}

	return matName;
}

bool SceneBuilder::TextureExists(string fileName)
{
	string materialsPath = m_LocalPath + "/Materials/" + fileName;
	wstring pathWide = MODEL_PATH_WIDE + wstring(materialsPath.begin(), materialsPath.end());

	struct _stat64 fileStat;
	int fileExists = _wstat64(pathWide.c_str(), &fileStat);

	return fileExists != -1;
}

bool SceneBuilder::BuildMeshMaterial(string matName, aiMaterial* pMat, Material* pOurMat, bool hasDiff, bool hasBump, bool hasSpecular)
{
	aiColor3D diffuse(1.0f, 1.0f, 1.0f);
	aiColor3D specular(1.0f, 1.0f, 1.0f);
	aiColor3D ambient(1.0f, 1.0f, 1.0f);
	aiColor3D emissive(0.0f, 0.0f, 0.0f);
	aiColor3D transparent(1.0f, 1.0f, 1.0f);
	aiColor3D reflective(0.0f, 0.0f, 0.0f);
	float opacity = 1.0f;
	float shininess = 0.99f;
	float specularStrength = 1.0f;
	float transparencyFactor = 0.0f;
	UINT blendFunc;

	//															// Glass:
	pMat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);				// {r=0.588000000 g=0.588000000 b=0.588000000 }
	pMat->Get(AI_MATKEY_COLOR_SPECULAR, specular);				// {r=0.899999976 g=0.899999976 b=0.899999976 }
	pMat->Get(AI_MATKEY_COLOR_AMBIENT, ambient);				// {r=0.588000000 g=0.588000000 b=0.588000000 }
	pMat->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);				// {r=0.000000000 g=0.000000000 b=0.000000000 }
	pMat->Get(AI_MATKEY_COLOR_TRANSPARENT, transparent);		// {r=0.639999986 g=0.639999986 b=0.639999986 } , normally {r=1.0 g=1.0 b=1.0 }
	pMat->Get(AI_MATKEY_COLOR_REFLECTIVE, reflective);			// 
	pMat->Get(AI_MATKEY_OPACITY, opacity);						// 0.360000014 ( transparent.r = 1.0 - opacity )  
	pMat->Get(AI_MATKEY_SHININESS, shininess);					// 2.0 
	pMat->Get(AI_MATKEY_SHININESS_STRENGTH, specularStrength);	// 0.649999976 
	pMat->Get(AI_MATKEY_BLEND_FUNC, blendFunc);			    // aiBlendMode_Default = 0x0, aiBlendMode_Additive = 0x1, not used returns 0xCCCCCCCC
	pMat->Get(AI_MATKEY_TRANSPARENCYFACTOR, transparencyFactor);// 0.639999986, normally 0.0

	pOurMat->Name = matName;
	pOurMat->MatCBIndex = m_MaterialCBIndex++;

	if (!hasDiff)
		pOurMat->DiffuseSrvHeapIndex = 0;
	else
		pOurMat->DiffuseSrvHeapIndex = m_NextSrvHeapSlot++;

	if (!hasBump)
		pOurMat->NormalSrvHeapIndex = 1;
	else
		pOurMat->NormalSrvHeapIndex = m_NextSrvHeapSlot++;

	if (!hasSpecular)
		pOurMat->SpecularSrvHeapIndex = 1;
	else
		pOurMat->SpecularSrvHeapIndex = m_NextSrvHeapSlot++;

	if (diffuse.r == 0 && diffuse.g == 0 && diffuse.b == 0)
		pOurMat->DiffuseAlbedo = XMFLOAT4(1, 1, 1, opacity);
	else
		pOurMat->DiffuseAlbedo = XMFLOAT4(diffuse.r, diffuse.g, diffuse.b, opacity);

	pOurMat->FresnelR0 = XMFLOAT3(0.02f, 0.02f, 0.02f);
	pOurMat->Roughness = 0.99f; // TODO
	//if (shininess == 0.0f)
	//	pOurMat->Roughness = 0.99f;
	//else
	//	pOurMat->Roughness = (256.0f - shininess) / 256.0f;

	pOurMat->MatTransform = Maths::Identity4x4();

	assert(m_NextSrvHeapSlot < m_MaxDescriptors);

	return opacity < 1.0f;
}

bool SceneBuilder::BuildMeshSharedMaterial(string matName, aiMaterial* pMat, Material* pOurMat, UINT sharedTextureIndex)
{
	aiColor3D diffuse(1.0f, 1.0f, 1.0f);
	aiColor3D specular(1.0f, 1.0f, 1.0f);
	aiColor3D ambient(1.0f, 1.0f, 1.0f);
	aiColor3D emissive(0.0f, 0.0f, 0.0f);
	aiColor3D transparent(1.0f, 1.0f, 1.0f);
	aiColor3D reflective(0.0f, 0.0f, 0.0f);
	float opacity = 1.0f;
	float shininess = 0.0f;
	float specularStrength = 1.0f;
	float transparencyFactor = 0.0f;
	UINT blendFunc;

	//															// Glass:
	pMat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);				// {r=0.588000000 g=0.588000000 b=0.588000000 }
	pMat->Get(AI_MATKEY_COLOR_SPECULAR, specular);				// {r=0.899999976 g=0.899999976 b=0.899999976 }
	pMat->Get(AI_MATKEY_COLOR_AMBIENT, ambient);				// {r=0.588000000 g=0.588000000 b=0.588000000 }
	pMat->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);				// {r=0.000000000 g=0.000000000 b=0.000000000 }
	pMat->Get(AI_MATKEY_COLOR_TRANSPARENT, transparent);		// {r=0.639999986 g=0.639999986 b=0.639999986 } , normally {r=1.0 g=1.0 b=1.0 }
	pMat->Get(AI_MATKEY_COLOR_REFLECTIVE, reflective);			// 
	pMat->Get(AI_MATKEY_OPACITY, opacity);						// 0.360000014 ( transparent.r = 1.0 - opacity )  
	pMat->Get(AI_MATKEY_SHININESS, shininess);					// 2.0 
	pMat->Get(AI_MATKEY_SHININESS_STRENGTH, specularStrength);	// 0.649999976 
	pMat->Get(AI_MATKEY_BLEND_FUNC, blendFunc);			    // aiBlendMode_Default = 0x0, aiBlendMode_Additive = 0x1, not used returns 0xCCCCCCCC
	pMat->Get(AI_MATKEY_TRANSPARENCYFACTOR, transparencyFactor);// 0.639999986, normally 0.0

	pOurMat->Name = matName;
	pOurMat->MatCBIndex = m_MaterialCBIndex++;

	pOurMat->DiffuseSrvHeapIndex = sharedTextureIndex;
	pOurMat->NormalSrvHeapIndex = 1;

	pOurMat->DiffuseAlbedo = XMFLOAT4(diffuse.r, diffuse.g, diffuse.b, opacity);
	pOurMat->FresnelR0 = XMFLOAT3(0.02f, 0.02f, 0.02f);
	if (shininess == 0.0f)
		pOurMat->Roughness = 0.99f;
	else
		pOurMat->Roughness = (256.0f - shininess) / 256.0f;

	pOurMat->MatTransform = Maths::Identity4x4();

	return opacity < 1.0f;
}

string SceneBuilder::DetermineTextureType(aiMaterial* mat)
{
	aiString textypeStr;
	mat->GetTexture(aiTextureType_DIFFUSE, 0, &textypeStr);
	string textypeteststr = textypeStr.C_Str();
	if (textypeteststr == "*0" || textypeteststr == "*1" || textypeteststr == "*2" || textypeteststr == "*3" || textypeteststr == "*4" || textypeteststr == "*5")
	{
		if (m_pAiScene->mTextures[0]->mHeight == 0)
			return "embedded compressed texture";
		else
			return "embedded non-compressed texture";
	}
	if (textypeteststr.find('.') != string::npos)
		return "textures are on disk";

	return ".";
}

int SceneBuilder::GetTextureIndex(aiString* str)
{
	string tistr;

	tistr = str->C_Str();

	tistr = tistr.substr(1);

	return stoi(tistr);
}

void SceneBuilder::ConvertMatrix4x4(XMFLOAT4X4& ourMatrix, aiMatrix4x4& theirMatrix)
{
	memcpy(&ourMatrix, &theirMatrix, sizeof(ourMatrix));
}

void SceneBuilder::CreateTextureFromModel(int textureindex)
{
	int* size = reinterpret_cast<int*>(&m_pAiScene->mTextures[textureindex]->mWidth);

	auto pTex = make_unique<Texture>();

	ThrowIfFailed(CreateDDSTextureFromMemory12(m_pDevice, m_pCommandList, reinterpret_cast<unsigned char*>(m_pAiScene->mTextures[textureindex]->pcData), *size, pTex->Resource, pTex->UploadHeap));

	// CreateWICTextureFromMemory12( m_pDevice, m_pCommandList, reinterpret_cast<unsigned char*>(scene->mTextures[textureindex]->pcData), *size, pTex->Resource, pTex->UploadHeap ); TODO
}

#pragma region Skinned

bool SceneBuilder::LoadSkinnedAssImpModel( string filePath, vector<SkinnedMesh>& meshes, string theModelName, XMFLOAT4X4& theModelWorld, bool isClipped )
{
	Assimp::Importer importer;

#ifdef ENABLE_ASSIMP_LOGGER
	if ( Assimp::DefaultLogger::isNullLogger())
        Assimp::DefaultLogger::create( NULL, Assimp::Logger::NORMAL ); // Assimp::Logger::VERBOSE
#endif
	
	// ASSIMP Import skinned model
	m_pAiScene = importer.ReadFile( filePath, ImportFlagsSkinned );

	if ( m_pAiScene == NULL )
		return false;

	// Process meshes and materials ~ recursively
	ProcessSkinnedNode( m_pAiScene->mRootNode, meshes, theModelName, theModelWorld, isClipped );

	BuildSceneNodes(m_pAiScene->mRootNode);

	BuildSceneHierarhcy(m_pAiScene->mRootNode, -1);

	BuildSceneGlobalTransforms();

	BuildJoints(m_pAiScene->mMeshes[0], &meshes[0]);

	// Build and upload the mesh geometry to the gpu
	BuildSkinnedModelGeometry( theModelName, meshes );

	// Process animations
	ProcessAnimationClips( theModelName );

	return true;
}

void SceneBuilder::BuildSceneGlobalTransforms()
{
	m_SceneGlobalTransforms.resize(m_SceneHierarchy.size());
	m_SceneGlobalTransforms[0] = m_SceneLocalTransforms[0];

	for (auto i = 1; i < m_SceneHierarchy.size(); ++i)
	{
		UINT	   parent = m_SceneHierarchy[i];
		XMFLOAT4X4 p	  = m_SceneGlobalTransforms[parent];
		XMFLOAT4X4 t	  = m_SceneLocalTransforms[i];
		XMMATRIX   mp	  = XMLoadFloat4x4(&p);
		XMMATRIX   mt	  = XMLoadFloat4x4(&t);
		XMMATRIX   mg	  = XMMatrixMultiply(mp, mt);
		XMStoreFloat4x4(&m_SceneGlobalTransforms[i], mg);
	}
}

void SceneBuilder::BuildJoints(aiMesh* aMesh, SkinnedMesh* model )
{
	//Extract only nodes and their parents that are part of the bones
	//For all nodes that are not animated, do an identity offset matrix

	int32_t root = 255;

	vector<aiNode*> armatures;
	{
		aiBone* b		= aMesh->mBones[0];
		aiNode* r		= b->mArmature; 
		armatures.push_back(r);
		while (r->mParent != nullptr)
		{
			r = r->mParent;
			armatures.push_back(r);
		}

		root			= m_SceneNodes[armatures.back()->mName.C_Str()];
	}

	//For animation mapping
	m_skeletonLoading.m_sceneToJoint.clear();
	m_skeletonLoading.m_jointToScene.clear();

	auto& jointToScene = m_skeletonLoading.m_jointToScene;
	auto& sceneToJoint = m_skeletonLoading.m_sceneToJoint;

	{
		int32_t index = 0;
		for (auto&& it = armatures.rbegin(); it != armatures.rend(); it = it + 1)
		{
			int32_t sceneIndex = m_SceneNodes[(*it)->mName.C_Str()];
			sceneToJoint.insert(make_pair(sceneIndex, index++));
		}

		for (auto i = 0U; i < aMesh->mNumBones; ++i)
		{
			aiBone* b = aMesh->mBones[i];
			int32_t	sceneIndex = m_SceneNodes[b->mName.C_Str()];
			sceneToJoint.insert(make_pair(sceneIndex, index + i ));
		}

		//Process bones and find a bone with parent, not in the index
		for (auto i = 0U; i < aMesh->mNumBones; ++i)
		{
			aiBone* b			   = aMesh->mBones[i];
			int8_t	sceneIndex	   = m_SceneNodes[b->mName.C_Str()];
			int8_t parent		   = m_SceneHierarchy[sceneIndex];

			if (parent != -1)
			{
				const auto& it = sceneToJoint.find(parent);
				if (it == sceneToJoint.end())
				{
					sceneToJoint.insert(make_pair(parent, static_cast<int8_t>(sceneToJoint.size() + 1)));
				}
			}
		}
	}

	//Move them sequentially
	{
		auto i = 0;
		for (auto & p : sceneToJoint)
		{
			p.second = i++;
		}
	}

	{
		auto index = 0;
		for (auto&& sc : sceneToJoint)
		{
			jointToScene.insert(make_pair(sc.second, sc.first));
		}
	}

	vector<int8_t>		jointHierarchy;
	{
		uint32_t index = 0;
		for (auto&& it = armatures.rbegin(); it != armatures.rend(); it = it + 1)
		{
			auto sceneIndex = m_SceneNodes[(*it)->mName.C_Str()];
			jointToScene.insert(make_pair(index++, sceneIndex));
		}
	}

	for (auto i = 0U; i < aMesh->mNumBones; ++i)
	{
		aiBone* b				= aMesh->mBones[i];
		uint32_t	sceneIndex	= m_SceneNodes[b->mName.C_Str()];
		uint32_t    jointIndex  = sceneToJoint[sceneIndex];

		jointToScene.insert(make_pair( jointIndex, sceneIndex));
	}

	for (auto& p : jointToScene)
	{
		uint32_t parent			= m_SceneHierarchy[p.second];
		const auto& it			= sceneToJoint.find(parent);
		if ( it != sceneToJoint.end())
		{
			uint32_t jointParent = sceneToJoint[parent];
			jointHierarchy.push_back(jointParent);
		}
		else
		{
			if (parent == -1)
			{
				jointHierarchy.push_back(-1);
			}
		}
	}

	m_skeleton.m_hierarchy = move(jointHierarchy);
	m_skeleton.m_offsetMatrix.resize(m_skeleton.m_hierarchy.size());

	for ( auto i = 0U; i < m_skeleton.m_hierarchy.size(); ++i)
	{
		XMStoreFloat4x4(&m_skeleton.m_offsetMatrix[i], XMMatrixIdentity());
	}

	//Now Joint Indices contain remapped tree, cut out from the scene other nodes
	for (auto i = 0U; i < aMesh->mNumBones; ++i)
	{
		aiBone* b = aMesh->mBones[i];
		uint32_t	sceneIndex = m_SceneNodes[b->mName.C_Str()];
		uint32_t    jointIndex = sceneToJoint[sceneIndex];

		ConvertMatrix4x4(m_skeleton.m_offsetMatrix[jointIndex], b->mOffsetMatrix);
	}

	//Now Joint Indices contain remapped tree, cut out from the scene other nodes
	for (auto i = 0U; i < aMesh->mNumBones; ++i)
	{
		aiBone* b			   = aMesh->mBones[i];
		uint32_t	sceneIndex = m_SceneNodes[b->mName.C_Str()];
		uint32_t    jointIndex = sceneToJoint[sceneIndex];

		for (auto w = 0U; w < b->mNumWeights; ++w)
		{
			aiVertexWeight* vw  = b->mWeights + w;
	
			SkinnedVertex& vtx = model->mVertices[vw->mVertexId];

			if (vtx.BoneWeights.x  == 0.0f)
			{
				vtx.BoneWeights.x  = vw->mWeight;
				vtx.BoneIndices[0] = jointIndex;
			}
			else if (vtx.BoneWeights.y == 0.0f)
			{
				vtx.BoneWeights.y = vw->mWeight;
				vtx.BoneIndices[1] = jointIndex;
			}
			else if (vtx.BoneWeights.z == 0.0f)
			{
				vtx.BoneWeights.z = vw->mWeight;
				vtx.BoneIndices[2] = jointIndex;
			}
			else if (vtx.BoneWeights.w == 0.0f)
			{
				vtx.BoneWeights.w = vw->mWeight;
				vtx.BoneIndices[3] = jointIndex;
			}
		}
	}
}

void SceneBuilder::BuildSceneNodes(aiNode* sceneRoot)
{
	m_Scene.push_back(sceneRoot);
	for (UINT i = 0; i < sceneRoot->mNumChildren; i++)
	{
		BuildSceneNodes(sceneRoot->mChildren[i]);
	}
}

void SceneBuilder::BuildSceneHierarhcy(aiNode* parent, int32_t parentIndex)
{
	int32_t children = 0;
	BuildSceneHierarhcy(parent, parentIndex, &children);
}

void SceneBuilder::BuildSceneHierarhcy(aiNode* parent, int32_t parentIndex, int32_t* childIndex)
{
	m_SceneHierarchy.resize(*childIndex + 1);
	m_SceneLocalTransforms.resize(*childIndex + 1);

	m_SceneHierarchy[*childIndex]		= parentIndex;
	aiMatrix4x4 transform				= parent->mTransformation;

	XMFLOAT4X4 t;
	ConvertMatrix4x4(t, transform);
	m_SceneLocalTransforms[*childIndex] = t;
	m_SceneNodes.insert(make_pair(string(parent->mName.C_Str()), *childIndex));

	UINT c = *childIndex;

	// Children
	for (UINT i = 0; i < parent->mNumChildren; i++)
	{
		*childIndex = *childIndex+1;
		BuildSceneHierarhcy(parent->mChildren[i], c, childIndex);
	}
}

void SceneBuilder::ProcessSkinnedNode( aiNode* pNode, vector<SkinnedMesh>& meshes, string theModelName, XMFLOAT4X4& theModelWorld, bool alphaClipped )
{
	for ( UINT meshIndex = 0 ; meshIndex < pNode->mNumMeshes ; meshIndex++ )
	{
		aiMesh* pMesh = m_pAiScene->mMeshes[ pNode->mMeshes[meshIndex] ];
		
		SkinnedMesh thisMesh;
		string meshName = pMesh->mName.data;
		
		thisMesh.mMeshName = to_string(m_AiNode) + "_" + theModelName + meshName;
		thisMesh.mModelWorld = theModelWorld;

		ProcessSkinnedMesh( meshIndex, pMesh, thisMesh );

		meshes.push_back( thisMesh );

		m_AiNode++;
	}

	for ( UINT i = 0 ; i < pNode->mNumChildren ; i++ )
		ProcessSkinnedNode( pNode->mChildren[i], meshes,  theModelName, theModelWorld, alphaClipped );
}

void SceneBuilder::ProcessSkinnedMesh( UINT meshIndex, aiMesh* aMesh, SkinnedMesh& mesh )
{
	vector<SkinnedVertex>   vertices;
	vector<USHORT>			indices;

	// Bounds
	XMFLOAT3 vMinf3(+Maths::Infinity, +Maths::Infinity, +Maths::Infinity);
	XMFLOAT3 vMaxf3(-Maths::Infinity, -Maths::Infinity, -Maths::Infinity);

	XMVECTOR vMin = XMLoadFloat3(&vMinf3);
	XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

	// Vertices
	for ( UINT v = 0 ; v < aMesh->mNumVertices ; v++ )
	{
		SkinnedVertex vertex;
		// Position, Normal, TexCoord, Tangent
		vertex.Pos.x = aMesh->mVertices[ v ].x;
		vertex.Pos.y = aMesh->mVertices[ v ].y;
		vertex.Pos.z = aMesh->mVertices[ v ].z;
		vertex.Normal.x =aMesh->mNormals[ v ].x;
		vertex.Normal.y = aMesh->mNormals[ v ].y;
		vertex.Normal.z = aMesh->mNormals[ v ].z;

		if (aMesh->mTextureCoords[0] != nullptr )
		{
			vertex.TexC.x = (float)aMesh->mTextureCoords[0][ v ].x;
			vertex.TexC.y = (float)aMesh->mTextureCoords[0][ v ].y;
		}
		if (aMesh->mTangents != nullptr )
		{
			vertex.TangentU.x = aMesh->mTangents[ v ].x;
			vertex.TangentU.y = aMesh->mTangents[ v ].y;
			vertex.TangentU.z = aMesh->mTangents[ v ].z;
		}

		vertices.push_back( vertex );

		// Bounds
		XMVECTOR P = XMLoadFloat3( &vertices[v].Pos );

		vMin = XMVectorMin(vMin, P);
		vMax = XMVectorMax(vMax, P);
	}

	mesh.mVertices = vertices;
	// Bounds
	BoundingBox bounds;
	XMStoreFloat3(&bounds.Center, 0.5f*(vMin + vMax));
	XMStoreFloat3(&bounds.Extents, 0.5f*(vMax - vMin));

	mesh.mBounds = bounds;

	// Indices
	for ( UINT i = 0; i < aMesh->mNumFaces; i++ )
	{
		aiFace face = aMesh->mFaces[i];

		for ( UINT j = 0 ; j < face.mNumIndices ; j++ )
			indices.push_back( face.mIndices[j] );
	}

	mesh.mIndices = indices;

	// Materials
	if (aMesh->mMaterialIndex >= 0 )
	{
		aiMaterial* material = m_pAiScene->mMaterials[aMesh->mMaterialIndex];
		if ( m_TexStr.empty() ) 
			m_TexStr = DetermineTextureType( material );

		UINT textureCount;
		bool transparent;
		mesh.mMaterialName = LoadMaterialTextures( meshIndex, material, textureCount, transparent );
		//theMesh.mHasTextures = textureCount != 0;
		
		aiUVTransform transform;
		material->Get( AI_MATKEY_UVTRANSFORM_DIFFUSE(0), transform );
		float scaleU = transform.mScaling.x;
		float scaleV = transform.mScaling.y;
		XMStoreFloat4x4( &mesh.mTextureTransform, XMMatrixScaling( scaleU, scaleV, 1.0f ) );

		//unsigned int  index = 0;
		//int mapping_ = static_cast<int>(aiTextureMapping_UV);	
		//aiGetMaterialInteger( pMat, AI_MATKEY_MAPPING( aiTextureMapping_UV,index ), &mapping_);
		//aiTextureMapping mapping = static_cast<aiTextureMapping>(mapping_); // aiTextureMapping_UV, aiTextureMapping_SPHERE, aiTextureMapping_CYLINDER...

		
	}
}

#pragma endregion

#pragma endregion

#pragma region Geometry Helpers

void SceneBuilder::BuildWallsWithWindows(float xPos, UINT rightCount, UINT southCount, float w, string wallTexfile, string bumpFile, string windowTexfile, float transparency)
{
	const float h = 3.0f * w / 4.0f;

	array<Vertex, 24> vertices =
	{
		Vertex(-w / 2,     h / 2, 0, 0, 0, -1,   0, 0.4f), // 0 ~ left panel
		Vertex(-w / 2, 5 * h / 6, 0, 0, 0, -1,   0,   0),
		Vertex(3 * -w / 8, 5 * h / 6, 0, 0, 0, -1, 0.25,   0),
		Vertex(3 * -w / 8,     h / 2, 0, 0, 0, -1, 0.25f, 0.4f),

		Vertex(-w / 8,     h / 2, 0, 0, 0, -1, 0, 0.4f), // 4 ~ right panel
		Vertex(-w / 8, 5 * h / 6, 0, 0, 0, -1,	0,   0),
		Vertex(w / 2, 5 * h / 6, 0, 0, 0, -1,	1.25,   0),
		Vertex(w / 2,     h / 2, 0, 0, 0, -1, 1.25, 0.4f),

		Vertex(-w / 2, 5 * h / 6, 0, 0, 0, -1, 0, 0.2f), // 8 ~ top strip entire width
		Vertex(-w / 2,       h, 0, 0, 0, -1, 0,   0),
		Vertex(w / 2,       h, 0, 0, 0, -1, 2,   0),
		Vertex(w / 2, 5 * h / 6, 0, 0, 0, -1, 2, 0.2f),

		Vertex(-w / 2,   0, 0, 0, 0, -1,	0, 0.6f), // 12 ~ bottom strip entire width
		Vertex(-w / 2, h / 2, 0, 0, 0, -1,	0, 0),
		Vertex(w / 2, h / 2, 0, 0, 0, -1,	2, 0),
		Vertex(w / 2,   0, 0, 0, 0, -1,	2, 0.6f),

		Vertex(3 * -w / 8,     h / 2, 0, 0, 0, -1, 0, 1), // 16 ~ window
		Vertex(3 * -w / 8, 5 * h / 6, 0, 0, 0, -1, 0, 0),
		Vertex(-w / 8, 5 * h / 6, 0, 0, 0, -1, 1, 0),
		Vertex(-w / 8,     h / 2, 0, 0, 0, -1, 1, 1)

	};

	array<int16_t, 36> indices =
	{
		// Walls
		0, 1, 2,
		0, 2, 3,

		4, 5, 6,
		4, 6, 7,

		8, 9, 10,
		8, 10, 11,

		12, 13, 14,
		12, 14, 15,

		// window
		16, 17, 18,
		16, 18, 19
	};

	SubmeshGeometry wallSubmesh;
	wallSubmesh.IndexCount = 24;
	wallSubmesh.StartIndexLocation = 0;
	wallSubmesh.BaseVertexLocation = 0;

	SubmeshGeometry windowSubmesh;
	windowSubmesh.IndexCount = 6;
	windowSubmesh.StartIndexLocation = 24;
	windowSubmesh.BaseVertexLocation = 0;

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = "wallGeo";

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs["wall"] = wallSubmesh;
	geo->DrawArgs["window"] = windowSubmesh;

	m_Geometries[geo->Name] = move(geo);

	// Materials
	XMFLOAT4 diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT3 fresnel = XMFLOAT3(0.05f, 0.05f, 0.05f);
	float    roughness = 0.5f;
	Mat matProps(diffuse, fresnel, roughness);

	// common wall material
	UINT diffSlot = m_NextSrvHeapSlot;

	BuildTextureResource("wallTex1", wallTexfile.c_str());

	UINT bumpSlot = 1;
	if (bumpFile != "None")
	{
		bumpSlot = m_NextSrvHeapSlot;
		BuildTextureResource("wallTex1_nrm", bumpFile.c_str());
	}

	matProps.FresnelR0 = XMFLOAT3(0.05f, 0.05f, 0.05f);
	matProps.Roughness = 0.25f;

	AddMaterial("bricks", matProps, diffSlot, bumpSlot);

	// Window material
	diffSlot = m_NextSrvHeapSlot;
	BuildTextureResource("wallTex3", windowTexfile.c_str());

	matProps.DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, transparency);
	matProps.FresnelR0 = XMFLOAT3(0.1f, 0.1f, 0.1f);
	matProps.Roughness = 0.5f;

	AddMaterial("icemirror", matProps, diffSlot, 1);

	// walls
	XMMATRIX objPos, objRot;
	XMMATRIX objScale = XMMatrixScaling(1.0f, 1.0f, 1.0f);

	// north wall
	objRot = XMMatrixRotationRollPitchYaw(0.0f, 0.0f, 0.0f);
	objPos = XMMatrixTranslation(xPos, 0.0f, 0.0f);
	AddRenderItem("wallGeo", "wall", "bricks", objScale, objRot, objPos);
	AddRenderItem("wallGeo", "window", "icemirror", objScale, objRot, objPos, TexturedTransparent);

	// north walls to the right of north wall
	for (UINT n = 1; n < rightCount + 1; ++n)
	{
		objPos = XMMatrixTranslation(xPos + w * n, 0.0f, 0.0f);
		AddRenderItem("wallGeo", "wall", "bricks", objScale, objRot, objPos);
		AddRenderItem("wallGeo", "window", "icemirror", objScale, objRot, objPos, TexturedTransparent);
	}
	// west and east walls spawn towards camera from north wall
	for (UINT n = 0; n != southCount; ++n)
	{
		// west wall
		objRot = XMMatrixRotationRollPitchYaw(0.0f, -Maths::Pi / 2.0f, 0.0f);
		objPos = XMMatrixTranslation(-w / 2.0f, 0.0f, -w / 2.0f - (n * w));
		AddRenderItem("wallGeo", "wall", "bricks", objScale, objRot, objPos);
		AddRenderItem("wallGeo", "window", "icemirror", objScale, objRot, objPos, TexturedTransparent);

		// east wall
		objRot = XMMatrixRotationRollPitchYaw(0.0f, Maths::Pi / 2.0f, 0.0f);
		objPos = XMMatrixTranslation(rightCount * w + w / 2.0f, 0.0f, -w / 2.0f - (n * w));
		AddRenderItem("wallGeo", "wall", "bricks", objScale, objRot, objPos);
		AddRenderItem("wallGeo", "window", "icemirror", objScale, objRot, objPos, TexturedTransparent);
	}
}

void SceneBuilder::BuildFloor(float posX, float width, float depth, string texFile, string bumpFile)
{
	array<Vertex, 24> vertices =
	{
		Vertex(posX - width / 2.0f, 0.0f, -depth, 0.0f, 1.0f, 0.0f, 0.0f, 2.0f), // 0 
		Vertex(posX - width / 2.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(posX + width / 2.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f),
		Vertex(posX + width / 2.0f, 0.0f, -depth, 0.0f, 1.0f, 0.0f, 1.0f, 2.0f),
	};

	array<int16_t, 36> indices =
	{
		0, 1, 2,
		0, 2, 3,
	};

	SubmeshGeometry floorSubmesh;
	floorSubmesh.IndexCount = 6;
	floorSubmesh.StartIndexLocation = 0;
	floorSubmesh.BaseVertexLocation = 0;

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = "floorGeo";

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs["floor"] = floorSubmesh;

	m_Geometries[geo->Name] = move(geo);

	// Materials
	XMFLOAT4 diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT3 fresnel = XMFLOAT3(0.05f, 0.05f, 0.05f);
	float    roughness = 0.5f;
	Mat matProps(diffuse, fresnel, roughness);

	// Floor material
	UINT diffSlot = m_NextSrvHeapSlot;
	BuildTextureResource("roomTex2", texFile.c_str());

	// Bump
	UINT bumpSlot = 1;
	if (bumpFile != "None")
	{
		bumpSlot = m_NextSrvHeapSlot;
		BuildTextureResource("roomTex2_Bump", bumpFile.c_str());
	}

	matProps.FresnelR0 = XMFLOAT3(0.07f, 0.07f, 0.07f);
	matProps.Roughness = 0.3f;
	AddMaterial("bluetiles", matProps, diffSlot, bumpSlot);

	// Floor
	XMMATRIX objScale = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX objRot = XMMatrixRotationRollPitchYaw(0, 0, 0);
	XMMATRIX objPos = XMMatrixTranslation(0, 0, 0);

	AddRenderItem("floorGeo", "floor", "bluetiles", objScale, objRot, objPos);
}

void SceneBuilder::BuildCeiling(float posX, float height, float width, float depth, string texFile, string bumpFile)
{
	array<Vertex, 24> vertices =
	{
		Vertex(posX - width / 2.0f, 0.0f, -depth, 0.0f, 1.0f, 0.0f, 0.0f, 4.0f), // 0 
		Vertex(posX - width / 2.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(posX + width / 2.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 4.0f, 0.0f),
		Vertex(posX + width / 2.0f, 0.0f, -depth, 0.0f, 1.0f, 0.0f, 4.0f, 4.0f),
	};

	array<int16_t, 36> indices =
	{
		0, 1, 2,
		0, 2, 3,
	};

	SubmeshGeometry floorSubmesh;
	floorSubmesh.IndexCount = 6;
	floorSubmesh.StartIndexLocation = 0;
	floorSubmesh.BaseVertexLocation = 0;

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = "ceilGeo";

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice,
		m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs["ceiling"] = floorSubmesh;

	m_Geometries[geo->Name] = move(geo);

	// Materials
	XMFLOAT4 diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT3 fresnel = XMFLOAT3(0.05f, 0.05f, 0.05f);
	float    roughness = 0.5f;
	Mat matProps(diffuse, fresnel, roughness);

	// Floor material
	UINT diffSlot = m_NextSrvHeapSlot;
	BuildTextureResource("roomTex3", texFile.c_str());

	// Bump
	UINT bumpSlot = 1;
	if (bumpFile != "None")
	{
		bumpSlot = m_NextSrvHeapSlot;
		BuildTextureResource("roomTex3_Bump", bumpFile.c_str());
	}

	matProps.FresnelR0 = XMFLOAT3(0.07f, 0.07f, 0.07f);
	matProps.Roughness = 0.3f;
	AddMaterial("rooftiles", matProps, diffSlot, bumpSlot);

	// Ceiling
	XMMATRIX objScale = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	XMMATRIX objRot = XMMatrixRotationRollPitchYaw(0, 0, Maths::Pi);
	XMMATRIX objPos = XMMatrixTranslation(width / 2.0f, height, 0);

	AddRenderItem("ceilGeo", "ceiling", "rooftiles", objScale, objRot, objPos);
}

void SceneBuilder::BuildQuad(const string& geoName, const string& type, float x, float y, float wide, float high, float depth)
{
	//string geoName = type + "Geo";

	GeometryGenerator geoGen;
	GeometryGenerator::MeshData quad = geoGen.CreateQuad(x, y, wide, high, depth);

	vector<Vertex> vertices(quad.Vertices.size());
	for (UINT i = 0; i < quad.Vertices.size(); ++i)
	{
		vertices[i].Pos = quad.Vertices[i].Position;
		vertices[i].Normal = quad.Vertices[i].Normal;
		vertices[i].TexC = quad.Vertices[i].TexC;
		vertices[i].TangentU = quad.Vertices[i].TangentU;
	}

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);

	vector<uint16_t> indices = quad.GetIndices16();
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	geo->DrawArgs[type] = submesh;

	m_Geometries[geoName] = move(geo);
}

void SceneBuilder::BuildGrid(const string& geoName, const string& type, float width, float depth, UINT m, UINT n)
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData grid = geoGen.CreateGrid(width, depth, m, n);

	SubmeshGeometry gridSubmesh;
	gridSubmesh.IndexCount = (UINT)grid.Indices32.size();
	gridSubmesh.StartIndexLocation = 0;
	gridSubmesh.BaseVertexLocation = 0;

	// Vertices
	vector<Vertex> vertices(grid.Vertices.size());

	for (size_t i = 0; i < grid.Vertices.size(); ++i)
	{
		vertices[i].Pos = grid.Vertices[i].Position;
		vertices[i].Normal = grid.Vertices[i].Normal;
		vertices[i].TexC = grid.Vertices[i].TexC;
		vertices[i].TangentU = grid.Vertices[i].TangentU;
	}

	// Indices
	vector<uint16_t> indices;
	indices.insert(indices.end(), begin(grid.GetIndices16()), end(grid.GetIndices16()));

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	// Create buffers for our grid
	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);
	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs[type] = gridSubmesh;

	m_Geometries[geoName] = move(geo);
}

void SceneBuilder::BuildBox(const string& geoName, const string& type, float width, float height, float depth, UINT subdivisions)
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData box = geoGen.CreateBox(width, height, depth, subdivisions);

	vector<Vertex> vertices(box.Vertices.size());
	for (size_t i = 0; i < box.Vertices.size(); ++i)
	{
		auto& p = box.Vertices[i].Position;
		vertices[i].Pos = p;
		vertices[i].Normal = box.Vertices[i].Normal;
		vertices[i].TexC = box.Vertices[i].TexC;
	}

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);

	vector<uint16_t> indices = box.GetIndices16();
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	geo->DrawArgs[type] = submesh;

	m_Geometries[geoName] = move(geo);
}

void SceneBuilder::BuildSphere(const string& geoName, float radius, UINT sliceCount, UINT stackCount)
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData sphere = geoGen.CreateSphere(radius, sliceCount, stackCount);

	SubmeshGeometry sphereSubmesh;
	sphereSubmesh.IndexCount = (UINT)sphere.Indices32.size();
	sphereSubmesh.StartIndexLocation = 0;
	sphereSubmesh.BaseVertexLocation = 0;

	// Vertices
	vector<Vertex> vertices(sphere.Vertices.size());

	for (size_t i = 0; i < sphere.Vertices.size(); ++i)
	{
		vertices[i].Pos = sphere.Vertices[i].Position;
		vertices[i].Normal = sphere.Vertices[i].Normal;
		vertices[i].TexC = sphere.Vertices[i].TexC;
		vertices[i].TangentU = sphere.Vertices[i].TangentU;
	}

	// Indices
	vector<uint16_t> indices;
	indices.insert(indices.end(), begin(sphere.GetIndices16()), end(sphere.GetIndices16()));

	// Totals for our outside Geo buffer
	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	// Create buffers for our outside geometry
	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);
	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs["sphere"] = sphereSubmesh;

	m_Geometries[geoName] = move(geo);
}

void SceneBuilder::BuildCylinder(const string& geoName, const string& type, float bottomRadius, float topRadius, float height, UINT sliceCount, UINT stackCount)
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData cylinder = geoGen.CreateCylinder(bottomRadius, topRadius, height, sliceCount, stackCount);

	SubmeshGeometry cylinderSubmesh;
	cylinderSubmesh.IndexCount = (UINT)cylinder.Indices32.size();
	cylinderSubmesh.StartIndexLocation = 0;
	cylinderSubmesh.BaseVertexLocation = 0;

	// Vertices
	vector<Vertex> vertices(cylinder.Vertices.size());

	// Cylinder vertices
	for (size_t i = 0; i < cylinder.Vertices.size(); ++i)
	{
		vertices[i].Pos = cylinder.Vertices[i].Position;
		vertices[i].Normal = cylinder.Vertices[i].Normal;
		vertices[i].TexC = cylinder.Vertices[i].TexC;
		vertices[i].TangentU = cylinder.Vertices[i].TangentU;
	}

	// Indices
	vector<uint16_t> indices;
	indices.insert(indices.end(), begin(cylinder.GetIndices16()), end(cylinder.GetIndices16()));

	// Totals for our outside Geo buffer
	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	// Create buffers for our outside geometry
	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);
	geo->IndexBufferGPU = Tools::CreateDefaultBuffer(m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs[type] = cylinderSubmesh;

	m_Geometries[geoName] = move(geo);
}

#pragma endregion

#pragma region Water and Land Helpers

void SceneBuilder::BuildWavesGeometry( string geoName, float width, float depth )
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData grid = geoGen.CreateGrid(width, depth, m_Waves->RowCount(), m_Waves->ColumnCount());

	vector<Vertex> vertices(grid.Vertices.size());
	for(size_t i = 0; i < grid.Vertices.size(); ++i)
	{
		vertices[i].Pos = grid.Vertices[i].Position;
		vertices[i].Normal = grid.Vertices[i].Normal;
		vertices[i].TexC = grid.Vertices[i].TexC;
	}

	vector<uint32_t> indices = grid.Indices32;

	UINT vbByteSize = m_Waves->VertexCount()*sizeof(Vertex);
	UINT ibByteSize = (UINT)indices.size()*sizeof(uint32_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = geoName;

	ThrowIfFailed( D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU) );
	CopyMemory( geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize );

	ThrowIfFailed( D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU) );
	CopyMemory( geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize );

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader );

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader );

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R32_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	geo->DrawArgs["grid"] = submesh;

	m_Geometries[geoName] = move(geo);
}

void SceneBuilder::BuildLandGeometry()
{
    GeometryGenerator geoGen;
    GeometryGenerator::MeshData grid = geoGen.CreateGrid(160.0f, 160.0f, 50, 50);

    //
    // Extract the vertex elements we are interested and apply the height function to
    // each vertex.  In addition, color the vertices based on their height so we have
    // sandy looking beaches, grassy low hills, and snow mountain peaks.
    //

    vector<Vertex> vertices(grid.Vertices.size());
    for(size_t i = 0; i < grid.Vertices.size(); ++i)
    {
        auto& p = grid.Vertices[i].Position;
        vertices[i].Pos = p;
        vertices[i].Pos.y = GetHillsHeight(p.x, p.z);
        vertices[i].Normal = GetHillsNormal(p.x, p.z);
		vertices[i].TexC = grid.Vertices[i].TexC;
    }

    const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);

    vector<uint16_t> indices = grid.GetIndices16();
    const UINT ibByteSize = (UINT)indices.size() * sizeof(uint16_t);

	auto geo = make_unique<MeshGeometry>();
	geo->Name = LAND_GEO;

	ThrowIfFailed( D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU) );
	CopyMemory( geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize );

	ThrowIfFailed( D3DCreateBlob( ibByteSize, &geo->IndexBufferCPU ) );
	CopyMemory( geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize );

	geo->VertexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice, m_pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader ); 

	geo->IndexBufferGPU = Tools::CreateDefaultBuffer( m_pDevice, m_pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader );

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	geo->DrawArgs["grid"] = submesh;

	m_Geometries[LAND_GEO] = move(geo);
}

float SceneBuilder::GetHillsHeight(float x, float z)const
{
    return 0.3f*(z*sinf(0.1f*x) + x*cosf(0.1f*z));
}

XMFLOAT3 SceneBuilder::GetHillsNormal(float x, float z)const
{
    // n = (-df/dx, 1, -df/dz)
    XMFLOAT3 n(
        -0.03f*z*cosf(0.1f*x) - 0.3f*cosf(0.1f*z),
        1.0f,
        -0.3f*sinf(0.1f*x) + 0.03f*x*sinf(0.1f*z));

    XMVECTOR unitNormal = XMVector3Normalize(XMLoadFloat3(&n));
    XMStoreFloat3(&n, unitNormal);

    return n;
}

#pragma endregion




