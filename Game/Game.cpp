#include "Game.h"
#include <D3Dcompiler.h>
#include <DirectXColors.h>
#include <array>

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "D3D12.lib")
#pragma comment(lib, "QLibrary.lib")
#pragma comment(lib, "assimp-vc142-mt.lib")

using namespace std;
using namespace DirectX;

using Microsoft::WRL::ComPtr;

#include "GameConstants.h"
#include "GameObject.h"


Game::Game( HINSTANCE hInstance ) : QApp( hInstance )
{
}

Game::~Game()
{
	if (m_pSceneBuilder != nullptr)
	{
		delete m_pSceneBuilder;
		m_pSceneBuilder = nullptr;
	}
	if (m_pGameObject != nullptr)
	{
		delete m_pGameObject;
		m_pGameObject = nullptr;
	}
}

#pragma region Initialisation

bool Game::Initialize()
{
	QApp::m_ClientWidth = 1280; // 1920 x 1080 has aspect ratio of 1.77777, same as
	QApp::m_ClientHeight = 720; // // 2560 x 1440 , 1280 x 720

    if ( !QApp::Initialize() )
        return false;

	ThrowIfFailed( CoInitializeEx(nullptr, COINITBASE_MULTITHREADED) );

	// Reset the command list to prep for initialization commands
    ThrowIfFailed(  m_CommandList->Reset( m_DirectCmdListAlloc.Get(), nullptr ) ); 
	
	// Create the SRV Heap
	m_SRVDescriptorSize = m_Device->GetDescriptorHandleIncrementSize( D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV ); // Get the increment size of a descriptor in this heap type, this is hardware specific so we have to query this information

	m_DescriptorCount = 48; // DESCRIPTOR_COUNT

	D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
	srvHeapDesc.NumDescriptors = m_DescriptorCount;
	srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed( m_Device->CreateDescriptorHeap( &srvHeapDesc, IID_PPV_ARGS( &m_SRVHeap ) ) );
	CD3DX12_CPU_DESCRIPTOR_HANDLE	hCpu = CD3DX12_CPU_DESCRIPTOR_HANDLE( m_SRVHeap->GetCPUDescriptorHandleForHeapStart() );

	CD3DX12_GPU_DESCRIPTOR_HANDLE   hVid = CD3DX12_GPU_DESCRIPTOR_HANDLE( m_SRVHeap->GetGPUDescriptorHandleForHeapStart() );

	// Load the scene assets
	m_pSceneBuilder = new SceneBuilder( m_Device.Get(), m_CommandList.Get(), hCpu, hVid, m_SRVDescriptorSize, m_DescriptorCount );

	m_pGameObject = new GameObject("boxTest");
	if ( !m_pSceneBuilder->LoadGeometryResources( L"Config/Geometry.txt", m_pGameObject, m_Player ) )
		return false;

	if ( !m_pSceneBuilder->LoadSceneModels( L"Config/Models.txt", m_Player ) )
		return false;

	m_SkySrvOffset = m_pSceneBuilder->LoadSkyCube( L"Sky/stars1024.dds" );

	m_WavesSrvOffset = m_pSceneBuilder->LoadWaterWaves(L"Config/Water.txt");
		
	assert(m_WavesSrvOffset + m_pSceneBuilder->GetWaterWaves()->DescriptorCount() < m_DescriptorCount);

	LoadCameraAndLights( L"Config/Lighting.txt" );
	
	BuildRootSignature();

	BuildWavesRootSignature();

	BuildShadersAndInputLayout();
	BuildFrameResources();
	BuildPSOs();
	
	// Finished initialising GPU resources with scene assets, so execute command queue and hope
    ThrowIfFailed( m_CommandList->Close());
    ID3D12CommandList* cmdsLists[] = { m_CommandList.Get() };
    m_CommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

    FlushCommandQueue();

	return true;
}

void Game::BuildRootSignature()
{
	CD3DX12_DESCRIPTOR_RANGE skyMap;							
	skyMap.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0);	    // t0

	CD3DX12_DESCRIPTOR_RANGE displacementMapTable;
	displacementMapTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1); // t1

	CD3DX12_DESCRIPTOR_RANGE texMaps;
	texMaps.Init( D3D12_DESCRIPTOR_RANGE_TYPE_SRV, m_DescriptorCount, 2 ); // t2

	CD3DX12_ROOT_PARAMETER slotRootParameter[7];
	slotRootParameter[0].InitAsConstantBufferView(0);												// 0 - cbPerObject 
	slotRootParameter[1].InitAsConstantBufferView(1);												// 1 - cbSkinned
	slotRootParameter[2].InitAsConstantBufferView(2);												// 2 - cbPass
	slotRootParameter[3].InitAsShaderResourceView(0, 1);											// 3 - gMaterialData
	slotRootParameter[4].InitAsDescriptorTable(1, &texMaps, D3D12_SHADER_VISIBILITY_PIXEL);		    // 4 - gTextureMaps[]
	slotRootParameter[5].InitAsDescriptorTable(1, &skyMap, D3D12_SHADER_VISIBILITY_PIXEL);		    // 5 - gCubeMap
	slotRootParameter[6].InitAsDescriptorTable(1, &displacementMapTable, D3D12_SHADER_VISIBILITY_ALL);		    // 6 - gDisplacementMap

	auto staticSamplers = GetStaticSamplers();

	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(7, slotRootParameter, (UINT)staticSamplers.size(), staticSamplers.data(), D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> serializedRootSig = nullptr;
	ComPtr<ID3DBlob> errorBlob = nullptr;
	HRESULT hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf());

	if (errorBlob != nullptr)
		::OutputDebugStringA((char*)errorBlob->GetBufferPointer());

	ThrowIfFailed(hr);

	ThrowIfFailed(m_Device->CreateRootSignature(0, serializedRootSig->GetBufferPointer(), serializedRootSig->GetBufferSize(), IID_PPV_ARGS(m_RootSignature.GetAddressOf())));
}

void Game::BuildWavesRootSignature()
{
	CD3DX12_DESCRIPTOR_RANGE uavTable0;
	uavTable0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);

	CD3DX12_DESCRIPTOR_RANGE uavTable1;
	uavTable1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 1);

	CD3DX12_DESCRIPTOR_RANGE uavTable2;
	uavTable2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 2);

	// Root parameter can be a table, root descriptor or root constants.
	CD3DX12_ROOT_PARAMETER slotRootParameter[4];

	// Perfomance TIP: Order from most frequent to least frequent.
	slotRootParameter[0].InitAsConstants(6, 0);
	slotRootParameter[1].InitAsDescriptorTable(1, &uavTable0);
	slotRootParameter[2].InitAsDescriptorTable(1, &uavTable1);
	slotRootParameter[3].InitAsDescriptorTable(1, &uavTable2);

	// A root signature is an array of root parameters.
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(4, slotRootParameter,
		0, nullptr,
		D3D12_ROOT_SIGNATURE_FLAG_NONE);

	// create a root signature with a single slot which points to a descriptor range consisting of a single constant buffer
	ComPtr<ID3DBlob> serializedRootSig = nullptr;
	ComPtr<ID3DBlob> errorBlob = nullptr;
	HRESULT hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1,
		serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf());

	if(errorBlob != nullptr)
	{
		::OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}
	ThrowIfFailed( hr );

	ThrowIfFailed( m_Device->CreateRootSignature( 0, serializedRootSig->GetBufferPointer(), serializedRootSig->GetBufferSize(), IID_PPV_ARGS( mWavesRootSignature.GetAddressOf() ) ) );
}

void Game::BuildShadersAndInputLayout()
{
	const D3D_SHADER_MACRO pixelSpecular[] = { "SPECULAR_REFLECTIONS", "1", NULL, NULL };
	const D3D_SHADER_MACRO alphaTestDefines[] = { "ALPHA_TEST", "1", NULL, NULL };
	const D3D_SHADER_MACRO fogDefines[] = { "FOG", "1", NULL, NULL };
	const D3D_SHADER_MACRO displaceDefines[] = { "DISPLACEMENT_MAP", "1", NULL, NULL };
	
	m_Shaders["standardVS"] = Tools::CompileShader(L"Shaders\\Static.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["standardPS"] = Tools::CompileShader(L"Shaders\\Static.hlsl", m_SpecularReflections ? pixelSpecular : nullptr, "PS", "ps_5_1");
	m_Shaders["alphaTestedPS"] = Tools::CompileShader(L"Shaders\\Static.hlsl", alphaTestDefines, "PS", "ps_5_1");
	m_Shaders["wavesVS"] = Tools::CompileShader(L"Shaders\\Static.hlsl", displaceDefines, "VS", "vs_5_1");

	m_Shaders["bumpedVS"] = Tools::CompileShader(L"Shaders\\StaticBump.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["bumpedPS"] = Tools::CompileShader(L"Shaders\\StaticBump.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["skinnedVS"] = Tools::CompileShader(L"Shaders\\Skinned.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["skinnedPS"] = Tools::CompileShader(L"Shaders\\Skinned.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["colorVS"] = Tools::CompileShader(L"Shaders\\Colour.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["colorPS"] = Tools::CompileShader(L"Shaders\\Colour.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["skyVS"] = Tools::CompileShader(L"Shaders\\Sky.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["skyPS"] = Tools::CompileShader(L"Shaders\\Sky.hlsl", nullptr, "PS", "ps_5_1");

	
	m_Shaders["wavesUpdateCS"] = Tools::CompileShader(L"Shaders\\WaveSim.hlsl", nullptr, "UpdateWavesCS", "cs_5_1");
	m_Shaders["wavesDisturbCS"] = Tools::CompileShader(L"Shaders\\WaveSim.hlsl", nullptr, "DisturbWavesCS", "cs_5_1");
    
	m_InputLayout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};
	
	m_BumpedInputLayout =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
    };

	m_SkinnedInputLayout =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 60, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
    };
}

void Game::BuildFrameResources()
{
	UINT SkinnedObjectCount = 1;

	for ( int i = 0 ; i < gNumFrameResources ; ++i )
    {
        m_FrameResources.push_back( std::make_unique<FrameResource>( m_Device.Get(), 1, (UINT)m_pSceneBuilder->GetAllRenderItems().size(), SkinnedObjectCount, (UINT)m_pSceneBuilder->GetMaterials().size() ) );
    }
}

void Game::BuildPSOs()
{
	// PSO default
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
	ZeroMemory(&psoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	psoDesc.InputLayout = { m_InputLayout.data(), (UINT)m_InputLayout.size() };
	psoDesc.pRootSignature = m_RootSignature.Get();
	psoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["standardVS"]->GetBufferPointer()), m_Shaders["standardVS"]->GetBufferSize() };
	psoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["standardPS"]->GetBufferPointer()), m_Shaders["standardPS"]->GetBufferSize() };
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = m_BackBufferFormat;
	psoDesc.SampleDesc.Count = m_4xMsaaState ? 4 : 1;
	psoDesc.SampleDesc.Quality = m_4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	psoDesc.DSVFormat = m_DepthStencilFormat;
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_PSOs["static"])));

	// standard transparency blending
	D3D12_RENDER_TARGET_BLEND_DESC blendDesc;
	blendDesc.BlendEnable = true;
	blendDesc.LogicOpEnable = false;
	blendDesc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	// PSO transparent
	D3D12_GRAPHICS_PIPELINE_STATE_DESC transparentPsoDesc = psoDesc;
	transparentPsoDesc.BlendState.RenderTarget[0] = blendDesc;
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&transparentPsoDesc, IID_PPV_ARGS(&m_PSOs["transparent"])));

	// PSO waves render
	D3D12_GRAPHICS_PIPELINE_STATE_DESC displacePsoDesc = transparentPsoDesc;
	displacePsoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["wavesVS"]->GetBufferPointer()), m_Shaders["wavesVS"]->GetBufferSize() };
	ThrowIfFailed( m_Device->CreateGraphicsPipelineState(&displacePsoDesc, IID_PPV_ARGS(&m_PSOs["wavesRender"])));

    // PSO alphaTested
	D3D12_GRAPHICS_PIPELINE_STATE_DESC alphaPsoDesc = psoDesc;
	alphaPsoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["alphaTestedPS"]->GetBufferPointer()), m_Shaders["alphaTestedPS"]->GetBufferSize() };
	alphaPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&alphaPsoDesc, IID_PPV_ARGS(&m_PSOs["alphaTested"])));

	// PSO sky
	D3D12_GRAPHICS_PIPELINE_STATE_DESC skyPsoDesc = psoDesc;
	skyPsoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["skyVS"]->GetBufferPointer()), m_Shaders["skyVS"]->GetBufferSize() };
	skyPsoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["skyPS"]->GetBufferPointer()), m_Shaders["skyPS"]->GetBufferSize() };
	skyPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE; // The camera is inside the sky sphere, so just turn off culling
	skyPsoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL; // Make sure the depth function is LESS_EQUAL and not just LESS. Otherwise, the normalized depth values at z = 1 (NDC) will fail the depth test if the depth buffer was cleared to 1
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&skyPsoDesc, IID_PPV_ARGS(&m_PSOs["sky"])));

	// PSO colour
	D3D12_GRAPHICS_PIPELINE_STATE_DESC colorPsoDesc = psoDesc;
	colorPsoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["colorVS"]->GetBufferPointer()), m_Shaders["colorVS"]->GetBufferSize() };
	colorPsoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["colorPS"]->GetBufferPointer()), m_Shaders["colorPS"]->GetBufferSize() };
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&colorPsoDesc, IID_PPV_ARGS(&m_PSOs["colour"])));

	// PSO saticBumped
	D3D12_GRAPHICS_PIPELINE_STATE_DESC bumpedPsoDesc = psoDesc;
	bumpedPsoDesc.InputLayout = { m_BumpedInputLayout.data(), (UINT)m_BumpedInputLayout.size() };
	bumpedPsoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["bumpedVS"]->GetBufferPointer()), m_Shaders["bumpedVS"]->GetBufferSize() };
	bumpedPsoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["bumpedPS"]->GetBufferPointer()), m_Shaders["bumpedPS"]->GetBufferSize() };
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState(&bumpedPsoDesc, IID_PPV_ARGS(&m_PSOs["staticBumped"])));

	// PSP skinned
	D3D12_GRAPHICS_PIPELINE_STATE_DESC skinnedPsoDesc = psoDesc;
	skinnedPsoDesc.InputLayout = { m_SkinnedInputLayout.data(), (UINT)m_SkinnedInputLayout.size() };
	skinnedPsoDesc.VS = { reinterpret_cast<BYTE*>(m_Shaders["skinnedVS"]->GetBufferPointer()), m_Shaders["skinnedVS"]->GetBufferSize() };
	skinnedPsoDesc.PS = { reinterpret_cast<BYTE*>(m_Shaders["skinnedPS"]->GetBufferPointer()), m_Shaders["skinnedPS"]->GetBufferSize() };
	ThrowIfFailed(m_Device->CreateGraphicsPipelineState( &skinnedPsoDesc, IID_PPV_ARGS(&m_PSOs["skinned"])));



	// PSO wavesDisturb
	D3D12_COMPUTE_PIPELINE_STATE_DESC wavesDisturbPSO = {};
	wavesDisturbPSO.pRootSignature = mWavesRootSignature.Get();
	wavesDisturbPSO.CS = { reinterpret_cast<BYTE*>( m_Shaders["wavesDisturbCS"]->GetBufferPointer()), m_Shaders["wavesDisturbCS"]->GetBufferSize() };
	wavesDisturbPSO.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	ThrowIfFailed( m_Device->CreateComputePipelineState( &wavesDisturbPSO, IID_PPV_ARGS(&m_PSOs["wavesDisturb"]) ) );

	// PSO wavesUpdate
	D3D12_COMPUTE_PIPELINE_STATE_DESC wavesUpdatePSO = {};
	wavesUpdatePSO.pRootSignature = mWavesRootSignature.Get();
	wavesUpdatePSO.CS = { reinterpret_cast<BYTE*>( m_Shaders["wavesUpdateCS"]->GetBufferPointer()), m_Shaders["wavesUpdateCS"]->GetBufferSize() };
	wavesUpdatePSO.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	ThrowIfFailed( m_Device->CreateComputePipelineState( &wavesUpdatePSO, IID_PPV_ARGS(&m_PSOs["wavesUpdate"]) ) );
}

// highlight
// D3D12_GRAPHICS_PIPELINE_STATE_DESC highlightPsoDesc = psoDesc;
// Change the depth test from < to <= so that if we draw the same triangle twice, it will
// still pass the depth test.  This is needed because we redraw the picked triangle with a
// different material to highlight it.  If we do not use <=, the triangle will fail the 
// depth test the 2nd time we try and draw it.
// highlightPsoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
// highlightPsoDesc.BlendState.RenderTarget[0] = transparencyBlendDesc;

// BuildPSO_MarkingStencilMirrors()

	//CD3DX12_BLEND_DESC mirrorBlendState(D3D12_DEFAULT);
	//mirrorBlendState.RenderTarget[0].RenderTargetWriteMask = 0;
	//D3D12_DEPTH_STENCIL_DESC mirrorDSS;
	//mirrorDSS.DepthEnable = true;
	//mirrorDSS.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	//mirrorDSS.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	//mirrorDSS.StencilEnable = true;
	//mirrorDSS.StencilReadMask = 0xff;
	//mirrorDSS.StencilWriteMask = 0xff;
	//mirrorDSS.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	//mirrorDSS.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	//mirrorDSS.FrontFace.StencilPassOp = D3D12_STENCIL_OP_REPLACE;
	//mirrorDSS.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	//// We are not rendering backfacing polygons, so these settings do not matter.
	//mirrorDSS.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	//mirrorDSS.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	//mirrorDSS.BackFace.StencilPassOp = D3D12_STENCIL_OP_REPLACE;
	//mirrorDSS.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	//D3D12_GRAPHICS_PIPELINE_STATE_DESC markMirrorsPsoDesc = psoDesc;
	//markMirrorsPsoDesc.BlendState = mirrorBlendState;
	//markMirrorsPsoDesc.DepthStencilState = mirrorDSS;
	//ThrowIfFailed(md3dDevice->CreateGraphicsPipelineState(&markMirrorsPsoDesc, IID_PPV_ARGS(&mPSOs["markStencilMirrors"])));

// BuildPSO_StencilReflections()

	//D3D12_DEPTH_STENCIL_DESC reflectionsDSS;
	//reflectionsDSS.DepthEnable = true;
	//reflectionsDSS.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	//reflectionsDSS.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	//reflectionsDSS.StencilEnable = true;
	//reflectionsDSS.StencilReadMask = 0xff;
	//reflectionsDSS.StencilWriteMask = 0xff;
	//reflectionsDSS.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_EQUAL;
	//// We are not rendering backfacing polygons, so these settings do not matter.
	//reflectionsDSS.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	//reflectionsDSS.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_EQUAL;
	//D3D12_GRAPHICS_PIPELINE_STATE_DESC drawReflectionsPsoDesc = psoDesc;
	//drawReflectionsPsoDesc.DepthStencilState = reflectionsDSS;
	//drawReflectionsPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
	//drawReflectionsPsoDesc.RasterizerState.FrontCounterClockwise = true;
	//ThrowIfFailed(md3dDevice->CreateGraphicsPipelineState(&drawReflectionsPsoDesc, IID_PPV_ARGS(&mPSOs["drawStencilReflections"])));

// Corresponding_D3DBLENDING()

	// D3D12_BLEND_OP_ADD			Add source 1 and source 2.
	// D3D12_BLEND_OP_SUBTRACT		Subtract source 1 from source 2.
	// D3D12_BLEND_OP_REV_SUBTRACT	Subtract source 2 from source 1.
	// D3D12_BLEND_OP_MIN			Find the minimum of source 1 and source 2.
	// D3D12_BLEND_OP_MAX			Find the maximum of source 1 and source 2.
	//typedef enum D3D12_BLEND {
	//  D3D12_BLEND_ZERO,
	//  D3D12_BLEND_ONE,
	//  D3D12_BLEND_SRC_COLOR,
	//  D3D12_BLEND_INV_SRC_COLOR,
	//  D3D12_BLEND_SRC_ALPHA,
	//  D3D12_BLEND_INV_SRC_ALPHA,
	//  D3D12_BLEND_DEST_ALPHA,
	//  D3D12_BLEND_INV_DEST_ALPHA,
	//  D3D12_BLEND_DEST_COLOR,
	//  D3D12_BLEND_INV_DEST_COLOR,
	//  D3D12_BLEND_SRC_ALPHA_SAT,
	//  D3D12_BLEND_BLEND_FACTOR,
	//  D3D12_BLEND_INV_BLEND_FACTOR,
	//  D3D12_BLEND_SRC1_COLOR,
	//  D3D12_BLEND_INV_SRC1_COLOR,
	//  D3D12_BLEND_SRC1_ALPHA,
	//  D3D12_BLEND_INV_SRC1_ALPHA
	//} ;

std::array<const CD3DX12_STATIC_SAMPLER_DESC, 6> Game::GetStaticSamplers()
{
	// Applications usually only need a handful of samplers.  So just define them all up front
	// and keep them available as part of the root signature.  

	const CD3DX12_STATIC_SAMPLER_DESC pointWrap(
		0, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC pointClamp(
		1, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC linearWrap(
		2, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC linearClamp(
		3, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC anisotropicWrap(
		4, // shaderRegister
		D3D12_FILTER_ANISOTROPIC, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressW
		0.0f,                             // mipLODBias
		8);                               // maxAnisotropy

	const CD3DX12_STATIC_SAMPLER_DESC anisotropicClamp(
		5, // shaderRegister
		D3D12_FILTER_ANISOTROPIC, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressW
		0.0f,                              // mipLODBias
		8);                                // maxAnisotropy

	return { 
		pointWrap, pointClamp,
		linearWrap, linearClamp, 
		anisotropicWrap, anisotropicClamp };
}

// CreateRtvAndDsvDescriptorHeaps()

//void Game::CreateRtvAndDsvDescriptorHeaps()
//{
//    // Add +1 for screen normal map, +2 for ambient maps.
//    D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
//    rtvHeapDesc.NumDescriptors	= SwapChainBufferCount + 3;
//    rtvHeapDesc.Type			= D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
//    rtvHeapDesc.Flags			= D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
//    rtvHeapDesc.NodeMask		= 0;
//
//    ThrowIfFailed( m_Device->CreateDescriptorHeap( &rtvHeapDesc, IID_PPV_ARGS( m_RtvHeap.GetAddressOf() ) ) );
//
//    // Add +1 DSV for shadow map
//    D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
//    dsvHeapDesc.NumDescriptors	= 2;
//    dsvHeapDesc.Type			= D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
//    dsvHeapDesc.Flags			= D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
//    dsvHeapDesc.NodeMask		= 0;
//
//    ThrowIfFailed( m_Device->CreateDescriptorHeap( &dsvHeapDesc, IID_PPV_ARGS( m_DsvHeap.GetAddressOf() ) ) );
//}

#pragma endregion

#pragma region Update

void Game::Update(const GameTimer& gt)
{
	static bool DoThisOnce = false;

	if (m_IsFollowCamera)
	{
		// Move player avatar position with WSAD keys
		UpdatePlayer(gt);

		// Player position has changed so adjust follow camera 
		UpdateFollowCamera(gt);
	}
	else
	{
		// Just move camera position with WSAD keys
		UpdateFirstPersonCamera(gt);
	}

	if ( !m_pGameObject->m_IsIdle )
		UpdateTheObject(gt);

	UpdateCameraAndLights(gt);

	// Cycle through the circular frame resource array
	m_CurrFrameResourceIndex = (m_CurrFrameResourceIndex + 1) % gNumFrameResources;
	m_CurrFrameResource = m_FrameResources[m_CurrFrameResourceIndex].get();

	// Wait until the GPU has completed commands up to this fence point
	if (m_CurrFrameResource->Fence != 0 && m_Fence->GetCompletedValue() < m_CurrFrameResource->Fence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
		ThrowIfFailed(m_Fence->SetEventOnCompletion(m_CurrFrameResource->Fence, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	if ( m_bDrawWaterWaves )
		UpdateMaterials(gt);

	UpdateObjectCBs(gt);

	UpdateSkinnedCBs(gt);

	UpdateMaterialBuffer(gt);

	UpdateMainPassCB(gt);
}

void Game::UpdatePlayer(const GameTimer& gt)
{
	const float dt = gt.DeltaTime();

	float speed = CAM_WALK_SPEED * dt;
	if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
		speed *= 2.0f;

	if (GetAsyncKeyState('W') & 0x8000)
		m_Player.WalkFacing(speed, m_PlayerLook);

	if (GetAsyncKeyState('S') & 0x8000)
		m_Player.WalkFacing(-speed, m_PlayerLook);

	if (GetAsyncKeyState('A') & 0x8000)
		m_Player.LookStrafe(m_PlayerLook, m_Camera.GetRight(), -speed);

	if (GetAsyncKeyState('D') & 0x8000)
		m_Player.LookStrafe(m_PlayerLook, m_Camera.GetRight(), speed);
}

void Game::UpdateFirstPersonCamera(const GameTimer& gt)
{
	const float dt = gt.DeltaTime();

	float camSpeed = CAM_WALK_SPEED * dt;
	if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
		camSpeed *= 2.0f;

	if (GetAsyncKeyState('W') & 0x8000)
		m_Camera.Walk(camSpeed);

	if (GetAsyncKeyState('S') & 0x8000)
		m_Camera.Walk(-camSpeed);

	if (GetAsyncKeyState('A') & 0x8000)
		m_Camera.Strafe(-camSpeed);

	if (GetAsyncKeyState('D') & 0x8000)
		m_Camera.Strafe(camSpeed);

	if (m_WalkCamMode)
	{
		XMFLOAT3 camPos = m_Camera.GetPosition3f();
		m_Camera.SetPosition(camPos.x, PLAYER_HEIGHT, camPos.z);
	}

	if (m_HeadLampOn)
	{
		XMFLOAT3 camPos = m_Camera.GetPosition3f();
		m_LightPosition[1] = camPos;
		m_LightingDirty = true;
	}

	m_Camera.UpdateViewMatrix();
}

void Game::UpdateFollowCamera(const GameTimer& gt)
{
	const XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);

	const float smoothPos = 2.4f;

	const float dt = gt.DeltaTime();

	XMVECTOR targetPosition = XMVectorAdd(m_Player.GetPosition(), m_DistanceUp * Up);
	targetPosition = XMVectorSubtract(targetPosition, m_DistanceBack * m_PlayerLook);
	XMVECTOR nextCamPos = XMVectorLerp(m_Camera.GetPosition(), targetPosition, smoothPos * dt);

	m_Camera.SetPosition(nextCamPos);
	m_Camera.LookAtForward(m_PlayerLook);


	m_Camera.UpdateViewMatrix();
}

void Game::UpdateTheObject(const GameTimer& gt)
{
	// Move the object at  'm_TheObjectIndex'
	XMVECTOR zero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR S = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
	XMVECTOR Q = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

	// 1. Update the object's target position
	XMVECTOR target = XMLoadFloat3(&m_pGameObject->m_TargetPos);
	target = XMVectorSubtract(target, m_pGameObject->m_Pos);

	// 2. Update the object's target orientation
	XMVECTOR targetRot = XMLoadFloat3( &m_pGameObject->m_TargetRot );
	targetRot = XMVectorSubtract( targetRot, m_pGameObject->m_Rot );

	// 3. Move object a little way towards it's final position
	m_pGameObject->m_Pos = XMVectorLerp( m_pGameObject->m_Pos, target, gt.DeltaTime() );

	// 4. Adjust object orientation a little way towards it's final orientation
	m_pGameObject->m_Rot = XMQuaternionSlerp( m_pGameObject->m_Rot, targetRot, gt.DeltaTime() );

	// 5. Update the object's world
	auto pBox = m_pSceneBuilder->GetAllRenderItems().at(m_pGameObject->m_CBIndex).get();
	XMStoreFloat4x4( &pBox->mWorld, XMMatrixAffineTransformation( S, zero, m_pGameObject->m_Rot, m_pGameObject->m_Pos ) );
	pBox->mNumFramesDirty = gNumFrameResources;

	if ( XMVector3NearEqual( m_pGameObject->m_Pos, target, gUnitQuaternionEpsilon ) )
		m_pGameObject->m_IsIdle = true;
}

void Game::UpdateCameraAndLights(const GameTimer& gt)
{
	static int Preset = 0; // Current colour preset 
	static int Active = 0; // Currently Selected light

	if (m_OurDownKey != 0)
		return;

	wstring info = L"";

	// Arrow Keys ~ Move Selected light
	if (GetAsyncKeyState(VK_LEFT) & 0x8000)
	{
		m_LightPosition[Active].x += -4.0f * gt.DeltaTime();
		m_LightingDirty = true;
	}

	if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
	{
		m_LightPosition[Active].x += 4.0f * gt.DeltaTime();
		m_LightingDirty = true;
	}

	if (GetAsyncKeyState(VK_UP) & 0x8000)
	{
		if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
			m_LightPosition[Active].z += 4.0f * gt.DeltaTime();
		else
			m_LightPosition[Active].y += 4.0f * gt.DeltaTime();

		m_LightingDirty = true;
	}

	if (GetAsyncKeyState(VK_DOWN) & 0x8000)
	{
		if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
			m_LightPosition[Active].z += -4.0f * gt.DeltaTime();
		else
			m_LightPosition[Active].y += -4.0f * gt.DeltaTime();

		m_LightingDirty = true;
	}

	// Key Presses for lighting setup

	// BACKSPACE key ~ Toggle ambient light strength
	else if (GetAsyncKeyState(VK_BACK) & 0x8001)
	{
		if (m_AmbientLight.x == 0.12f)
		{
			m_AmbientLight = m_OriginalAmbience;
			info = L"\nOriginal Ambience";
		}
		else
		{
			m_OriginalAmbience = m_AmbientLight;
			m_AmbientLight = XMFLOAT4(0.12f, 0.12f, 0.12f, 1.0f);
			info = L"\nLow Ambience";
		}
		m_LightingDirty = true;

		m_OurDownKey = VK_BACK;
	}

	// L Key ~ Select Next Light
	if (GetAsyncKeyState('L') & 0x8001)
	{
		Active++;
		if (Active > m_NumLights - 1)
			Active = 0;

		m_HeadLampOn = Active == 1;

		info = L"\n\n" + m_LightName[Active] + L" selected ~ " + m_LightColourName[Active] + L"\n";
		if (Active == 0)
			info += to_wstring(m_LightDirection[Active].x) + L" " + to_wstring(m_LightDirection[Active].y) + L" " + to_wstring(m_LightDirection[Active].z) + L"\n";
		else
		{
			info += to_wstring(m_LightPosition[Active].x) + L" " + to_wstring(m_LightPosition[Active].y) + L" " + to_wstring(m_LightPosition[Active].z) + L"\n";
			info += to_wstring(m_FallOffStart[Active]) + L" " + to_wstring(m_FallOffEnd[Active]) + L"\n";
			//info += to_wstring(m_SpotPower[Active]) + L"\n";
		}
		m_OurDownKey = 'L';
	}

	// K Key ~ Cycle colour preset for active light
	else if (GetAsyncKeyState('K') & 0x8001)
	{
		Preset++;
		if (Preset > gNumColorPresets - 1)
			Preset = 0;

		m_LightColour[Active] = ColourPresets[Preset];
		m_LightColourName[Active] = ColourNames[Preset];
		m_LightingDirty = true;

		info = L"\n" + m_LightColourName[Active];
		m_OurDownKey = 'K';
	}

	// 0 key ~ Restore colour of active light to Black or White
	else if (GetAsyncKeyState('0') & 0x8001)
	{
		if (m_LightColourName[Active] == L"Black")
		{
			m_LightColour[Active] = ColourPresets[0];
			m_LightColourName[Active] = ColourNames[0];
			m_LightingDirty = true;
		}
		else
		{
			m_LightColour[Active] = ColourPresets[gNumColorPresets - 1];
			m_LightColourName[Active] = ColourNames[gNumColorPresets - 1];
			m_LightingDirty = true;
		}
		info = L"\n" + m_LightColourName[Active];
		m_OurDownKey = '0';
	}

	// DELETE key ~ Move Selected light to camera position
	else if (GetAsyncKeyState(VK_DELETE) & 0x8001)
	{
		if (!m_HeadLampOn)
		{
			XMFLOAT3 camPos = m_Camera.GetPosition3f();
			m_LightPosition[Active] = camPos;

			if (Active == 0)
			{
				XMVECTOR camPos = m_Camera.GetPosition();
				camPos *= -1;
				XMStoreFloat3(&m_LightDirection[Active], XMVector3Normalize(camPos));
			}

			m_LightingDirty = true;

			info = L"\nMoved " + m_LightName[Active] + L" to camera position " + to_wstring(camPos.x) + L" " + to_wstring(camPos.y) + L" " + to_wstring(camPos.z) + L"\n";
		}
		else
			info = L"\n" + m_LightName[Active] + L" is already attachecd to your head\n";

		m_OurDownKey = VK_DELETE;
	}

	// 1 key ~ FallOffs tiny for active light
	else if (GetAsyncKeyState('1') & 0x8001)
	{
		m_FallOffStart[Active] = 4.0f;
		m_FallOffEnd[Active] = 10.0f;
		m_LightingDirty = true;

		info = L"\nTiny falloff";
		m_OurDownKey = '1';
	}

	// 2 key ~ FallOffs small for active light
	else if (GetAsyncKeyState('2') & 0x8001)
	{
		m_FallOffStart[Active] = 10.0f;
		m_FallOffEnd[Active] = 18.0f;
		m_LightingDirty = true;

		info = L"\nSmall falloff";
		m_OurDownKey = '2';
	}

	// 3 key ~ FallOffs big for active light
	else if (GetAsyncKeyState('3') & 0x8001)
	{
		m_FallOffStart[Active] = 20.0f;
		m_FallOffEnd[Active] = 26.0f;
		m_LightingDirty = true;

		info = L"\nBig falloff";
		m_OurDownKey = '3';
	}

	// ENTER key ~ Save all current lighting information as LightInformation.txt
	else if (GetAsyncKeyState(VK_RETURN) & 0x8001)
	{
		wofstream wfo(L"LightInformation.txt");

		for (int i = 0; i < m_NumLights; ++i)
		{
			wfo << m_LightName[i] << L" ~ ";
			wfo << m_LightColourName[i] << L"\n";
			if (i == 0)
				wfo << to_wstring(m_LightDirection[i].x) << L" " << to_wstring(m_LightDirection[i].y) << L" " << to_wstring(m_LightDirection[i].z) << L"\n\n";
			else
			{
				wfo << to_wstring(m_LightPosition[i].x) << L" " << to_wstring(m_LightPosition[i].y) << L" " << to_wstring(m_LightPosition[i].z) << L"\n";
				wfo << to_wstring(m_FallOffStart[i]) << L" " << to_wstring(m_FallOffEnd[i]) << L"\n\n";
				//	wfo << to_wstring(m_SpotPower[i]) << L"\n";
			}
		}

		XMFLOAT3 camPos = m_Camera.GetPosition3f();
		wfo << L"Camera Position " + to_wstring(camPos.x) << L" " << to_wstring(camPos.y) << L" " << to_wstring(camPos.z) << L"\n";
		wfo << L"Distance Back " + to_wstring(m_DistanceBack) << L" Distance Up " << to_wstring(m_DistanceUp) << L"\n";
		wfo.close();

		info = L"\n\nSaved current lighting information as LightInformation.txt\n";
		m_OurDownKey = VK_RETURN;
	}

	if (GetAsyncKeyState('U') & 0x8001)
		m_DistanceUp = m_DistanceUp + gt.DeltaTime();

	if (GetAsyncKeyState('J') & 0x8001)
		m_DistanceUp = m_DistanceUp - gt.DeltaTime();

	if (GetAsyncKeyState('B') & 0x8001)
		m_DistanceBack = m_DistanceBack + gt.DeltaTime();

	if (GetAsyncKeyState(VK_SPACE) & 0x8001)
	{
		m_bDrawWaterWaves = !m_bDrawWaterWaves;
		m_OurDownKey = VK_SPACE;

	}
	if (m_OurDownKey != 0)
		OutputDebugString(info.c_str());
}

void Game::DebounceAsyncKey(const char key)
{
	while (true)
	{
		if ((GetAsyncKeyState(key) & 0x8000) == 0x0000)
			break;
	}
}

void Game::UpdateMaterials( const GameTimer& gt )
{
	auto pMat = m_pSceneBuilder->GetMaterial(WATER_MAT);

	float& tu = pMat->MatTransform(3, 0);
	float& tv = pMat->MatTransform(3, 1);

	tu += 0.1f * gt.DeltaTime();
	tv += 0.02f * gt.DeltaTime();

	if (tu >= 1.0f)
		tu -= 1.0f;

	if (tv >= 1.0f)
		tv -= 1.0f;

	pMat->MatTransform(3, 0) = tu;
	pMat->MatTransform(3, 1) = tv;

	pMat->NumFramesDirty = 3; // gNumFrameResources;
}

void Game::UpdateObjectCBs(const GameTimer& gt)
{
	auto currObjectCB = m_CurrFrameResource->ObjectCB.get();
	for (auto& thisItem : m_pSceneBuilder->GetAllRenderItems() )
	{
		if (thisItem->mNumFramesDirty > 0)
		{
			XMMATRIX world = XMLoadFloat4x4(&thisItem->mWorld);
			XMMATRIX texTransform = XMLoadFloat4x4(&thisItem->mTexTransform);

			ObjectConstants objConstants;
			XMStoreFloat4x4(&objConstants.World, XMMatrixTranspose(world));
			XMStoreFloat4x4(&objConstants.TexTransform, XMMatrixTranspose(texTransform));
			objConstants.MaterialIndex = thisItem->mMat->MatCBIndex;
			objConstants.DisplacementMapTexelSize = thisItem->mDisplacementMapTexelSize;
			objConstants.GridSpatialStep = thisItem->mGridSpatialStep;

			currObjectCB->CopyData(thisItem->mObjCBIndex, objConstants);

			thisItem->mNumFramesDirty--;
		}
	}
}

void Game::UpdateSkinnedCBs(const GameTimer& gt)
{
	if ( m_pSceneBuilder->GetSkinnedModelInstance() != nullptr)
	{
		auto currSkinnedCB = m_CurrFrameResource->SkinnedCB.get();

		m_pSceneBuilder->GetSkinnedModelInstance()->UpdateSkinnedAnimation(gt.DeltaTime());

		SkinnedConstants skinnedConstants;
		copy(begin(m_pSceneBuilder->GetSkinnedModelInstance()->FinalTransforms), end(m_pSceneBuilder->GetSkinnedModelInstance()->FinalTransforms), &skinnedConstants.BoneTransforms[0]);

		currSkinnedCB->CopyData(0, skinnedConstants);
	}
}

void Game::UpdateMaterialBuffer(const GameTimer& gt)
{
	auto currMaterialBuffer = m_CurrFrameResource->MaterialBuffer.get();
	auto& sceneMaterials = m_pSceneBuilder->GetMaterials();
	for (auto& e : sceneMaterials)
	{
		Material* mat = e.second.get();

		if (mat->NumFramesDirty > 0)
		{
			XMMATRIX matTransform = XMLoadFloat4x4(&mat->MatTransform);

			MaterialData matData;
			matData.DiffuseAlbedo = mat->DiffuseAlbedo;
			matData.FresnelR0 = mat->FresnelR0;
			matData.Roughness = mat->Roughness;
			XMStoreFloat4x4(&matData.MatTransform, XMMatrixTranspose(matTransform));
			matData.DiffuseMapIndex = mat->DiffuseSrvHeapIndex;
			matData.NormalMapIndex = mat->NormalSrvHeapIndex;
			matData.SpecularMapIndex = mat->SpecularSrvHeapIndex;
			matData.HeightFactor = mat->HeightFactor;
			currMaterialBuffer->CopyData(mat->MatCBIndex, matData);

			mat->NumFramesDirty--;
		}
	}
}

void Game::UpdateMainPassCB(const GameTimer& gt)
{

	XMMATRIX view = m_Camera.GetView();
	XMMATRIX proj = m_Camera.GetProj();
	XMStoreFloat4x4(&m_MainPassCB.Proj, XMMatrixTranspose(proj));

	XMMATRIX viewProj = XMMatrixMultiply(view, proj);
	XMStoreFloat4x4(&m_MainPassCB.View, XMMatrixTranspose(view));
	XMStoreFloat4x4(&m_MainPassCB.ViewProj, XMMatrixTranspose(viewProj));

	XMVECTOR D = XMMatrixDeterminant(view);
	XMMATRIX invView = XMMatrixInverse( &D , view);
	XMStoreFloat4x4(&m_MainPassCB.InvView, XMMatrixTranspose(invView));

	D = XMMatrixDeterminant(proj);
	XMMATRIX invProj = XMMatrixInverse(&D, proj);
	D = XMMatrixDeterminant(viewProj);
	XMMATRIX invViewProj = XMMatrixInverse(&D, viewProj);
	XMStoreFloat4x4(&m_MainPassCB.InvProj, XMMatrixTranspose(invProj));
	XMStoreFloat4x4(&m_MainPassCB.InvViewProj, XMMatrixTranspose(invViewProj));

	m_MainPassCB.EyePosW = m_Camera.GetPosition3f();
	m_MainPassCB.RenderTargetSize = XMFLOAT2((float)m_ClientWidth, (float)m_ClientHeight);
	m_MainPassCB.InvRenderTargetSize = XMFLOAT2(1.0f / m_ClientWidth, 1.0f / m_ClientHeight);
	m_MainPassCB.NearZ = 1.0f;
	m_MainPassCB.FarZ = 1000.0f;
	m_MainPassCB.TotalTime = gt.TotalTime();
	m_MainPassCB.DeltaTime = gt.DeltaTime();
	

	if (m_LightingDirty)
	{
		m_MainPassCB.AmbientLight = m_AmbientLight;
		m_MainPassCB.FogColor = { 0.752941251f, 0.752941251f, 0.752941251f, 1.0f };
		m_MainPassCB.FogStart = 15.0f;
		m_MainPassCB.FogRange = 175.0f;

		// Directional lights
		for (int i = 0; i < m_NumDirLights; ++i)
		{
			m_MainPassCB.Lights[i].Strength = m_LightColour[i];
			m_MainPassCB.Lights[i].Direction = m_LightDirection[i];
		}

		// Point lights
		for (int i = m_NumDirLights; i < m_NumDirLights + m_NumPointLights; ++i)
		{
			m_MainPassCB.Lights[i].Strength = m_LightColour[i];
			m_MainPassCB.Lights[i].FalloffStart = m_FallOffStart[i];
			m_MainPassCB.Lights[i].FalloffEnd = m_FallOffEnd[i];
			m_MainPassCB.Lights[i].Position = m_LightPosition[i];
		}

		// Spot lights
		for (int i = m_NumDirLights + m_NumPointLights; i < m_NumDirLights + m_NumPointLights + m_NumSpotLights; ++i)
		{
			m_MainPassCB.Lights[i].Strength = m_LightColour[i];
			m_MainPassCB.Lights[i].FalloffStart = m_FallOffStart[i];
			m_MainPassCB.Lights[i].Direction = m_LightDirection[i];
			m_MainPassCB.Lights[i].FalloffEnd = m_FallOffEnd[i];
			m_MainPassCB.Lights[i].Position = m_LightPosition[i];
			m_MainPassCB.Lights[i].SpotPower = m_SpotPower[i];
		}

		m_LightingDirty = false;
	}

	auto currPassCB = m_CurrFrameResource->PassCB.get();
	currPassCB->CopyData(0, m_MainPassCB);
}

void Game::UpdateWavesGPU( const GameTimer& gt )
{
	// Every quarter second, generate a random wave.
	static float t_base = 0.0f;

	if ( (m_Timer.TotalTime() - t_base) >= 0.25f )
	{
		t_base += 0.25f;

		int i = Maths::Rand( 4, m_pSceneBuilder->GetWaterWaves()->RowCount() - 5 );
		int j = Maths::Rand( 4, m_pSceneBuilder->GetWaterWaves()->ColumnCount() - 5 );

		float r = Maths::RandF( 1.0f, 2.0f );

		m_pSceneBuilder->GetWaterWaves()->Disturb( m_CommandList.Get(), mWavesRootSignature.Get(), m_PSOs["wavesDisturb"].Get(), i, j, r );
	}

	// Update the wave simulation.
	m_pSceneBuilder->GetWaterWaves()->Update( gt, m_CommandList.Get(), mWavesRootSignature.Get(), m_PSOs["wavesUpdate"].Get() );
}

// PerformStaticsFrustumCulling()

//void Game::PerformStaticsFrustumCulling()
//{
//	
//	XMVECTOR scale, rotQuat, translation;
//
//	XMMATRIX W, invWorld, toLocal;
//	XMMATRIX view = m_Camera.GetView();
//	XMVECTOR detView = XMMatrixDeterminant(view);
//	XMMATRIX invView = XMMatrixInverse(&detView, view);
//
//	for ( auto& modelMap : m_StaticModels )
//	{
//		StaticModel* pModel = modelMap.second.get();
//		W = XMLoadFloat4x4( &pModel->World );
//		invWorld = XMMatrixInverse( &XMMatrixDeterminant(W), W );
//
//		// View space to the object's local space
//		toLocal = XMMatrixMultiply(invView, invWorld);
//
//		// Decompose the matrix into its individual parts
//		XMMatrixDecompose(&scale, &rotQuat, &translation, toLocal);
//
//		//if (m_Camera.IsIntersectingWithFrustum(&pModel->ModelAABB, XMVectorGetX(scale), rotQuat, translation))
//		//	pModel->Visible = true;
//		//else
//		//	pModel->Visible = false;
//	}
//}

#pragma endregion

#pragma region Render

void Game::Draw(const GameTimer& gt)
{
    auto cmdListAlloc = m_CurrFrameResource->CmdListAlloc;
	/// Reuse the memory associated with command recording.
    // We can only reset when the associated command lists have finished execution on the GPU.
    ThrowIfFailed( cmdListAlloc->Reset() );

	// A command list can be reset after it has been added to the command queue via ExecuteCommandList.
    // Reusing the command list reuses memory.
	ThrowIfFailed( m_CommandList->Reset(cmdListAlloc.Get(), m_PSOs["static"].Get() ) );

	// Set descriptor heaps
	ID3D12DescriptorHeap* descriptorHeaps[] = { m_SRVHeap.Get() };
	m_CommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);

	// Update the wave simulation on the gpu
	if ( m_bDrawWaterWaves )
	UpdateWavesGPU(gt);
	
	// Set the default pipeline stat
	m_CommandList->SetPipelineState( m_PSOs["static"].Get() );

	// Set viewport 
    m_CommandList->RSSetViewports(1, &m_ScreenViewport);
    m_CommandList->RSSetScissorRects(1, &m_ScissorRect);
	const auto barrier0 = CD3DX12_RESOURCE_BARRIER::Transition(CurrentBackBuffer(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
	m_CommandList->ResourceBarrier(1, &barrier0);
	
	// Clear back and depth buffers
    m_CommandList->ClearRenderTargetView(CurrentBackBufferView(), Colors::MidnightBlue, 0, nullptr); // (float*)&mMainPassCB.FogColor
    m_CommandList->ClearDepthStencilView(DepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	// Specify the buffers we are going to render to
	const auto backbuffer  = CurrentBackBufferView();
	const auto depthbuffer = DepthStencilView();
	m_CommandList->OMSetRenderTargets(1, &backbuffer, true, &depthbuffer);

	// Main rendering pass
	m_CommandList->SetGraphicsRootSignature(m_RootSignature.Get());

	// Pass Constant Buffer View ~ cbPass
	auto passCB = m_CurrFrameResource->PassCB->Resource();
	m_CommandList->SetGraphicsRootConstantBufferView(2, passCB->GetGPUVirtualAddress());

	// Descriptor Table Heap ~ gDisplacementMap
	m_CommandList->SetGraphicsRootDescriptorTable( 6, m_pSceneBuilder->GetWaterWaves()->DisplacementMap() );

	// Descriptor Table Heap ~ gTextureMaps[]
	// Bind all the textures used in this scene.  Observe
    // that we only have to specify the first descriptor in the table.  
    // The root signature knows how many descriptors are expected in the table.
	CD3DX12_GPU_DESCRIPTOR_HANDLE hTex( m_SRVHeap->GetGPUDescriptorHandleForHeapStart() );
	m_CommandList->SetGraphicsRootDescriptorTable( 4, hTex );

	// Descriptor Table Heap ~ gCubeMap
	// If we wanted to use "local" cube maps, we would have to change them per-object, or dynamically
    // index into an array of cube maps.
	CD3DX12_GPU_DESCRIPTOR_HANDLE hCubeMap = hTex.Offset( m_SkySrvOffset, m_SRVDescriptorSize );
	m_CommandList->SetGraphicsRootDescriptorTable( 5, hCubeMap );

	// Shader Resource View ~ gMaterialData
	// Rebind state whenever graphics root signature changes.
	// Bind all the materials used in this scene.  For structured buffers, we can bypass the heap and 
    // set as a root descriptor.
	auto matSRV = m_CurrFrameResource->MaterialBuffer->Resource();
	m_CommandList->SetGraphicsRootShaderResourceView( 3, matSRV->GetGPUVirtualAddress());

	// Draw Sky layer
	m_CommandList->SetPipelineState( m_PSOs["sky"].Get() );
	DrawRenderItems(m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::Sky ) );

	// Draw Coloured layer
	m_CommandList->SetPipelineState( m_PSOs["colour"].Get() );
    DrawRenderItems( m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::Colour ) );

	// Draw Static layer
	m_CommandList->SetPipelineState( m_PSOs["static"].Get() );
    DrawRenderItems( m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::Static ) );

	// Draw Static Bump Mapped layer
	m_CommandList->SetPipelineState( m_PSOs["staticBumped"].Get() );
    DrawRenderItems( m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::StaticBump ) );

	if ( m_pSceneBuilder->GetSkinnedModelInstance() != nullptr )
	{
		// Draw Skinned layer
		m_CommandList->SetPipelineState( m_PSOs["skinned"].Get() );
		DrawRenderItems( m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::Skinned ) );
	}
	
	// Draw Apha Tested layer
	m_CommandList->SetPipelineState(m_PSOs["alphaTested"].Get());
	DrawRenderItems(m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::AlphaTested ));

	// Draw Transparent layer
	m_CommandList->SetPipelineState(m_PSOs["transparent"].Get());
	DrawRenderItems(m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::Transparent ) );

	// Draw waves
	if (m_bDrawWaterWaves)
	{
	m_CommandList->SetPipelineState(m_PSOs["wavesRender"].Get() );
	DrawRenderItems( m_CommandList.Get(), m_pSceneBuilder->GetRenderItems( RenderLayer::GpuWaves ) );
	}
		
	// Finished recording commands
	const auto barrier1 = CD3DX12_RESOURCE_BARRIER::Transition(CurrentBackBuffer(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
	m_CommandList->ResourceBarrier(1, &barrier1);
    ThrowIfFailed( m_CommandList->Close() );

	// Add the command list to the queue for execution
    ID3D12CommandList* cmdsLists[] = { m_CommandList.Get() };
    m_CommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	 // Swap the back and front buffers
	ThrowIfFailed( m_SwapChain->Present(0, 0) );
	m_CurrBackbuffer = (m_CurrBackbuffer + 1) % SwapChainBufferCount;

	// Advance the fence value to mark commands up to this fence point
    m_CurrFrameResource->Fence = ++m_CurrentFence;

	// Add an instruction to the command queue to set a new fence point. 
    // Because we are on the GPU timeline, the new fence point won't be 
    // set until the GPU finishes processing all the commands prior to this Signal()
    m_CommandQueue->Signal(m_Fence.Get(), m_CurrentFence);

	
}

void Game::DrawRenderItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& ritems)
{
    UINT objCBByteSize = Tools::CalcConstantBufferByteSize(sizeof(ObjectConstants));
    UINT skinnedCBByteSize = Tools::CalcConstantBufferByteSize(sizeof(SkinnedConstants));

	auto objectCB = m_CurrFrameResource->ObjectCB->Resource();
	auto skinnedCB = m_CurrFrameResource->SkinnedCB->Resource();

    for ( size_t i = 0 ; i < ritems.size() ; ++i )
    {
        auto ri = ritems[i];
		const auto vb = ri->mGeo->VertexBufferView();
		const auto ib = ri->mGeo->IndexBufferView();
        cmdList->IASetVertexBuffers( 0, 1, &vb);
        cmdList->IASetIndexBuffer( &ib);
        cmdList->IASetPrimitiveTopology( ri->mPrimitiveType );

		// Object Constant Buffer View ~ cbPerObject
        D3D12_GPU_VIRTUAL_ADDRESS objCBAddress = objectCB->GetGPUVirtualAddress() + ri->mObjCBIndex * objCBByteSize;
		cmdList->SetGraphicsRootConstantBufferView( 0, objCBAddress );

		// Skinned Constant Buffer View ~ cbSkinned
		if ( ri->mSkinnedModelInst != nullptr )
		{
			D3D12_GPU_VIRTUAL_ADDRESS skinnedCBAddress = skinnedCB->GetGPUVirtualAddress() + ri->mSkinnedCBIndex * skinnedCBByteSize;
			cmdList->SetGraphicsRootConstantBufferView( 1, skinnedCBAddress );
		}
		else
			cmdList->SetGraphicsRootConstantBufferView( 1, 0 );

		const UINT numInstances = 1;

        cmdList->DrawIndexedInstanced( ri->mIndexCount, numInstances, ri->mStartIndexLocation, ri->mBaseVertexLocation, 0 );
    }
}

#pragma endregion

#pragma region Input

void Game::OnKeyUp(WPARAM vKey)
{
	if (vKey == VK_ESCAPE)
		PostQuitMessage(0);

	else if ((int)vKey == VK_F2)
		Set4xMsaaState(!m_4xMsaaState);

	else if ((int)vKey == m_OurDownKey)
		m_OurDownKey = 0;
}

void Game::OnMouseDown(WPARAM btnState, int x, int y)
{
	if ( (btnState & MK_LBUTTON) != 0 )
	{
		m_LastMousePos.x = x;
		m_LastMousePos.y = y;

		SetCapture(m_hMainWnd);

		
	}
	//	else if ( (btnState & MK_RBUTTON ) != 0 )
	//	{
	//		PickTriangle( x, y );
	//	}
}

void Game::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void Game::OnMouseMove(WPARAM btnState, int x, int y)
{
	if ((btnState & MK_LBUTTON) != 0)
	{
		if (m_IsFollowCamera)
		{
			// Follow Camera ~ Update the m_PlayerLook vector
			float dx = 0.15f * static_cast<float>(x - m_LastMousePos.x); 	// Make each pixel correspond to 0.15 of a degree
			dx = ClampAngle(dx, -60, 60);

			XMVECTOR rotQuat = XMQuaternionRotationAxis(WORLD_UP, XMConvertToRadians(dx));

			m_PlayerLook = XMVector3Rotate(m_PlayerLook, rotQuat);
		}
		else
		{
			// First Person Camera ~ Just rotate camera about it's local axii
			float dx = XMConvertToRadians(0.25f * static_cast<float>(x - m_LastMousePos.x)); // Make each pixel correspond to a quarter of a degree.
			float dy = XMConvertToRadians(0.25f * static_cast<float>(y - m_LastMousePos.y));

			m_Camera.Pitch(dy);
			m_Camera.RotateY(dx);
		}
		
	}
	m_LastMousePos.x = x;
	m_LastMousePos.y = y;

}

void Game::OnMouseWheel(short delta)
{
	m_ScrollWheelValue += delta;

	m_DistanceBack = 0.016f * m_ScrollWheelValue;
	if (m_DistanceBack < 0.2f)
	{
		m_DistanceBack = 0.2f;
		m_ScrollWheelValue = 12;
	}

	if (m_DistanceBack > 50.0f)
	{
		m_DistanceBack = 50.0f;
		m_ScrollWheelValue = 3125;
	}
}

void Game::OnResize()
{
	QApp::OnResize();

	constexpr float rads = XMConvertToRadians(VERTICALFOV_DEGREES);
	m_Camera.SetLens(rads, AspectRatio(), CAM_NEAR_Z, 1000.0f); // With the aspect ratio of 4:3 the optimal FoV would be 90°.

	m_Camera.ComputeFrustum();
}

float Game::ClampAngle(float angle, float min, float max)
{
	if (angle < -360)
		angle += 360;

	if (angle > 360)
		angle -= 360;

	return Maths::Clamp(angle, min, max);
}

void Game::PickTriangle(int sx, int sy)
{
	XMFLOAT4X4 P = m_Camera.GetProj4x4f();

	// Compute picking ray in view space.
	float vx = (+2.0f * sx / m_ClientWidth - 1.0f) / P(0, 0);
	float vy = (-2.0f * sy / m_ClientHeight + 1.0f) / P(1, 1);

	// Ray definition in view space.
	XMVECTOR rayOrigin = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR rayDir = XMVectorSet(vx, vy, 1.0f, 0.0f);

	XMMATRIX V = m_Camera.GetView();
	XMVECTOR d = XMMatrixDeterminant(V);
	XMMATRIX invView = XMMatrixInverse(&d, V);

	// Assume nothing is picked to start, so the picked render-item is invisible.
	RenderItem* mPickedRitem = nullptr;
	__debugbreak();

	mPickedRitem->mVisible = false;

	// Check if we picked an opaque render item.  A real app might keep a separate "picking list"
	// of objects that can be selected.   
	for ( auto ri : m_pSceneBuilder->GetRenderItems( RenderLayer::Static ) )
	{
		auto geo = ri->mGeo;

		// Skip invisible render-items.
		if (ri->mVisible == false)
			continue;

		XMMATRIX W = XMLoadFloat4x4(&ri->mWorld);
		XMVECTOR D = XMMatrixDeterminant(W);
		XMMATRIX invWorld = XMMatrixInverse(&D, W);

		// Tranform ray to vi space of Model.
		XMMATRIX toLocal = XMMatrixMultiply(invView, invWorld);

		rayOrigin = XMVector3TransformCoord(rayOrigin, toLocal);
		rayDir = XMVector3TransformNormal(rayDir, toLocal);

		// Make the ray direction unit length for the intersection tests.
		rayDir = XMVector3Normalize(rayDir);

		// If we hit the bounding box of the Model, then we might have picked a Model triangle,
		// so do the ray/triangle tests.
		//
		// If we did not hit the bounding box, then it is impossible that we hit 
		// the Model, so do not waste effort doing ray/triangle tests.
		float tmin = 0.0f;
		if (ri->mBounds.Intersects(rayOrigin, rayDir, tmin))
		{
			// NOTE: For the demo, we know what to cast the vertex/index data to.  If we were mixing
			// formats, some metadata would be needed to figure out what to cast it to.
			auto vertices = (Vertex*)geo->VertexBufferCPU->GetBufferPointer();
			auto indices = (std::uint32_t*)geo->IndexBufferCPU->GetBufferPointer();
			UINT triCount = ri->mIndexCount / 3;

			// Find the nearest ray/triangle intersection.
			tmin = Maths::Infinity;
			for (UINT i = 0; i < triCount; ++i)
			{
				// Indices for this triangle.
				UINT i0 = indices[i * 3 + 0];
				UINT i1 = indices[i * 3 + 1];
				UINT i2 = indices[i * 3 + 2];

				// Vertices for this triangle.
				XMVECTOR v0 = XMLoadFloat3(&vertices[i0].Pos);
				XMVECTOR v1 = XMLoadFloat3(&vertices[i1].Pos);
				XMVECTOR v2 = XMLoadFloat3(&vertices[i2].Pos);

				// We have to iterate over all the triangles in order to find the nearest intersection.
				float t = 0.0f;
				if (TriangleTests::Intersects(rayOrigin, rayDir, v0, v1, v2, t))
				{
					if (t < tmin)
					{
						// This is the new nearest picked triangle.
						tmin = t;
						UINT pickedTriangle = i;

						mPickedRitem->mVisible = true;
						mPickedRitem->mIndexCount = 3;
						mPickedRitem->mBaseVertexLocation = 0;

						// Picked render item needs same world matrix as object picked.
						mPickedRitem->mWorld = ri->mWorld;
						mPickedRitem->mNumFramesDirty = gNumFrameResources;

						// Offset to the picked triangle in the mesh index buffer.
						mPickedRitem->mStartIndexLocation = 3 * pickedTriangle;
					}
				}
			}
		}
	}
}

#pragma endregion


void Game::LoadCameraAndLights( const wstring& textFile )
{
	wifstream wfin( textFile );

	if ( wfin.bad() )
	{
		std::wstring err = L"File Not Found ~ " + textFile;
		OutputDebugString( err.c_str());
		return;
	}

	wstring ignore;
	wstring geoPath;
	
	wstring modelsPath;
	wstring	skyTexFile;

	FLOAT posX, posY, posZ, rotX, rotY, rotZ;
	
	wfin >> ignore;

	// Camera
	wfin >> ignore >> m_IsFollowCamera;
	wfin >> ignore >> posX >> posY >> posZ;
	wfin >> ignore >> m_DistanceUp;
	wfin >> ignore >> m_DistanceBack;
	wfin >> ignore >> rotX;
	wfin >> ignore >> rotY;
	wfin >> ignore >> rotZ;
	wfin >> ignore >> m_WalkCamMode;

	XMVECTOR pos = XMVectorSet( posX, posY, posZ, 0 );
	XMVECTOR rot = XMVectorSet( XMConvertToRadians(rotX), XMConvertToRadians(rotY), XMConvertToRadians(rotZ), 1);
	m_Camera.SetWorld( pos, rot );
	m_Camera.UpdateViewMatrix();
	m_PlayerLook = m_Camera.GetLook();

	// Lighting
	wfin >> ignore >> m_AmbientLight.x >> m_AmbientLight.y >> m_AmbientLight.z >> m_AmbientLight.w; // Ambient strength
	wfin >> ignore >> m_SpecularReflections;		// Enable specular reflections rendering

	m_OriginalAmbience = m_AmbientLight;

	// Lights information
	wfin >> ignore >> m_NumDirLights;
	wfin >> ignore >> m_NumPointLights;
	wfin >> ignore >> m_NumSpotLights;

	m_NumLights = m_NumDirLights + m_NumPointLights + m_NumSpotLights;
	assert( m_NumLights < gMaxLights - 1 );

	for ( int i = 0 ; i  < m_NumLights ; ++i )
	{
		wfin >> ignore >> m_LightName[i];
		wfin >> ignore >> m_LightColourName[i];
		m_LightColour[i] = ColourMap[ m_LightColourName[i] ];
		wfin >> ignore >> m_LightPosition[i].x >> m_LightPosition[i].y >> m_LightPosition[i].z;
		wfin >> ignore >> m_LightDirection[i].x >> m_LightDirection[i].y >> m_LightDirection[i].z;
		wfin >> ignore >> m_FallOffStart[i] >> m_FallOffEnd[i];
		wfin >> ignore >> m_SpotPower[i];
	}
	m_LightingDirty = true;

	wfin.close();

	return;
}


