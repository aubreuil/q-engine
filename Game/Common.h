#pragma once

enum class RenderLayer : int
{
	Colour = 0,
	Static,
	StaticBump,
	AlphaTested,
	Transparent,
	Skinned,
	Sky,
	Terrain,
	GpuWaves,
	Count
};

enum TextureType
{
	Tex = 0,
	Mesh,
	SkyCube
};

enum ImageFormats
{
	FormatNone = 0,
	FormatDDS,
	FormatTGA,
	FormatBMP,
	FormatPNG,
	FormatJPG,
	Formats
};

enum Walls
{
	North = 0,
	South,
	East,
	West
};

enum GeoType
{
	Quad = 0,
	Grid,
	Box,
	Sphere,
	Cylinder,
	Floor,
	Walls,
	Room
};

static std::unordered_map< std::string, GeoType > GeoTypeNames =
{
	{ "quad", Quad },
	{ "grid", Grid },
	{ "box", Box },
	{ "sphere", Sphere },
	{ "cylinder", Cylinder },
	{ "floor", Floor },
	{ "walls", Walls },
	{ "room", Room }
};


