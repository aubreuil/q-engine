#pragma once
#include <MeshGeometry.h>
#include <SkinnedData.h>
#include <Maths.h>

const int gNumFrameResources = 3;

// Simple struct to represent a material, a production 3D engine would likely create a class hierarchy of Materials
// MatCBIndex			~ Index into constant buffer corresponding to this material
// DiffuseSrvHeapIndex	~ Index into SRV heap for diffuse texture
// NormalSrvHeapIndex   ~ Index into SRV hep for bump texture
// HeightSrvHeapIndex   ~ Index into SRV heap for height map
// NumFramesDirty		~ Dirty flag indicating the material has changed and we need to update the constant buffer
//						~ ...because we have a material constant buffer for each FrameResource, we have to apply the update to each FrameResource
//						~ Thus, when we modify a material we should set NumFramesDirty = gNumFrameResources so that each frame resource gets the update
// Also holds material constant buffer data used for shading
//
struct Material		
{
	std::string			Name;
	int					MatCBIndex			= -1; 
	int					DiffuseSrvHeapIndex = -1;
	int					NormalSrvHeapIndex	= -1;
	int                 SpecularSrvHeapIndex  = -1;
	int                 GlossinessSrvHeapIndex = -1;
	float               HeightFactor = 1.0f;
	int					NumFramesDirty		= gNumFrameResources;
	DirectX::XMFLOAT4	DiffuseAlbedo		= { 1.0f, 1.0f, 1.0f, 1.0f };
	DirectX::XMFLOAT3	FresnelR0			= { 0.01f, 0.01f, 0.01f };
	float				Roughness			= 0.25f;
	DirectX::XMFLOAT4X4 MatTransform		= Maths::Identity4x4(); // Only used for effects like water
};

struct SkinnedModelInstance
{
	SkinnedData*						SkinnedInfo = nullptr;
	std::vector<DirectX::XMFLOAT4X4>	FinalTransforms;
	std::string							ClipName;
	float								TimePos = 0.0f;
		
	void UpdateSkinnedAnimation(float dt)
	{
		TimePos += dt;

		
		if ( TimePos > SkinnedInfo->GetClipEndTime( ClipName ) )				// Loop animation
			TimePos = 0.0f;

		
		SkinnedInfo->GetFinalTransforms( TimePos, ClipName, FinalTransforms );  // Compute the final transforms for this time position
	}
};

struct RenderItem
{
	RenderItem() = default;
	RenderItem( const RenderItem& rhs ) = delete;

	DirectX::XMFLOAT4X4			mWorld = Maths::Identity4x4();
	DirectX::XMFLOAT4X4			mTexTransform = Maths::Identity4x4();
	int							mNumFramesDirty	= gNumFrameResources; // so that each frame resource gets the Update
	UINT						mObjCBIndex = -1;
	Material*					mMat = nullptr;;
	MeshGeometry*				mGeo = nullptr;;
	D3D12_PRIMITIVE_TOPOLOGY	mPrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT						mIndexCount = 0;
	UINT						mStartIndexLocation = 0;
	int							mBaseVertexLocation = 0;
	UINT						mSkinnedCBIndex = -1;
	SkinnedModelInstance*		mSkinnedModelInst = nullptr;
	DirectX::BoundingBox		mBounds;
	DirectX::XMFLOAT2			mDisplacementMapTexelSize = { 1.0f, 1.0f };;
	float						mGridSpatialStep = 1.0f;
	bool						mVisible = true;
};

// mWorld
// World matrix of the shape that describes the object's local space
// relative to the world space, which defines the position, orientation,
// and scale of the object in the world.
//
// mNumFramesDirty
// Dirty flag indicating the object data has changed and we need to update the constant buffer.
// Because we have an object cbuffer for each FrameResource, we have to apply the
// update to each FrameResource.  Thus, when we modify obect data we should set 
// NumFramesDirty = gNumFrameResources so that each frame resource gets the update.
//
// mDisplacementMapTexelSize
// Used for GPU waves render items
//
// mObjCBIndex
// Index into GPU constant buffer corresponding to the ObjectCB for this render item.
//
// mPrimitiveType
// Using D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST primitive topology
//
// mIndexCount, mStartIndexLocation, mBaseVertexLocation
// are DrawIndexedInstanced parameters
//
// SkinnedModelInstance.UpdateSkinnedAnimation()
// Called every frame and increments the time position, interpolates the 
// animations for each bone based on the current animation clip, and 
// generates the final transforms which are ultimately set to the effect
// for processing in the vertex shader
//
//
