#include "SkinnedData.h"
#include "Maths.h"

using namespace DirectX;
using namespace std;

Keyframe::Keyframe() : TimePos(0.0f), Translation(0.0f, 0.0f, 0.0f), Scale(1.0f, 1.0f, 1.0f), RotationQuat(0.0f, 0.0f, 0.0f, 1.0f)
{
}

Keyframe::~Keyframe()
{
}
 
float BoneAnimation::GetStartTime()const
{
	// Keyframes are sorted by time, so first keyframe gives start time.
	return Keyframes.front().TimePos;
}

float BoneAnimation::GetEndTime()const
{
	// Keyframes are sorted by time, so last keyframe gives end time.
	float f = Keyframes.back().TimePos;

	return f;
}

void BoneAnimation::Interpolate(float t, XMFLOAT4X4& M)const
{
	if( t <= Keyframes.front().TimePos )
	{
		XMVECTOR S = XMLoadFloat3(&Keyframes.front().Scale);
		XMVECTOR P = XMLoadFloat3(&Keyframes.front().Translation);
		XMVECTOR Q = XMLoadFloat4(&Keyframes.front().RotationQuat);

		XMVECTOR zero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
		XMStoreFloat4x4(&M, XMMatrixAffineTransformation(S, zero, Q, P));
	}
	else if( t >= Keyframes.back().TimePos )
	{
		XMVECTOR S = XMLoadFloat3(&Keyframes.back().Scale);
		XMVECTOR P = XMLoadFloat3(&Keyframes.back().Translation);
		XMVECTOR Q = XMLoadFloat4(&Keyframes.back().RotationQuat);

		XMVECTOR zero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
		XMStoreFloat4x4(&M, XMMatrixAffineTransformation(S, zero, Q, P));
	}
	else
	{
		for(UINT i = 0; i < Keyframes.size()-1; ++i)
		{
			if( t >= Keyframes[i].TimePos && t <= Keyframes[i+1].TimePos )
			{
				float lerpPercent = (t - Keyframes[i].TimePos) / (Keyframes[i+1].TimePos - Keyframes[i].TimePos);

				XMVECTOR s0 = XMLoadFloat3(&Keyframes[i].Scale);
				XMVECTOR s1 = XMLoadFloat3(&Keyframes[i+1].Scale);

				XMVECTOR p0 = XMLoadFloat3(&Keyframes[i].Translation);
				XMVECTOR p1 = XMLoadFloat3(&Keyframes[i+1].Translation);

				XMVECTOR q0 = XMLoadFloat4(&Keyframes[i].RotationQuat);
				XMVECTOR q1 = XMLoadFloat4(&Keyframes[i+1].RotationQuat);

				XMVECTOR S = XMVectorLerp(s0, s1, lerpPercent);
				XMVECTOR P = XMVectorLerp(p0, p1, lerpPercent);
				XMVECTOR Q = XMQuaternionSlerp(q0, q1, lerpPercent);

				XMVECTOR zero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
				XMStoreFloat4x4(&M, XMMatrixAffineTransformation(S, zero, Q, P));

				break;
			}
		}
	}
}

float AnimationClip::GetClipStartTime()const
{
	// Find smallest start time over all bones in this clip.
	float t = Maths::Infinity;
	for(UINT i = 0; i < mBoneAnimations.size(); ++i)
	{
		t = Maths::Min(t, mBoneAnimations[i].GetStartTime());
	}

	return t;
}

float AnimationClip::GetClipEndTime()const
{
	// Find largest end time over all bones in this clip.
	float t = 0.0f;
	for(UINT i = 0; i < mBoneAnimations.size(); ++i)
	{
		t = Maths::Max(t, mBoneAnimations[i].GetEndTime());
	}

	return t;
}

void AnimationClip::Interpolate( float t, vector<XMFLOAT4X4>& boneTransforms ) const
{
	for ( UINT i = 0 ; i < mBoneAnimations.size(); ++i )
	{
		mBoneAnimations[i].Interpolate(t, boneTransforms[i]);
	}
}

float SkinnedData::GetClipStartTime( const string& clipName ) const
{
	auto clip = mAnimations.find(clipName);
	return clip->second.GetClipStartTime();
}

float SkinnedData::GetClipEndTime( const string& clipName ) const
{
	auto clip = mAnimations.find( clipName );
	return clip->second.GetClipEndTime();
}

UINT SkinnedData::BoneCount()const
{
	return (UINT)mBoneHierarchy.size();
}

void SkinnedData::Set( vector<int8_t>& boneHierarchy, vector<XMFLOAT4X4>& boneOffsets, unordered_map<string, AnimationClip>& animations )
{
	mBoneHierarchy			= boneHierarchy;
	mBoneOffsets			= boneOffsets;
	mAnimations				= animations;
}

void SkinnedData::GetFinalTransforms( float timePos, const string& clipName,  vector<XMFLOAT4X4>& finalTransforms ) const
{
	UINT numBones = BoneCount();

	vector<XMFLOAT4X4> localTransforms(numBones);
	auto clip = mAnimations.find(clipName);
	clip->second.Interpolate(timePos, localTransforms);

	vector<XMFLOAT4X4> globalTransforms(numBones);
	globalTransforms[0] = localTransforms[0];

	for (UINT i = 1; i < numBones; ++i)
	{
		uint32_t parent			= this->mBoneHierarchy[i];
		XMMATRIX animatedLocal  = XMLoadFloat4x4(&localTransforms[i]);
		XMMATRIX global			= XMLoadFloat4x4(&globalTransforms[parent]);
		XMMATRIX r				= XMMatrixMultiply(global, XMMatrixTranspose(animatedLocal));
		XMStoreFloat4x4(&globalTransforms[i], r);
	}

	{
		// Premultiply by the bone offset transform to get the final transform.
		for (UINT i = 0; i < numBones; ++i)
		{
			XMMATRIX offset			= XMLoadFloat4x4(&mBoneOffsets[i]);
			XMMATRIX toRoot			= XMLoadFloat4x4(&globalTransforms[i]);
			XMMATRIX finalTransform = XMMatrixMultiply(toRoot, offset);

			XMStoreFloat4x4(&finalTransforms[i], finalTransform);
		}
	}

	// finalTransforms now contains the key frames for each bone at timePos, in world space
}