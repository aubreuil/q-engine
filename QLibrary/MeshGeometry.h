#pragma once
#include "pch.h"

// SubmeshGeometry
// Defines a subrange of geometry in a MeshGeometry, for when multiple geometries are stored 
// in one vertex and index buffer, provides the offsets and data needed to draw a subset of 
// geometry stored in the vertex and index buffers
//
struct SubmeshGeometry
{
	UINT				 IndexCount			= 0;
	UINT				 StartIndexLocation = 0;
	INT					 BaseVertexLocation = 0;
	DirectX::BoundingBox Bounds;		// Bounding box of the geometry defined by this submesh.
};

// MeshGeometry
// A MeshGeometry may store multiple geometries in one vertex/index buffer
// Use DrawArgs container to define the Submesh geometries so we can draw the Submeshes individually
// System memory copies use Blobs because the vertex/index format can be generic
//
struct MeshGeometry
{
	std::string Name;
	Microsoft::WRL::ComPtr<ID3DBlob>				 VertexBufferCPU = nullptr;
	Microsoft::WRL::ComPtr<ID3DBlob>				 IndexBufferCPU  = nullptr;
	Microsoft::WRL::ComPtr<ID3D12Resource>			 VertexBufferGPU = nullptr;
	Microsoft::WRL::ComPtr<ID3D12Resource>			 IndexBufferGPU = nullptr;
	Microsoft::WRL::ComPtr<ID3D12Resource>			 VertexBufferUploader = nullptr;
	Microsoft::WRL::ComPtr<ID3D12Resource>			 IndexBufferUploader = nullptr;
    UINT											 VertexByteStride = 0;
	UINT											 VertexBufferByteSize = 0;
	DXGI_FORMAT										 IndexFormat = DXGI_FORMAT_R16_UINT;
	UINT											 IndexBufferByteSize = 0;
	std::unordered_map<std::string, SubmeshGeometry> DrawArgs;

	D3D12_VERTEX_BUFFER_VIEW VertexBufferView() const
	{
		D3D12_VERTEX_BUFFER_VIEW vbv;
		vbv.BufferLocation = VertexBufferGPU->GetGPUVirtualAddress();
		vbv.StrideInBytes  = VertexByteStride;
		vbv.SizeInBytes    = VertexBufferByteSize;
		return vbv;
	}

	D3D12_INDEX_BUFFER_VIEW IndexBufferView() const
	{
		D3D12_INDEX_BUFFER_VIEW ibv;
		ibv.BufferLocation = IndexBufferGPU->GetGPUVirtualAddress();
		ibv.Format		   = IndexFormat;
		ibv.SizeInBytes    = IndexBufferByteSize;
		return ibv;
	}
	
	void DisposeUploaders() // We can free this memory after we finish upload to the GPU
	{
		VertexBufferUploader = nullptr;
		IndexBufferUploader = nullptr;
	}
};

