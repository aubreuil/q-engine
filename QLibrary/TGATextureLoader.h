#pragma once

#include "pch.h"

typedef std::shared_ptr<std::vector<BYTE>> ByteArray;

class TGATextureLoader
{
public:
	TGATextureLoader();
	~TGATextureLoader();

	HRESULT		LoadTGAFromFile( const std::wstring& fileName, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs, bool sRGB );

private:
	void		CreateTGAFromMemory( const std::wstring& fileName, const void* _filePtr, size_t, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs, bool sRGB );
	void		Create( size_t Width, size_t Height, DXGI_FORMAT Format, const void* InitialData );
	ByteArray	ReadFileSync( const std::wstring& fileName );
	ByteArray	ReadFileHelperEx( std::shared_ptr<std::wstring> fileName );
	ByteArray	ReadFileHelper( const std::wstring& fileName );
	UINT		BytesPerPixel( DXGI_FORMAT Format );

private:
	std::vector<uint32_t*> m_pDatas;
	
	ByteArray NullFile = std::make_shared<std::vector<BYTE>> (std::vector<BYTE>() );
};