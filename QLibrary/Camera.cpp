#include "Camera.h"

using namespace DirectX;

Camera::Camera()
{
	float rads = XMConvertToRadians(VERTICALFOV_DEGREES);
	SetLens( rads, 1.0f, CAM_NEAR_Z, 1000.0f );
}

Camera::~Camera()
{
}

void Camera::UpdateViewMatrix()
{
	if(mViewDirty)
	{
		XMVECTOR R = XMLoadFloat3(&mRight);
		XMVECTOR U = XMLoadFloat3(&mUp);
		XMVECTOR L = XMLoadFloat3(&mLook);
		XMVECTOR P = XMLoadFloat3(&mPosition);

		// Keep camera's axes orthogonal to each other and of unit length.
		L = XMVector3Normalize(L);
		U = XMVector3Normalize(XMVector3Cross(L, R));

		// U, L already ortho-normal, so no need to normalize cross product.
		R = XMVector3Cross(U, L);

		// Fill in the view matrix entries.
		float x = -XMVectorGetX(XMVector3Dot(P, R));
		float y = -XMVectorGetX(XMVector3Dot(P, U));
		float z = -XMVectorGetX(XMVector3Dot(P, L));

		XMStoreFloat3(&mRight, R);
		XMStoreFloat3(&mUp, U);
		XMStoreFloat3(&mLook, L);

		mView(0, 0) = mRight.x;
		mView(1, 0) = mRight.y;
		mView(2, 0) = mRight.z;
		mView(3, 0) = x;

		mView(0, 1) = mUp.x;
		mView(1, 1) = mUp.y;
		mView(2, 1) = mUp.z;
		mView(3, 1) = y;

		mView(0, 2) = mLook.x;
		mView(1, 2) = mLook.y;
		mView(2, 2) = mLook.z;
		mView(3, 2) = z;

		mView(0, 3) = 0.0f;
		mView(1, 3) = 0.0f;
		mView(2, 3) = 0.0f;
		mView(3, 3) = 1.0f;

		mViewDirty = false;
	}
}

void Camera::ComputeFrustum()
{
	// Corners of the projection frustum in homogenous space
	static XMVECTOR HomogenousPoints[6] =
	{
		{ 1.0f,  0.0f, 1.0f, 1.0f },   // right (at far plane)
		{ -1.0f,  0.0f, 1.0f, 1.0f },   // left
		{ 0.0f,  1.0f, 1.0f, 1.0f },   // top
		{ 0.0f, -1.0f, 1.0f, 1.0f },   // bottom

		{ 0.0f, 0.0f, 0.0f, 1.0f },     // near
		{ 0.0f, 0.0f, 1.0f, 1.0f }      // far
	};

	XMVECTOR Determinant;
	XMMATRIX matInverse = XMMatrixInverse(&Determinant, XMLoadFloat4x4(&mProj));

	// Compute the frustum corners in world space
	XMVECTOR Points[6];

	for (INT i = 0; i < 6; i++)
	{
		// Transform point
		Points[i] = XMVector4Transform(HomogenousPoints[i], matInverse);
	}

	m_Frustum.Origin = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_Frustum.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	// Compute the slopes
	Points[0] = Points[0] * XMVectorReciprocal(XMVectorSplatZ(Points[0]));
	Points[1] = Points[1] * XMVectorReciprocal(XMVectorSplatZ(Points[1]));
	Points[2] = Points[2] * XMVectorReciprocal(XMVectorSplatZ(Points[2]));
	Points[3] = Points[3] * XMVectorReciprocal(XMVectorSplatZ(Points[3]));

	m_Frustum.RightSlope = XMVectorGetX(Points[0]);
	m_Frustum.LeftSlope = XMVectorGetX(Points[1]);
	m_Frustum.TopSlope = XMVectorGetY(Points[2]);
	m_Frustum.BottomSlope = XMVectorGetY(Points[3]);

	// Compute near and far
	Points[4] = Points[4] * XMVectorReciprocal(XMVectorSplatW(Points[4]));
	Points[5] = Points[5] * XMVectorReciprocal(XMVectorSplatW(Points[5]));

	m_Frustum.Near = XMVectorGetZ(Points[4]);
	m_Frustum.Far = XMVectorGetZ(Points[5]);
}

bool Camera::IsIntersectingWithFrustum( const AxisAlignedBox* pVolume, FLOAT scale, FXMVECTOR rotation, FXMVECTOR translation ) // Is the given AABB intersecting with my camera frustum ?
{
	// Transform the camera frustum from view space to the object's local space
	CamFrustum localspaceFrustum;
	TransformFrustum( &localspaceFrustum, scale, rotation, translation );

	// Perform the box/frustum intersection test in local space
	return IntersectAxisAlignedBoxFrustum( pVolume, &localspaceFrustum ) != 0;
}

#pragma region Setters

void Camera::SetLens(float fovY, float aspect, float zn, float zf)
{
	// cache properties
	mFovY = fovY;
	mAspect = aspect;
	mNearZ = zn;
	mFarZ = zf;

	mNearWindowHeight = 2.0f * mNearZ * tanf( 0.5f*mFovY );
	mFarWindowHeight  = 2.0f * mFarZ * tanf( 0.5f*mFovY );

	XMMATRIX P = XMMatrixPerspectiveFovLH(mFovY, mAspect, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);
}

void Camera::SetWorld(  XMVECTOR& translation, XMVECTOR& rotationQuat )
{
	const XMVECTOR rotOriginZero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	const XMVECTOR scaleOne = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);

	SetPosition( translation );
	SetRotation( rotationQuat );
	
	XMStoreFloat4x4( &m_World, XMMatrixAffineTransformation( scaleOne, rotOriginZero, rotationQuat, translation ) );

	mViewDirty = true;
}

void Camera::LookAtForward( FXMVECTOR playerForwards )
{
	XMVECTOR worldUp = XMVectorSet(0,1,0,0);
	XMVECTOR R = XMVector3Normalize(XMVector3Cross(worldUp, playerForwards));
	XMVECTOR U = XMVector3Cross(playerForwards, R);

	XMStoreFloat3(&mLook, playerForwards);
	XMStoreFloat3(&mRight, R);
	XMStoreFloat3(&mUp, U);

	mViewDirty = true;
}

void Camera::Walk(float d)
{
	// mPosition += d*mLook
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR l = XMLoadFloat3(&mLook);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, l, p));

	mViewDirty = true;
}

void Camera::Strafe(float d)
{
	// mPosition += d*mRight
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR r = XMLoadFloat3(&mRight);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, r, p));

	mViewDirty = true;
}

void Camera::Pitch(float angle)
{
	// Rotate up and look vector about the right vector
	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&mRight), angle);

	XMStoreFloat3(&mUp,   XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));
	
	mViewDirty = true;
}

void Camera::RotateY(float angle)
{
	// Rotate the basis vectors about the world y-axis
	XMMATRIX R = XMMatrixRotationY(angle);

	XMStoreFloat3( &mRight,   XMVector3TransformNormal( XMLoadFloat3( &mRight ), R ) );
	XMStoreFloat3( &mUp,      XMVector3TransformNormal( XMLoadFloat3( &mUp ),    R ) );
	XMStoreFloat3( &mLook,    XMVector3TransformNormal( XMLoadFloat3( &mLook ),  R ) );

	mViewDirty = true;
}

void Camera::SetPosition( const XMVECTOR& position )
{
	XMStoreFloat3( &mPosition, position );
	mViewDirty = true;
}

void Camera::SetPosition( float x, float y, float z )
{
	mPosition = XMFLOAT3(x, y, z);
	mViewDirty = true;
}

void Camera::SetRotation( const XMVECTOR& r )
{
	mRotation = r;
	
	XMMATRIX R = XMMatrixRotationRollPitchYaw( XMVectorGetX(r), XMVectorGetY(r), XMVectorGetZ(r)  );

	XMStoreFloat3(&mUp,   XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mRight, XMVector3TransformNormal(XMLoadFloat3(&mRight), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));

	mViewDirty = true;
}

// LookAt()

//void Camera::LookAt3(const XMFLOAT3& pos, const XMFLOAT3& target, const XMFLOAT3& up)
//{
//	XMVECTOR P = XMLoadFloat3(&pos);
//	XMVECTOR T = XMLoadFloat3(&target);
//	XMVECTOR U = XMLoadFloat3(&up);
//
//	LookAt(P, T, U);
//
//	mViewDirty = true;
//}
//
//void Camera::LookAt(FXMVECTOR pos, FXMVECTOR target, FXMVECTOR worldUp)
//{
//	XMVECTOR L = XMVector3Normalize(XMVectorSubtract(target, pos));
//	XMVECTOR R = XMVector3Normalize(XMVector3Cross(worldUp, L));
//	XMVECTOR U = XMVector3Cross(L, R);
//
//	XMStoreFloat3(&mPosition, pos);
//	XMStoreFloat3(&mLook, L);
//	XMStoreFloat3(&mRight, R);
//	XMStoreFloat3(&mUp, U);
//
//	mViewDirty = true;
//}

// RotateAbout()

//void Camera::RotateAbout( XMVECTOR& axis, float angle )
//{
//	XMMATRIX ROT = XMMatrixRotationAxis( axis, angle);
//
//	XMStoreFloat3( &mRight,   XMVector3TransformNormal( XMLoadFloat3( &mRight ), ROT ) );
//	XMStoreFloat3( &mUp,      XMVector3TransformNormal( XMLoadFloat3( &mUp ),    ROT ) );
//	XMStoreFloat3( &mLook,    XMVector3TransformNormal( XMLoadFloat3( &mLook ),  ROT ) );
//
//	mViewDirty = true;
//}

#pragma endregion

#pragma region Getters

XMFLOAT4X4 Camera::GetWorld() const
{
	return m_World;
}

XMMATRIX Camera::GetView()const
{
	assert(!mViewDirty);
	return XMLoadFloat4x4(&mView);
}

XMMATRIX Camera::GetProj()const
{
	return XMLoadFloat4x4(&mProj);
}


XMVECTOR Camera::GetRight()const
{
	return XMLoadFloat3(&mRight);
}

XMVECTOR Camera::GetUp()const
{
	return XMLoadFloat3(&mUp);
}

XMVECTOR Camera::GetLook()const
{
	return XMLoadFloat3(&mLook);
}

XMVECTOR Camera::GetPosition()const
{
	return XMLoadFloat3(&mPosition);
}

XMFLOAT3 Camera::GetPosition3f()const
{
	return mPosition;
}

XMVECTOR Camera::GetRotation() const
{
	return mRotation;
}

float Camera::GetNearZ()const
{
	return mNearZ;
}

float Camera::GetFarZ()const
{
	return mFarZ;
}

// Aspect 1.77777:
// 2560 x 1440
// 1920 x 1080
// 1280 x 720 
// 
// Aspect 1.33333:
// Aspect 1.33333
// 1280 x 960
// 1024 x 768
//  800 x 600
float Camera::GetAspect() const
{
	return mAspect;
}

float Camera::GetFovY()const
{
	return mFovY;
}

float Camera::GetFovX()const
{
	float halfWidth = 0.5f * GetNearWindowWidth();
	return 2.0f * (float)atan(halfWidth / mNearZ);
}

float Camera::GetNearWindowWidth()const
{
	return mAspect * mNearWindowHeight;
}

float Camera::GetNearWindowHeight()const
{
	return mNearWindowHeight;
}

float Camera::GetFarWindowWidth()const
{
	return mAspect * mFarWindowHeight;
}

float Camera::GetFarWindowHeight()const
{
	return mFarWindowHeight;
}

// Unused Getters

//XMFLOAT4X4 Camera::GetView4x4f()const
//{
//	assert(!mViewDirty);
//	return mView;
//}
//
XMFLOAT4X4 Camera::GetProj4x4f() const
{
	return mProj;
}

//XMFLOAT3 Camera::GetRight3f()const
//{
//	return mRight;
//}
//
//XMFLOAT3 Camera::GetUp3f()const
//{
//	return mUp;
//}
//
//XMFLOAT3 Camera::GetLook3f()const
//{
//	return mLook;
//}

#pragma endregion

void Camera::TransformFrustum( CamFrustum* pOut, FLOAT scale, FXMVECTOR rotation, FXMVECTOR translation ) // Transform the frustum by an angle preserving transform
{
    assert( pOut );
    assert( XMQuaternionIsUnit( rotation ) );

    // Load the frustum.
    XMVECTOR Origin = XMLoadFloat3( &m_Frustum.Origin );
    XMVECTOR Orientation = XMLoadFloat4( &m_Frustum.Orientation );

    assert( XMQuaternionIsUnit( Orientation ) );

    // Composite the frustum rotation and the transform rotation
    Orientation = XMQuaternionMultiply( Orientation, rotation );

    // Transform the origin.
    Origin = XMVector3Rotate( Origin * XMVectorReplicate( scale ), rotation ) + translation;

    // Store the frustum into the given frustum
    XMStoreFloat3( &pOut->Origin, Origin );
    XMStoreFloat4( &pOut->Orientation, Orientation );

    // Scale the near and far distances (the slopes remain the same)
    pOut->Near = m_Frustum.Near * scale;
    pOut->Far = m_Frustum.Far * scale;

    // Copy the slopes.
    pOut->RightSlope = m_Frustum.RightSlope;
    pOut->LeftSlope = m_Frustum.LeftSlope;
    pOut->TopSlope = m_Frustum.TopSlope;
    pOut->BottomSlope = m_Frustum.BottomSlope;
}

// IntersectAxisAlignedBoxFrustum
// Exact axis alinged box vs frustum test.  Constructs an oriented box and uses
// the oriented box vs frustum test
//
// Return values: 0 = no intersection, 
//                1 = intersection, 
//                2 = box is completely inside frustum
//
int Camera::IntersectAxisAlignedBoxFrustum( const AxisAlignedBox* pVolumeA, const CamFrustum* pVolumeB )
{
    assert( pVolumeA );
    assert( pVolumeB );

    // Make the axis aligned box oriented and do an OBB vs frustum test
    OrientedBox BoxA;

    BoxA.Center = pVolumeA->Center;
    BoxA.Extents = pVolumeA->Extents;
    BoxA.Orientation.x = 0.0f;
    BoxA.Orientation.y = 0.0f;
    BoxA.Orientation.z = 0.0f;
    BoxA.Orientation.w = 1.0f;

    return IntersectOrientedBoxFrustum( &BoxA, pVolumeB );
}

// IntersectOrientedBoxFrustum
// Exact oriented box vs frustum test
// Return values: 0 = no intersection, 
//                1 = intersection, 
//                2 = box is completely inside frustum
//
int Camera::IntersectOrientedBoxFrustum( const OrientedBox* pVolumeA, const CamFrustum* pVolumeB )
{
    assert( pVolumeA );
    assert( pVolumeB );

    static const XMVECTORI32 SelectY =
    {
        (int)XM_SELECT_0, (int)XM_SELECT_1, (int)XM_SELECT_0, (int)XM_SELECT_0
    };
    static const XMVECTORI32 SelectZ =
    {
		(int)XM_SELECT_0, (int)XM_SELECT_0, (int)XM_SELECT_1, (int)XM_SELECT_0
    };

    XMVECTOR Zero = XMVectorZero();

    // Build the frustum planes
    XMVECTOR Planes[6];
    Planes[0] = XMVectorSet( 0.0f, 0.0f, -1.0f, pVolumeB->Near );
    Planes[1] = XMVectorSet( 0.0f, 0.0f, 1.0f, -pVolumeB->Far );
    Planes[2] = XMVectorSet( 1.0f, 0.0f, -pVolumeB->RightSlope, 0.0f );
    Planes[3] = XMVectorSet( -1.0f, 0.0f, pVolumeB->LeftSlope, 0.0f );
    Planes[4] = XMVectorSet( 0.0f, 1.0f, -pVolumeB->TopSlope, 0.0f );
    Planes[5] = XMVectorSet( 0.0f, -1.0f, pVolumeB->BottomSlope, 0.0f );

    // Load origin and orientation of the frustum
    XMVECTOR Origin = XMLoadFloat3( &pVolumeB->Origin );
    XMVECTOR FrustumOrientation = XMLoadFloat4( &pVolumeB->Orientation );

    assert( XMQuaternionIsUnit( FrustumOrientation ) );

    // Load the box.
    XMVECTOR Center = XMLoadFloat3( &pVolumeA->Center );
    XMVECTOR Extents = XMLoadFloat3( &pVolumeA->Extents );
    XMVECTOR BoxOrientation = XMLoadFloat4( &pVolumeA->Orientation );

    assert( XMQuaternionIsUnit( BoxOrientation ) );

    // Transform the oriented box into the space of the frustum in order to 
    // minimize the number of transforms we have to do
    Center = XMVector3InverseRotate( Center - Origin, FrustumOrientation );
    BoxOrientation = XMQuaternionMultiply( BoxOrientation, XMQuaternionConjugate( FrustumOrientation ) );

    // Set w of the center to one so we can dot4 with the plane.
    Center = XMVectorInsert( Center, XMVectorSplatOne(), 0, 0, 0, 0, 1);

    // Build the 3x3 rotation matrix that defines the box axes
    XMMATRIX R = XMMatrixRotationQuaternion( BoxOrientation );

    // Check against each plane of the frustum.
    XMVECTOR Outside = XMVectorFalseInt();
    XMVECTOR InsideAll = XMVectorTrueInt();
    XMVECTOR CenterInsideAll = XMVectorTrueInt();

    for( INT i = 0; i < 6; i++ )
    {
        // Compute the distance to the center of the box
        XMVECTOR Dist = XMVector4Dot( Center, Planes[i] );

        // Project the axes of the box onto the normal of the plane.  Half the
        // length of the projection (sometime called the "radius") is equal to
        // h(u) * abs(n dot b(u))) + h(v) * abs(n dot b(v)) + h(w) * abs(n dot b(w))
        // where h(i) are extents of the box, n is the plane normal, and b(i) are the 
        // axes of the box
        XMVECTOR Radius = XMVector3Dot( Planes[i], R.r[0] );
        Radius = XMVectorSelect( Radius, XMVector3Dot( Planes[i], R.r[1] ), SelectY );
        Radius = XMVectorSelect( Radius, XMVector3Dot( Planes[i], R.r[2] ), SelectZ );
        Radius = XMVector3Dot( Extents, XMVectorAbs( Radius ) );

        // Outside the plane?
        Outside = XMVectorOrInt( Outside, XMVectorGreater( Dist, Radius ) );

        // Fully inside the plane?
        InsideAll = XMVectorAndInt( InsideAll, XMVectorLessOrEqual( Dist, -Radius ) );

        // Check if the center is inside the plane
        CenterInsideAll = XMVectorAndInt( CenterInsideAll, XMVectorLessOrEqual( Dist, Zero ) );
    }

    // If the box is outside any of the planes it is outside
    if ( XMVector4EqualInt( Outside, XMVectorTrueInt() ) )
        return 0;

    // If the box is inside all planes it is fully inside
    if ( XMVector4EqualInt( InsideAll, XMVectorTrueInt() ) )
        return 2;

    // If the center of the box is inside all planes and the box intersects 
    // one or more planes then it must intersect
    if ( XMVector4EqualInt( CenterInsideAll, XMVectorTrueInt() ) )
        return 1;

    // Build the corners of the frustum.
    XMVECTOR RightTop = XMVectorSet( pVolumeB->RightSlope, pVolumeB->TopSlope, 1.0f, 0.0f );
    XMVECTOR RightBottom = XMVectorSet( pVolumeB->RightSlope, pVolumeB->BottomSlope, 1.0f, 0.0f );
    XMVECTOR LeftTop = XMVectorSet( pVolumeB->LeftSlope, pVolumeB->TopSlope, 1.0f, 0.0f );
    XMVECTOR LeftBottom = XMVectorSet( pVolumeB->LeftSlope, pVolumeB->BottomSlope, 1.0f, 0.0f );
    XMVECTOR Near = XMVectorReplicatePtr( &pVolumeB->Near );
    XMVECTOR Far = XMVectorReplicatePtr( &pVolumeB->Far );

    XMVECTOR Corners[8];
    Corners[0] = RightTop * Near;
    Corners[1] = RightBottom * Near;
    Corners[2] = LeftTop * Near;
    Corners[3] = LeftBottom * Near;
    Corners[4] = RightTop * Far;
    Corners[5] = RightBottom * Far;
    Corners[6] = LeftTop * Far;
    Corners[7] = LeftBottom * Far;

    // Test against box axes (3)
    {
        // Find the min/max values of the projection of the frustum onto each axis.
        XMVECTOR FrustumMin, FrustumMax;

        FrustumMin = XMVector3Dot( Corners[0], R.r[0] );
        FrustumMin = XMVectorSelect( FrustumMin, XMVector3Dot( Corners[0], R.r[1] ), SelectY );
        FrustumMin = XMVectorSelect( FrustumMin, XMVector3Dot( Corners[0], R.r[2] ), SelectZ );
        FrustumMax = FrustumMin;

        for( INT i = 1; i < 8; i++ )
        {
            XMVECTOR Temp = XMVector3Dot( Corners[i], R.r[0] );
            Temp = XMVectorSelect( Temp, XMVector3Dot( Corners[i], R.r[1] ), SelectY );
            Temp = XMVectorSelect( Temp, XMVector3Dot( Corners[i], R.r[2] ), SelectZ );

            FrustumMin = XMVectorMin( FrustumMin, Temp );
            FrustumMax = XMVectorMax( FrustumMax, Temp );
        }

        // Project the center of the box onto the axes.
        XMVECTOR BoxDist = XMVector3Dot( Center, R.r[0] );
        BoxDist = XMVectorSelect( BoxDist, XMVector3Dot( Center, R.r[1] ), SelectY );
        BoxDist = XMVectorSelect( BoxDist, XMVector3Dot( Center, R.r[2] ), SelectZ );

        // The projection of the box onto the axis is just its Center and Extents.
        // if (min > box_max || max < box_min) reject;
        XMVECTOR Result = XMVectorOrInt( XMVectorGreater( FrustumMin, BoxDist + Extents ),
                                          XMVectorLess( FrustumMax, BoxDist - Extents ) );

        if( XMVector3AnyTrue( Result ) )
            return 0;
    }

    // Test against edge/edge axes (3*6).
    XMVECTOR FrustumEdgeAxis[6];

    FrustumEdgeAxis[0] = RightTop;
    FrustumEdgeAxis[1] = RightBottom;
    FrustumEdgeAxis[2] = LeftTop;
    FrustumEdgeAxis[3] = LeftBottom;
    FrustumEdgeAxis[4] = RightTop - LeftTop;
    FrustumEdgeAxis[5] = LeftBottom - LeftTop;

    for( INT i = 0; i < 3; i++ )
    {
        for( INT j = 0; j < 6; j++ )
        {
            // Compute the axis we are going to test.
            XMVECTOR Axis = XMVector3Cross( R.r[i], FrustumEdgeAxis[j] );

            // Find the min/max values of the projection of the frustum onto the axis.
            XMVECTOR FrustumMin, FrustumMax;

            FrustumMin = FrustumMax = XMVector3Dot( Axis, Corners[0] );

            for( INT k = 1; k < 8; k++ )
            {
                XMVECTOR Temp = XMVector3Dot( Axis, Corners[k] );
                FrustumMin = XMVectorMin( FrustumMin, Temp );
                FrustumMax = XMVectorMax( FrustumMax, Temp );
            }

            // Project the center of the box onto the axis.
            XMVECTOR Dist = XMVector3Dot( Center, Axis );

            // Project the axes of the box onto the axis to find the "radius" of the box.
            XMVECTOR Radius = XMVector3Dot( Axis, R.r[0] );
            Radius = XMVectorSelect( Radius, XMVector3Dot( Axis, R.r[1] ), SelectY );
            Radius = XMVectorSelect( Radius, XMVector3Dot( Axis, R.r[2] ), SelectZ );
            Radius = XMVector3Dot( Extents, XMVectorAbs( Radius ) );

            // if (center > max + radius || center < min - radius) reject;
            Outside = XMVectorOrInt( Outside, XMVectorGreater( Dist, FrustumMax + Radius ) );
            Outside = XMVectorOrInt( Outside, XMVectorLess( Dist, FrustumMin - Radius ) );
        }
    }

    if ( XMVector4EqualInt( Outside, XMVectorTrueInt() ) )
        return 0;

    // If we did not find a separating plane then the box must intersect the frustum.
    return 1;
}

int Camera::XMQuaternionIsUnit( FXMVECTOR Q ) // Return TRUE if the quaterion is a unit quaternion
{
    XMVECTOR Difference = XMVector4Length( Q ) - XMVectorSplatOne();

    return XMVector4Less( XMVectorAbs( Difference ), gUnitQuaternionEpsilon );
}

int Camera::XMVector3AnyTrue( FXMVECTOR V )
{
    XMVECTOR C;

    // Duplicate the fourth element from the first element.
    C = XMVectorSwizzle( V, 0, 1, 2, 0 );

    return XMComparisonAnyTrue( XMVector4EqualIntR( C, XMVectorTrueInt() ) );
}
