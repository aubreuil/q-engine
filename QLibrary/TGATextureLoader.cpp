#include "TGATextureLoader.h"
#include "DDSTextureLoader.h" // only for the BitsPerPixel() static definition
//#include <fstream>
//#include <string>
//#include <cassert>

using namespace std;

TGATextureLoader::TGATextureLoader()
{
	
}

TGATextureLoader::~TGATextureLoader()
{
	UINT count = (UINT)m_pDatas.size();
	for ( UINT n = 0 ; n < count ; n++ )
		delete [] m_pDatas[n];
}
HRESULT  TGATextureLoader::LoadTGAFromFile( const wstring& fileName, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs,  bool sRGB )
{
	ByteArray ba = ReadFileSync( fileName );
	if ( ba->size() > 0 )
	{
		CreateTGAFromMemory( fileName, ba->data(), ba->size(), texDesc, subs, sRGB );
		return S_OK;
	}

	wstring err = L"\n>>>>>>>>>>>>>>>>>>>>>>>>>>> TGATextureLoader cannot read the file " + fileName + L"\n!!";
	OutputDebugString(err.c_str());

	return E_FAIL;
}

void TGATextureLoader::CreateTGAFromMemory( const wstring& fileName, const void* _filePtr, size_t, D3D12_RESOURCE_DESC& texDesc, D3D12_SUBRESOURCE_DATA& subs, bool sRGB )
{
	const uint8_t* filePtr = (const uint8_t*)_filePtr;

	// Skip first two bytes
	filePtr += 2;

	uint8_t imageTypeCode = *filePtr++;

	// Ignore another 9 bytes
	filePtr += 9;

	uint16_t imageWidth = *(uint16_t*)filePtr;
	filePtr += sizeof(uint16_t);
	uint16_t imageHeight = *(uint16_t*)filePtr;
	filePtr += sizeof(uint16_t);
	uint8_t bitCount = *filePtr++;
	uint8_t numChannels = bitCount / 8;

	//std::wstring err = L"FILE:" + fileName + L" TYPE:" + to_wstring(imageTypeCode) + L" BITS:" + to_wstring(bitCount) + L" CHANNELS:" + to_wstring(numChannels) + L"\n";
	//OutputDebugString(err.c_str());
	
	uint32_t numBytes = imageWidth * imageHeight * numChannels;

	// Ignore another byte
	filePtr++;
	
	uint32_t* pData = new uint32_t[numBytes]; 
	uint32_t* iter = pData;

	m_pDatas.push_back(pData);

	switch (numChannels)
	{
	default:
		break;
	case 3:
		for (uint32_t byteIdx = 0; byteIdx < numBytes; byteIdx += 3)
		{
			*iter++ = 0xff000000 | filePtr[0] << 16 | filePtr[1] << 8 | filePtr[2];
			filePtr += 3;
		}
		break;
	case 4:
		for (uint32_t byteIdx = 0; byteIdx < numBytes; byteIdx += 4) // byteIdx was 1044268, numBytes was 1048576
		{
			*iter++ = filePtr[3] << 24 | filePtr[0] << 16 | filePtr[1] << 8 | filePtr[2];
			filePtr += 4;
		}
		break;
	}

	texDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	texDesc.Width = imageWidth;
	texDesc.Height = imageHeight;
	texDesc.DepthOrArraySize = 1;
	texDesc.MipLevels = 1;
	texDesc.Format = sRGB ? DXGI_FORMAT_R8G8B8A8_UNORM_SRGB : DXGI_FORMAT_R8G8B8A8_UNORM;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	texDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	subs.pData = pData;
	subs.RowPitch = imageWidth * BytesPerPixel(texDesc.Format);
	subs.SlicePitch = subs.RowPitch * imageHeight;
}

void TGATextureLoader::Create( size_t Width, size_t Height, DXGI_FORMAT Format, const void* InitialData )
{
	D3D12_RESOURCE_DESC texDesc = {};
	texDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	texDesc.Width = Width;
	texDesc.Height = (UINT)Height;
	texDesc.DepthOrArraySize = 1;
	texDesc.MipLevels = 1;
	texDesc.Format = Format;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	texDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	D3D12_SUBRESOURCE_DATA texResource;
	texResource.pData = InitialData;
	texResource.RowPitch = Width * BytesPerPixel(Format);
	texResource.SlicePitch = texResource.RowPitch * Height;
}

ByteArray TGATextureLoader::ReadFileSync( const wstring& fileName )
{
	return ReadFileHelperEx(make_shared<wstring>(fileName));
}

ByteArray TGATextureLoader::ReadFileHelperEx( shared_ptr<wstring> fileName)
{
	return ReadFileHelper(*fileName);
}

ByteArray TGATextureLoader::ReadFileHelper(const wstring& fileName)
{
	struct _stat64 fileStat;
	int fileExists = _wstat64(fileName.c_str(), &fileStat);
	if (fileExists == -1)
		return NullFile;

	std::ifstream file = std::ifstream( fileName, ios::in | ios::binary );
	if (!file)
		return NullFile;

	ByteArray byteArray = make_shared<vector<BYTE> >( file.seekg(0, ios::end).tellg() );
	file.seekg(0, ios::beg).read( (char*)byteArray->data(), byteArray->size() );
	file.close();

	assert(byteArray->size() == fileStat.st_size);

	return byteArray;
}

UINT TGATextureLoader::BytesPerPixel( DXGI_FORMAT Format )
{
	return (UINT)BitsPerPixel(Format) / 8;
};
