#pragma once

#include "pch.h"
#include "GameTimer.h"
#include "Camera.h"

class QApp
{
protected:
								QApp( HINSTANCE hInstance );
	virtual						~QApp();
	QApp( const QApp& rhs ) = delete;
    QApp& operator=( const QApp& rhs ) = delete;

public:
    static QApp*				GetApp();
	
	HINSTANCE					AppInst() const;
	HWND						MainWnd() const;

	virtual bool				Initialize();
    virtual LRESULT				MsgProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	float						AspectRatio() const;
    bool						Get4xMsaaState() const;
    void						Set4xMsaaState( bool value );
	int							Run();

protected:
    virtual void 				CreateRtvAndDsvDescriptorHeaps();
	virtual void 				OnResize();
	virtual void 				Update( const GameTimer& gt ) = 0;
    virtual void 				Draw( const GameTimer& gt ) = 0;
	virtual void				OnKeyUp( WPARAM wParam ) { }
	virtual void 				OnMouseDown( WPARAM btnState, int x, int y ) { }
	virtual void				OnMouseUp( WPARAM btnState, int x, int y )   { }
	virtual void				OnMouseMove( WPARAM btnState, int x, int y ) { }
	virtual void                OnMouseWheel( short delta ) { }

	// Initialisation...
	bool						InitMainWindow();
	bool						InitDirect3D();
	void						CreateCommandObjects();
    void						CreateSwapChain();
	void						FlushCommandQueue();
	ID3D12Resource*				CurrentBackBuffer() const;
	D3D12_CPU_DESCRIPTOR_HANDLE	CurrentBackBufferView() const;
	D3D12_CPU_DESCRIPTOR_HANDLE	DepthStencilView() const;
	void						CalculateFrameStats();
    void						LogAdapters();
    void 						LogAdapterOutputs( IDXGIAdapter* adapter );
    void 						LogOutputDisplayModes( IDXGIOutput* output, DXGI_FORMAT format );

protected:
    static QApp*										m_App;
	static const int									SwapChainBufferCount = 2;
	
	// protected members initialised in this order...
	HINSTANCE											m_hAppInst = nullptr;		// application instance handle
	HWND												m_hMainWnd = nullptr;		// main window handle
	int													m_ClientWidth = 1280; 
	int													m_ClientHeight = 720;		
	std::wstring										m_hMainWndCaption = L"Q's Library"; // Derived class should set these in derived constructor to customize starting values....
	D3D_DRIVER_TYPE										m_DriverType = D3D_DRIVER_TYPE_HARDWARE;
	DXGI_FORMAT											m_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	DXGI_FORMAT											m_DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
    Microsoft::WRL::ComPtr<IDXGIFactory4>				m_DxgiFactory;
	Microsoft::WRL::ComPtr<ID3D12Device>				m_Device;
	Microsoft::WRL::ComPtr<ID3D12Fence>					m_Fence;
	UINT64												m_CurrentFence = 0;
	UINT												m_RtvDescriptorSize = 0;
	UINT												m_DsvDescriptorSize = 0;
	UINT												m_CbvSrvUavDescriptorSize = 0;
	UINT      											m_4xMsaaQuality = 0;			// quality level of 4X MSAA
	Microsoft::WRL::ComPtr<ID3D12CommandQueue>			m_CommandQueue;
	Microsoft::WRL::ComPtr<ID3D12CommandAllocator>		m_DirectCmdListAlloc;
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_CommandList;
	Microsoft::WRL::ComPtr<IDXGISwapChain>				m_SwapChain;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_RtvHeap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_DsvHeap;
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_SwapChainBuffer[SwapChainBufferCount];
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_DepthStencilBuffer;
	D3D12_VIEWPORT										m_ScreenViewport;
	D3D12_RECT											m_ScissorRect;
	int													m_CurrBackbuffer = 0;
	bool      											m_Resizing = false;			// are the resize bars being dragged?
	bool												m_Minimized = false;			// is the application minimized?
	bool      											m_Maximized = false;			// is the application maximized?
	bool												m_AppPaused = false;			// is the application paused?
    bool      											m_4xMsaaState = false;		// 4X MSAA enabled
	GameTimer											m_Timer;
	Camera												m_Camera;


};

