#pragma once
//
// Simple first person style camera class that lets the viewer explore the 3D scene.
//   -It keeps track of the camera coordinate system relative to the world space
//    so that the view matrix can be constructed.  
//   -It keeps track of the viewing frustum of the camera so that the projection
//    matrix can be obtained.

#include "pch.h"
#include "Maths.h"

#define CAM_NEAR_Z 0.3f
#define VERTICALFOV_DEGREES 60.0f

static const DirectX::XMVECTOR gUnitQuaternionEpsilon =
{
    1.0e-4f, 1.0e-4f, 1.0e-4f, 1.0e-4f
};

struct CamFrustum
{
    DirectX::XMFLOAT3 Origin;            // Origin of the frustum (and projection)
    DirectX::XMFLOAT4 Orientation;       // Unit quaternion representing rotation

    float RightSlope;           // Positive X slope (X/Z)
    float LeftSlope;            // Negative X slope
    float TopSlope;             // Positive Y slope (Y/Z)
    float BottomSlope;          // Negative Y slope
    float Near, Far;            // Z of the near plane and far plane
};

struct AxisAlignedBox
{
    DirectX::XMFLOAT3 Center;            // Center of the box
    DirectX::XMFLOAT3 Extents;           // Distance from the center to each side
};

struct OrientedBox
{
    DirectX::XMFLOAT3 Center;            // Center of the box
    DirectX::XMFLOAT3 Extents;           // Distance from the center to each side
    DirectX::XMFLOAT4 Orientation;       // Unit quaternion representing rotation (box -> world)
};

class Camera
{
public:
						Camera();
						~Camera();

	void 				UpdateViewMatrix();								// Call to rebuild the view matrix after modifying camera position/orientation
	void				ComputeFrustum();
	bool				IsIntersectingWithFrustum( const AxisAlignedBox* pVolume, FLOAT scale, DirectX::FXMVECTOR rotation, DirectX::FXMVECTOR translation  );

	void 				SetLens( float fovY, float aspect, float zn, float zf );
	void				SetWorld(  DirectX::XMVECTOR& translation, DirectX::XMVECTOR& rotationQuat );
	void				LookAtForward( DirectX::FXMVECTOR playerForwards );
	void 				Walk( float d );
	void 				Strafe( float d );
	void 				Pitch( float angle );
	void 				RotateY( float angle );
	void				SetPosition( const DirectX::XMVECTOR& v );
	void				SetPosition( float x, float y, float z );
	void				SetRotation( const DirectX::XMVECTOR& r );
		
	DirectX::XMFLOAT4X4 GetWorld() const;
	DirectX::XMMATRIX   GetView() const;
	DirectX::XMMATRIX   GetProj() const;
	DirectX::XMFLOAT4X4 GetProj4x4f() const;
	DirectX::XMVECTOR	GetRight() const;		// Get camera basis vectors.
	DirectX::XMVECTOR	GetUp() const;
	DirectX::XMVECTOR	GetLook() const;
	DirectX::XMVECTOR	GetPosition() const;
	DirectX::XMFLOAT3	GetPosition3f() const;
	DirectX::XMVECTOR	GetRotation() const;
		
	float				GetNearZ() const;		// Get frustum properties
	float 				GetFarZ() const;
	float 				GetAspect() const;
	float 				GetFovY() const;
	float 				GetFovX() const;
	float 				GetNearWindowWidth() const;	// Get near and far plane dimensions in view space coordinates
	float 				GetNearWindowHeight() const;
	float 				GetFarWindowWidth() const;
	float 				GetFarWindowHeight() const;

private:
	void				TransformFrustum( CamFrustum* pOut, FLOAT scale, DirectX::FXMVECTOR rotation, DirectX::FXMVECTOR translation );
	int					IntersectAxisAlignedBoxFrustum( const AxisAlignedBox* pVolumeA, const CamFrustum* pVolumeB );
	int					IntersectOrientedBoxFrustum( const OrientedBox* pVolumeA, const CamFrustum* pVolumeB );
	int					XMQuaternionIsUnit( DirectX::FXMVECTOR Q );
	int					XMVector3AnyTrue( DirectX::FXMVECTOR V );

private:
	DirectX::XMFLOAT4X4 m_World = Maths::Identity4x4();
	DirectX::XMFLOAT4X4 mView = Maths::Identity4x4();		// Cache View/Proj matrices
	DirectX::XMFLOAT4X4 mProj = Maths::Identity4x4();
	DirectX::XMFLOAT3	mPosition	= { 0.0f, 0.0f, 0.0f };	// Camera coordinate system with coordinates relative to world space
	DirectX::XMFLOAT3	mRight		= { 1.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3	mUp			= { 0.0f, 1.0f, 0.0f };
	DirectX::XMFLOAT3	mLook		= { 0.0f, 0.0f, 1.0f };
	DirectX::XMVECTOR   mRotation   = { 0.0f, 0.0f, 0.0f, 1.0f };
	float				mNearZ		= 0.0f;					// Cache frustum properties
	float 				mFarZ		= 0.0f;
	float 				mAspect		= 0.0f;
	float 				mFovY		= 0.0f;
	float 				mNearWindowHeight = 0.0f;
	float 				mFarWindowHeight = 0.0f;
	bool 				mViewDirty = true;

	CamFrustum			m_Frustum;
};

