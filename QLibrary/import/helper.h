//FBX Loading
namespace Fbx
{
    class exception : std::exception
    {
        using base = std::exception;

        public:
        exception(const char* message) : base(message)
        {

        }
    };

    template < typename t, typename ...args >
    inline void throw_exception(args&&... a)
    {
        throw t(std::forward<args>(a)...);
    }


    template <typename t>
    struct fbxsdk_object_deleter
    {
        inline void operator()(t* m) const
        {
            m->Destroy();
        }
    };

    using fbxmanager_deleter = fbxsdk_object_deleter < FbxManager >;
    using fbxscene_deleter = fbxsdk_object_deleter < FbxScene >;
    using fbximporter_deleter = fbxsdk_object_deleter < FbxImporter >;

    struct fbx_context
    {
        std::unique_ptr<fbxsdk::FbxManager, fbxmanager_deleter>     m_manager;
        std::unique_ptr<fbxsdk::FbxScene, fbxscene_deleter>         m_scene;
        std::unique_ptr<fbxsdk::FbxImporter, fbximporter_deleter>   m_importer;
        bool                                                        m_coordinate_system_swap_y_z;
        bool                                                        m_invert_handness;
    };


    std::unique_ptr<fbx_context> load_fbx_file(const std::string& file_name)
    {
        std::unique_ptr<fbxsdk::FbxManager, fbxmanager_deleter>     manager(fbxsdk::FbxManager::Create(), fbxmanager_deleter());
        std::unique_ptr<fbxsdk::FbxScene, fbxscene_deleter>         scene(fbxsdk::FbxScene::Create(manager.get(), ""), fbxscene_deleter());
        std::unique_ptr<fbxsdk::FbxImporter, fbximporter_deleter>   importer(fbxsdk::FbxImporter::Create(manager.get(), ""), fbximporter_deleter());

        auto f = file_name;

        auto import_status = importer->Initialize(f.c_str(), -1, manager->GetIOSettings());

        if (import_status == false)
        {
            auto status = importer->GetStatus();
            auto error = status.GetErrorString();
            throw_exception<exception>(error);
        }

        import_status = importer->Import(scene.get());
        if (import_status == false)
        {
            auto status = importer->GetStatus();
            auto error = status.GetErrorString();
            throw_exception<exception>(error);
        }

        FbxGeometryConverter geometryConverter(manager.get());
        geometryConverter.Triangulate(scene.get(), true);

        fbxsdk::FbxAxisSystem scene_axis_system = scene->GetGlobalSettings().GetAxisSystem();
        fbxsdk::FbxAxisSystem our_axis_system = fbxsdk::FbxAxisSystem(fbxsdk::FbxAxisSystem::EPreDefinedAxisSystem::eDirectX);

        if (scene_axis_system != our_axis_system)
        {
            our_axis_system.ConvertScene(scene.get());
        }

        fbxsdk::FbxSystemUnit units = scene->GetGlobalSettings().GetSystemUnit();
        fbxsdk::FbxSystemUnit meters = fbxsdk::FbxSystemUnit::m;

        if (units != fbxsdk::FbxSystemUnit::m)
        {
            //FbxSystemUnit::m.ConvertScene(scene.get());
        }

        auto r = std::make_unique<fbx_context>();
        r->m_manager = std::move(manager);
        r->m_scene = std::move(scene);
        r->m_importer = std::move(importer);
        {
            int32_t sign;
            r->m_coordinate_system_swap_y_z = scene_axis_system.GetUpVector(sign) == fbxsdk::FbxAxisSystem::eZAxis;
            r->m_invert_handness            = false; //todo: from maya, always transform
        }

        return r;
    }






}

