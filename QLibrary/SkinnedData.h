#pragma once

#include "pch.h"

///<summary>
/// A Keyframe defines the bone transformation at an instant in time.
///</summary>
struct Keyframe
{
	Keyframe();
	~Keyframe();

    float TimePos;
	DirectX::XMFLOAT3 Translation;
    DirectX::XMFLOAT3 Scale;
    DirectX::XMFLOAT4 RotationQuat;
};

///<summary>
/// A BoneAnimation is defined by a list of keyframes.  For time
/// values inbetween two keyframes, we interpolate between the
/// two nearest keyframes that bound the time.  
///
/// We assume an animation always has two keyframes.
///</summary>
struct BoneAnimation
{
	float GetStartTime()const;
	float GetEndTime()const;

    void Interpolate( float t, DirectX::XMFLOAT4X4& M ) const;
	void GetRootPose( DirectX::XMFLOAT4X4& M ) const;
	std::vector<Keyframe> Keyframes; 	
};

///<summary>
/// Examples of AnimationClips are "Walk", "Run", "Attack", "Defend".
/// An AnimationClip requires a BoneAnimation for every bone to form
/// the animation clip.    
///</summary>
struct AnimationClip
{
	float GetClipStartTime()const;
	float GetClipEndTime()const;

    void Interpolate( float t, std::vector<DirectX::XMFLOAT4X4>& boneTransforms ) const;
	
    std::vector<BoneAnimation> mBoneAnimations; 	
};

struct SkinnedVertex
{
    DirectX::XMFLOAT3 Pos;
    DirectX::XMFLOAT3 Normal;
    DirectX::XMFLOAT2 TexC;
    DirectX::XMFLOAT3 TangentU;
	DirectX::XMFLOAT4 BoneWeights = DirectX::XMFLOAT4(0,0,0, 0);
	BYTE			  BoneIndices[4] = {0};
};

struct SkinnedMesh
{
   
	std::string					mMeshName;
	std::string					mMaterialName;
	DirectX::XMFLOAT4X4			mModelWorld;
	DirectX::XMFLOAT4X4			mTextureTransform;
	std::vector<SkinnedVertex>	mVertices;
	std::vector<USHORT>			mIndices;
	DirectX::BoundingBox		mBounds;
};

class SkinnedData
{
public:

	UINT BoneCount()const;

	float GetClipStartTime( const std::string& clipName ) const;
	float GetClipEndTime( const std::string& clipName ) const;

	void  Set( std::vector<int8_t>& boneHierarchy, std::vector<DirectX::XMFLOAT4X4>& boneOffsets, std::unordered_map<std::string, AnimationClip>& animations );

	void GetFinalTransforms( float timePos, const std::string& clipName, std::vector<DirectX::XMFLOAT4X4>& finalTransforms ) const;

    // In a real project, you'd want to cache the result if there was a chance
	 // that you were calling this several times with the same clipName at 
	 // the same timePos.
    
private:
	std::vector<int8_t>  							mBoneHierarchy; /// parent index of ith bone
	std::vector<DirectX::XMFLOAT4X4>				mJointLocalTransforms;
	std::vector<DirectX::XMFLOAT4X4>				mJointGlobalTransforms;
	std::vector<DirectX::XMFLOAT4X4>				mBoneOffsets;
	std::unordered_map<std::string, AnimationClip>	mAnimations;
};
 
