#pragma once

#include "private/ImporterDefines.h"
#include <cstdint>
#include <vector>
#include <memory>
#include <string>

namespace Importer
{
  enum ApiResult
  {
    Success,
    Error
  };

  struct Point3
  {
    float m_x;
    float m_y;
    float m_z;
  };

  struct Point2
  {
      float m_x;
      float m_y;
  };

  struct Vector3
  {
      float m_x;
      float m_y;
      float m_z;
  };

  struct Byte4
  {
      uint8_t m_x;
      uint8_t m_y;
      uint8_t m_z;
      uint8_t m_w;
  };

  struct Weight4
  {
      float m_x;
      float m_y;
      float m_z;
      float m_w;
  };

  struct UV
  {
      float m_u;
      float m_v;
  };

    struct Triangle32Bit
  {
      uint32_t m_i0;
      uint32_t m_i1;
      uint32_t m_i2;
  };

  struct Triangle16Bit
  {
      uint16_t m_i0;
      uint16_t m_i1;
      uint16_t m_i2;
  };

  struct Mesh
  {
      std::vector<Point3>   m_positions;
      std::vector<Vector3>  m_normals;
  };

  struct SkinnedMesh
  {
      std::vector<Point3>   m_positions;
      std::vector<Vector3>  m_normals;
      std::vector<Vector3>  m_tangents;
      std::vector<Weight4>  m_blend_weights;
      std::vector<Byte4>    m_blend_indices;
      std::vector<UV>       m_uv;

      std::vector<Triangle32Bit> m_triangles;
  };

  struct Material
  {
      std::string m_diffuse;
      std::string m_normal;
      std::string m_specular;
      std::string m_glossiness;
  };

  struct StaticMesh
  {
      std::vector<Point3>   m_positions;
      std::vector<Vector3>  m_normals;
      std::vector<Vector3>  m_tangents;
      std::vector<Vector3>  m_bitangents;
      std::vector<Point2>   m_uv;
      std::vector<Triangle32Bit> m_triangles;
  };

  struct MaterialMeshTable
  {
      uint16_t m_material;
      uint16_t m_mesh;
  };
  
  struct MeshMaterialTable
  {
      uint16_t m_mesh;
      uint16_t m_material;
  };

  struct StaticMeshBounds
  {
      Point3 m_aabb_min;
      Point3 m_aabb_max;
  };
  
  struct StaticModel
  {
      std::vector<std::unique_ptr<StaticMesh>> m_mesh;
      std::vector<std::unique_ptr<Material>>   m_material;
      std::vector< MaterialMeshTable>          m_material_mesh_table;   //for data oriented design, normalization of the data, like in sql tables
      std::vector< MeshMaterialTable>          m_mesh_material_table;
      std::vector< StaticMeshBounds >          m_mesh_bounds;
  };

  DLL_API  ApiResult ImportFbxMesh(const char* file_name, Mesh*& mesh );
  DLL_API  ApiResult ImportFbxSkinnedMesh(const char* file_name, SkinnedMesh*& mesh);
  DLL_API  ApiResult ImportFbxStaticModel(const char* file_name, StaticModel*& model);

  //DLL_API  ApiResult ImportFbxSkeleton(const char* file_name, Skeleton*& mesh);
  //DLL_API  ApiResult ImportFbxAnimation(const char* file_name, Animation*& mesh);

}

