// ImporterTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Importer.h>
#include "static_model_generated.h"
#include <fstream>

int main()
{
    //Importer::SkinnedMesh* m;
    //Importer::ImportFbxSkinnedMesh("Guivre.fbx", m);
    //std::unique_ptr<Importer::SkinnedMesh> p(m);

    Importer::StaticModel* m;
    Importer::ImportFbxStaticModel("Castle-new.fbx", m);
    std::unique_ptr<Importer::StaticModel> p(m);

    {
        using namespace flatbuffers;
        using namespace QLibrary::Geometry;

        FlatBufferBuilder builder;

        std::vector<Offset<Material>>   materials;
        std::vector<Offset<StaticMesh>> meshes;

        for (auto& mtl : m->m_material)
        {
            Offset<String>   d = builder.CreateString(mtl->m_diffuse);
            Offset<String>   n = builder.CreateString(mtl->m_normal);
            Offset<String>   s = builder.CreateString(mtl->m_specular);
            Offset<String>   g = builder.CreateString(mtl->m_glossiness);
            Offset<Material> r = CreateMaterial(builder, d, n, s, g);
            materials.push_back(r);
        }

        for (auto& mtl : m->m_mesh)
        {
            const auto triangles = 
                builder.CreateVectorOfStructs<Triangle32Bit>(reinterpret_cast<const Triangle32Bit*>(&mtl->m_triangles[0]), mtl->m_triangles.size());

            const auto positions =
                builder.CreateVectorOfStructs<Point3>(reinterpret_cast<const Point3*>(&mtl->m_positions[0]), mtl->m_positions.size());

            const auto uv =
                builder.CreateVectorOfStructs<Point2>(reinterpret_cast<const Point2*>(&mtl->m_uv[0]), mtl->m_uv.size());

            const auto normals =
                builder.CreateVectorOfStructs<Vector3>(reinterpret_cast<const Vector3*>(&mtl->m_normals[0]), mtl->m_normals.size());

            const auto tangents =
                builder.CreateVectorOfStructs<Vector3>(reinterpret_cast<const Vector3*>(&mtl->m_tangents[0]), mtl->m_tangents.size());

            const auto bitangents =
                builder.CreateVectorOfStructs<Vector3>(reinterpret_cast<const Vector3*>(&mtl->m_bitangents[0]), mtl->m_bitangents.size());

            const auto mesh = CreateStaticMesh(builder, triangles, positions, normals, tangents, bitangents, uv);
            
            meshes.push_back(mesh);
        }

        const auto tbl0      =
            builder.CreateVectorOfStructs<MaterialMeshTable>(reinterpret_cast<const MaterialMeshTable*>(&m->m_material_mesh_table[0]), m->m_material_mesh_table.size());

        const auto tbl1 =
            builder.CreateVectorOfStructs<MeshMaterialTable>(reinterpret_cast<const MeshMaterialTable*>(&m->m_mesh_material_table[0]), m->m_mesh_material_table.size());

        const auto  bounds =
            builder.CreateVectorOfStructs<StaticMeshBounds>(reinterpret_cast<const StaticMeshBounds*>(&m->m_mesh_bounds[0]), m->m_mesh_bounds.size());

        const auto mtl      = builder.CreateVector(materials);
        const auto msh      = builder.CreateVector(meshes);
        const auto model    = CreateStaticModel(builder, msh, mtl, tbl0, tbl1, bounds);
        builder.Finish(model, "QLBL");

        std::ofstream qlbl;
        qlbl.open("Castle.qlbl", std::ios::binary | std::ios::out);

        if (qlbl.good())
        {
            uint8_t* bp = builder.GetBufferPointer();
            uoffset_t bs = builder.GetSize();
            qlbl.write(reinterpret_cast<const char*>(bp), bs);
        }
    }

    {
        /*
        //Interleave
        {
            size_t s = m->m_positions.size();
            std::vector<QLibrary::Geometry::SkinnedVertex> v(s);

            for (auto i = 0; i < s; ++i)
            {
                Importer::Point3   positions        = m->m_positions[i];
                Importer::Vector3  normals          = m->m_normals[i];
                Importer::Vector3  tangents         = m->m_tangents[i];
                Importer::Weight4  blend_weights    = m->m_blend_weights[i];
                Importer::Byte4    blend_indices    = m->m_blend_indices[i];
                Importer::UV       uv               = m->m_uv[i];

                QLibrary::Geometry::Point3      p(positions.m_x, positions.m_y, positions.m_z);
                QLibrary::Geometry::UV          u(uv.m_u, uv.m_v);
                QLibrary::Geometry::Vector3     n(normals.m_x, normals.m_y, normals.m_z);
                QLibrary::Geometry::Vector3     t(tangents.m_x, tangents.m_y, tangents.m_z);
                QLibrary::Geometry::Weight4     bw(blend_weights.m_x, blend_weights.m_y, blend_weights.m_z, blend_weights.m_w);
                QLibrary::Geometry::Byte4       bi(blend_indices.m_x, blend_indices.m_y, blend_indices.m_z, blend_indices.m_w);
                
                v[i] = QLibrary::Geometry::SkinnedVertex(p, u, n, t, bw, bi);
            }

            using namespace flatbuffers;
            using namespace QLibrary::Geometry;

            FlatBufferBuilder builder;
            auto vertices = builder.CreateVectorOfStructs<QLibrary::Geometry::SkinnedVertex>(&v[0], v.size());
            auto indices  = builder.CreateVectorOfStructs<QLibrary::Geometry::Triangle>(reinterpret_cast<QLibrary::Geometry::Triangle *>(&m->m_triangles[0]), m->m_triangles.size());
            auto m1       = CreateSkinnedMesh(builder, vertices, indices);

            builder.Finish(m1, "QLBL");

            uint8_t*  bp = builder.GetBufferPointer();
            uoffset_t bs = builder.GetSize();

            {
                auto mesh               = GetSkinnedMesh(bp);
                const auto* pos         = reinterpret_cast<const QLibrary::Geometry::SkinnedVertex*>  (mesh->vertices()->data());
                const auto* triangles   = reinterpret_cast<const QLibrary::Geometry::Triangle*>  (mesh->triangles()->data());
            }

            std::ofstream qlbl;
            
            qlbl.open("Guivre.qlbl", std::ios::binary | std::ios::out);

            if (qlbl.good())
            {
                qlbl.write(reinterpret_cast<const char*>(bp), bs);
            }
        }
        */
    }

    std::cout << "Hello World!\n";
}

