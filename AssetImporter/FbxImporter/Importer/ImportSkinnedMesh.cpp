// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <Importer.h>
#include <algorithm>
#include <map>

#include <fbxsdk.h>
#include "fbx_common.h"

namespace Importer
{
    namespace
    {
        inline bool has_normals(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementNormal(0) != nullptr;
        }

        inline bool has_tangents(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementTangent(0) != nullptr;
        }

        inline bool has_bitangents(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementBinormal(0) != nullptr;
        }

        inline bool has_uv(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementUV(0) != nullptr;
        }

        inline bool is_skinned_mesh(const fbxsdk::FbxMesh* m)
        {
            return m->GetDeformerCount(fbxsdk::FbxDeformer::eSkin) > 0;
        }

        inline bool has_triangles(const fbxsdk::FbxMesh* m)
        {
            return m->GetPolygonSize(0) && m->IsTriangleMesh();
        }

        //returns which materials, which polygons affect
        struct polygon_index
        {
            uint32_t m_v;
        };

        polygon_index index(int32_t v)
        {
            polygon_index r;
            r.m_v = v;
            return r;
        }

        std::vector< std::vector<polygon_index> > get_material_indices(const fbxsdk::FbxMesh* mesh)
        {
            auto material_count = mesh->GetElementMaterialCount();

            std::vector< std::vector<polygon_index> > materials_indices;

            materials_indices.resize(material_count);

            //assign polygons to materials
            for (auto i = 0; i < material_count; ++i)
            {
                auto&& element_material = mesh->GetElementMaterial(i);
                auto&& material_indices = element_material->GetIndexArray();
                auto count = material_indices.GetCount();

                if (element_material->GetMappingMode() == fbxsdk::FbxLayerElement::eAllSame)
                {
                    auto polygon_count              = mesh->GetPolygonCount();
                    auto material_index_for_polygon = material_indices.GetAt(0);

                    materials_indices[material_index_for_polygon].reserve(polygon_count);

                    for (auto k = 0; k < polygon_count; ++k)
                    {
                        materials_indices[material_index_for_polygon].push_back(index ( k ) );
                    }
                }
                else
                {
                    for (auto j = 0; j < count; ++j)
                    {
                        auto material_index_for_polygon = material_indices.GetAt(j);
                        materials_indices[material_index_for_polygon].push_back(index(j));
                    }
                }
            }

            return materials_indices;
        }

        //returns all positions of an fbx sdk mesh
        std::vector<Point3>   get_positions(const fbxsdk::FbxMesh* mesh)
        {
            const auto& points         = mesh->GetControlPoints();
            const auto& indices        = mesh->GetPolygonVertices();

            const auto& triangle_count = mesh->GetPolygonCount();

            std::vector<Point3> positions;
            auto node_transform         = fbx::world_transform(mesh->GetNode());

            for (auto triangle = 0; triangle < triangle_count; ++triangle)
            {
                using namespace DirectX;

                auto i0 = indices[triangle * 3];
                auto i1 = indices[triangle * 3 + 1];
                auto i2 = indices[triangle * 3 + 2];

                //positions
                {
                    const double* v0 = points[i0];
                    const double* v1 = points[i1];
                    const double* v2 = points[i2];

                    const XMVECTOR  vr0 = XMVectorSet(static_cast<float>(v0[0]), static_cast<float>(v0[1]), static_cast<float>(v0[2]), 1.0f);
                    const XMVECTOR  vr1 = XMVectorSet(static_cast<float>(v1[0]), static_cast<float>(v1[1]), static_cast<float>(v1[2]), 1.0f);
                    const XMVECTOR  vr2 = XMVectorSet(static_cast<float>(v2[0]), static_cast<float>(v2[1]), static_cast<float>(v2[2]), 1.0f);

                    const XMVECTOR  vq0 = XMVector3Transform(vr0, node_transform);
                    const XMVECTOR  vq1 = XMVector3Transform(vr1, node_transform);
                    const XMVECTOR  vq2 = XMVector3Transform(vr2, node_transform);

                    Point3 vp0;
                    Point3 vp1;
                    Point3 vp2;

                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp0), vq0);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp1), vq1);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp2), vq2);

                    positions.push_back(vp0);
                    positions.push_back(vp1);
                    positions.push_back(vp2);
                }
            }

            return positions;
        }

        struct get_vector4_element
        {
            virtual fbxsdk::FbxVector4 get_element(uint32_t index) const = 0;
        };

        struct get_normal_element_direct : public get_vector4_element
        {
            const fbxsdk::FbxGeometryElementNormal* m_normal;

        public:

            get_normal_element_direct(const FbxGeometryElementNormal* normal) : m_normal(normal)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                return m_normal->GetDirectArray().GetAt(control_point_index);
            }
        };

        struct get_normal_element_index_to_direct : public get_vector4_element
        {
            const fbxsdk::FbxGeometryElementNormal* m_normal;

        public:

            get_normal_element_index_to_direct(const FbxGeometryElementNormal* normal) : m_normal(normal)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                auto id = m_normal->GetIndexArray().GetAt(control_point_index);
                return m_normal->GetDirectArray().GetAt(id);
            }
        };

        struct get_basic_point_index
        {
            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const = 0;
        };


        template <typename fbx_geometry_element_t>
        struct get_typed_element_direct : public get_vector4_element
        {
            const fbx_geometry_element_t* m_element;

        public:

            get_typed_element_direct(const fbx_geometry_element_t* element) : m_element(element)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                return m_element->GetDirectArray().GetAt(control_point_index);
            }
        };

        template <typename fbx_geometry_element_t>
        struct get_typed_element_index_to_direct : public get_vector4_element
        {
            const fbx_geometry_element_t* m_element;

        public:

            get_typed_element_index_to_direct(const fbx_geometry_element_t* element) : m_element(element)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                auto id = m_element->GetIndexArray().GetAt(control_point_index);
                return m_element->GetDirectArray().GetAt(id);
            }
        };

        struct get_control_point_index : public get_basic_point_index
        {
            int* m_control_points;

        public:
            get_control_point_index(int* control_points) : m_control_points(control_points)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                return m_control_points[triangle_index * 3 + triangle_vertex];
            }
        };

        struct get_point_index : public get_basic_point_index
        {
            const fbxsdk::FbxMesh* m_mesh;
        public:

            get_point_index(const fbxsdk::FbxMesh* m) : m_mesh(m)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                return 3 * triangle_index + triangle_vertex;
            }
        };


        struct get_uv_element
        {
            virtual fbxsdk::FbxVector2 get_element(uint32_t index) const = 0;
        };

        struct get_uv_element_direct : public get_uv_element
        {
            const fbxsdk::FbxGeometryElementUV* m_uv;

        public:

            get_uv_element_direct(const FbxGeometryElementUV* uv) : m_uv(uv)
            {

            }

            virtual fbxsdk::FbxVector2 get_element(uint32_t control_point_index) const override
            {
                return m_uv->GetDirectArray().GetAt(control_point_index);
            }
        };

        struct get_uv_element_index_to_direct : public get_uv_element
        {
            const fbxsdk::FbxGeometryElementUV* m_uv;

        public:

            get_uv_element_index_to_direct(const FbxGeometryElementUV* uv) : m_uv(uv)
            {

            }

            virtual fbxsdk::FbxVector2 get_element(uint32_t control_point_index) const override
            {
                auto id = m_uv->GetIndexArray().GetAt(control_point_index);
                return m_uv->GetDirectArray().GetAt(id);
            }
        };

        struct get_uv_control_point_index : public get_basic_point_index
        {
            int* m_control_points;

        public:
            get_uv_control_point_index(int* control_points) : m_control_points(control_points)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                assert(triangle_vertex == 0 || triangle_vertex == 1 || triangle_vertex == 2);
                return m_control_points[triangle_index * 3 + triangle_vertex];
            }
        };

        struct get_uv_texture_index : public get_basic_point_index
        {
            const fbxsdk::FbxMesh* m_mesh;
        public:

            get_uv_texture_index(const fbxsdk::FbxMesh* m) : m_mesh(m)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                auto m = const_cast<fbxsdk::FbxMesh*>(m_mesh);
                return m->GetTextureUVIndex(triangle_index, triangle_vertex);
            }
        };


        std::vector<UV> get_uvs(const fbxsdk::FbxMesh* mesh)
        {
            auto indices = mesh->GetPolygonVertices();
            auto triangle_count = mesh->GetPolygonCount();
            auto uv = mesh->GetElementUV(0);

            std::vector<UV>             uvs;

            get_uv_element* get_uv          = nullptr;
            get_uv_element_direct           get_uv_0(uv);
            get_uv_element_index_to_direct  get_uv_1(uv);

            if (uv->GetReferenceMode() == fbxsdk::FbxGeometryElement::eDirect)
            {
                get_uv = &get_uv_0;
            }
            else
            {
                assert(uv->GetReferenceMode() == fbxsdk::FbxGeometryElement::eIndexToDirect);
                get_uv = &get_uv_1;
            }

            get_basic_point_index* gpi = nullptr;
            get_uv_texture_index        get_texture0(mesh);
            get_uv_control_point_index  get_texture1(indices);

            if (uv->GetMappingMode() == fbxsdk::FbxGeometryElement::eByControlPoint)
            {
                gpi = &get_texture1;
            }
            else
            {
                assert(uv->GetMappingMode() == fbxsdk::FbxGeometryElement::eByPolygonVertex);
                gpi = &get_texture0;
                get_uv = &get_uv_0; //todo; check this, produces different uvs? the file data indicates otherwise
            }

            for (auto triangle = 0; triangle < triangle_count; ++triangle)
            {
                //uv
                {
                    auto uvi0 = gpi->get_element(triangle, 0);
                    auto uvi1 = gpi->get_element(triangle, 1);
                    auto uvi2 = gpi->get_element(triangle, 2);

                    double* uv0 = get_uv->get_element(uvi0);
                    double* uv1 = get_uv->get_element(uvi1);
                    double* uv2 = get_uv->get_element(uvi2);

                    UV uvp0 = { static_cast<float>(uv0[0]), static_cast<float>(uv0[1]) };
                    UV uvp1 = { static_cast<float>(uv1[0]), static_cast<float>(uv1[1]) };
                    UV uvp2 = { static_cast<float>(uv2[0]), static_cast<float>(uv2[1]) };

                    //transform to directx
                    uvp0.m_v = 1.0f - uvp0.m_v;
                    uvp1.m_v = 1.0f - uvp1.m_v;
                    uvp2.m_v = 1.0f - uvp2.m_v;

                    uvs.push_back(uvp0);
                    uvs.push_back(uvp1);
                    uvs.push_back(uvp2);
                }
            }

            return uvs;
        }

        template <typename triangle_indices_functor_t, typename triangle_count_functor_t, typename vector_element_type_t, typename return_vectors_type_t>
        void get_vectors_typed(const fbxsdk::FbxMesh* mesh, triangle_indices_functor_t triangle_indices, triangle_count_functor_t triangle_count_functor, const vector_element_type_t* vector_element, return_vectors_type_t& vectors)
        {
            auto indices = mesh->GetPolygonVertices();
            auto triangle_count = triangle_count_functor();
            auto element = vector_element;

            get_vector4_element* gv = nullptr;
            get_typed_element_direct<vector_element_type_t>             gv_0(element);
            get_typed_element_index_to_direct<vector_element_type_t>    gv_1(element);

            if (element->GetReferenceMode() == fbxsdk::FbxGeometryElement::eDirect)
            {
                gv = &gv_0;
            }
            else
            {
                assert(element->GetReferenceMode() == fbxsdk::FbxGeometryElement::eIndexToDirect);
                gv = &gv_1;
            }

            get_basic_point_index* gpi              = nullptr;
            get_point_index                         get_texture0(mesh);
            get_control_point_index                 get_texture1(indices);

            if (element->GetMappingMode() == fbxsdk::FbxGeometryElement::eByControlPoint)
            {
                gpi = &get_texture1;
            }
            else
            {
                assert(element->GetMappingMode() == fbxsdk::FbxGeometryElement::eByPolygonVertex);
                gpi = &get_texture0;
                //gv = &gv_0; //todo; check this, produces different normals? the file data indicates otherwise
            }

            for (auto triangle = 0; triangle < triangle_count; ++triangle)
            {
                using namespace DirectX;
                auto triangle_to_fetch = triangle_indices(triangle);
                //vector
                {
                    auto normali0 = gpi->get_element(triangle_to_fetch, 0);
                    auto normali1 = gpi->get_element(triangle_to_fetch, 1);
                    auto normali2 = gpi->get_element(triangle_to_fetch, 2);

                    double* normal0 = gv->get_element(normali0);
                    double* normal1 = gv->get_element(normali1);
                    double* normal2 = gv->get_element(normali2);

                    using vector_type_t = typename return_vectors_type_t::value_type;

                    vector_type_t normalp0;
                    vector_type_t normalp1;
                    vector_type_t normalp2;

                    XMVECTOR  vr0 = XMVectorSet(static_cast<float>(normal0[0]), static_cast<float>(normal0[1]), static_cast<float>(normal0[2]), 0.0f);
                    XMVECTOR  vr1 = XMVectorSet(static_cast<float>(normal1[0]), static_cast<float>(normal1[1]), static_cast<float>(normal1[2]), 0.0f);
                    XMVECTOR  vr2 = XMVectorSet(static_cast<float>(normal2[0]), static_cast<float>(normal2[1]), static_cast<float>(normal2[2]), 0.0f);

                    XMVECTOR  vq0 = vr0;
                    XMVECTOR  vq1 = vr1;
                    XMVECTOR  vq2 = vr2;

                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&normalp0), vq0);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&normalp1), vq1);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&normalp2), vq2);

                    vectors.push_back(normalp0);
                    vectors.push_back(normalp1);
                    vectors.push_back(normalp2);
                }
            }
        }

        std::vector<Vector3> transform_normals(const DirectX::XMMATRIX m, const std::vector<Vector3>& s)
        {
            using namespace DirectX;

            std::vector<Vector3> r;
            r.resize(s.size());
            std::transform(s.cbegin(), s.cend(), r.begin(),
                [&m](const Vector3 n)
                {
                    XMVECTOR   r0 = XMLoadFloat3(reinterpret_cast<const XMFLOAT3*>(&n));
                    XMVECTOR   r1 = XMVector3Normalize(XMVector3Transform(r0, m));

                    Vector3 r;
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&r), r1);
                    return r;
                });

            return r;
        }


        std::vector<Vector3> get_normals(const fbxsdk::FbxMesh* mesh)
        {
            using namespace DirectX;
            using namespace fbx;

            std::vector<Vector3>  vectors;
            const auto f0 = [](auto triangle_index) {return triangle_index; };
            const auto f1 = [&mesh]() { return mesh->GetPolygonCount(); };

            get_vectors_typed(mesh, f0, f1, mesh->GetElementNormal(0), vectors);

            const auto normal_node_transform = XMMatrixTranspose(XMMatrixInverse(nullptr, world_transform(mesh->GetNode())));
            return transform_normals(normal_node_transform, vectors);
        }

        std::vector<Vector3> get_tangents(const fbxsdk::FbxMesh* mesh)
        {
            using namespace DirectX;
            using namespace fbx;

            std::vector<Vector3>  vectors;
            const auto f0 = [](auto triangle_index) {return triangle_index; };
            const auto f1 = [&mesh]() { return mesh->GetPolygonCount(); };

            get_vectors_typed(mesh, f0, f1, mesh->GetElementTangent(0), vectors);

            const auto normal_node_transform = XMMatrixTranspose(XMMatrixInverse(nullptr, world_transform(mesh->GetNode())));
            return transform_normals(normal_node_transform, vectors);
        }

        inline std::vector<fbxsdk::FbxNode*> get_skeleton_nodes(const fbxsdk::FbxNode* n)
        {
            std::vector<fbxsdk::FbxNode*> r;

            fbx::transform_node_recursive(n, [&r](const fbxsdk::FbxNode* n)
                {
                    if (n->GetNodeAttribute() && n->GetNodeAttribute()->GetAttributeType() == fbxsdk::FbxNodeAttribute::eSkeleton)
                    {
                        r.push_back(const_cast<fbxsdk::FbxNode*>(n));
                    }
                });

            return r;
        }


        std::map<fbxsdk::FbxNode*, uint16_t> get_joint_indices(const fbxsdk::FbxNode* root)
        {
            auto skeletal_nodes = get_skeleton_nodes(root);

            std::map<fbxsdk::FbxNode*, uint16_t> joint2index;

            {
                for (auto&& i = 0U; i < skeletal_nodes.size(); ++i)
                {
                    auto n = skeletal_nodes[i];
                    joint2index.insert(std::make_pair(n, static_cast<uint16_t>(i)));
                }
            }
            return joint2index;
        }

        Weight4 get_blend_weight(const std::vector<float>& v)
        {
            Weight4 r = {};
            float* as_float = &r.m_x;
            auto j = 0U;

            for (auto&& i : v)
            {
                if (j >= 4)
                {
                    break;
                }
                as_float[j++] = i;
            }

            return r;
        }


        //////////////////////
        std::vector<Weight4>    get_blend_weights(const fbxsdk::FbxMesh* mesh, const std::vector<int32_t>& triangle_indices)
        {
            struct joint_influence
            {
                std::vector<float>    m_weight;
                std::vector<uint32_t> m_index;
            };

            std::vector<joint_influence> influences;
            influences.resize(mesh->GetControlPointsCount());

            auto joint_indices = get_joint_indices(mesh->GetScene()->GetRootNode());

            int skinCount = mesh->GetDeformerCount(fbxsdk::FbxDeformer::eSkin);
            for (int skinIndex = 0; skinIndex < skinCount; skinIndex++)
            {
                FbxSkin* skin = (FbxSkin*)mesh->GetDeformer(skinIndex, fbxsdk::FbxDeformer::eSkin);

                int jointsCount = skin->GetClusterCount();
                for (int jointIndex = 0; jointIndex < jointsCount; jointIndex++)
                {
                    fbxsdk::FbxCluster* joint = skin->GetCluster(jointIndex);

                    int influencedCount = joint->GetControlPointIndicesCount();

                    int* influenceIndices = joint->GetControlPointIndices();
                    double* influenceWeights = joint->GetControlPointWeights();

                    for (int influenceIndex = 0; influenceIndex < influencedCount; influenceIndex++)
                    {
                        int controlPointIndex = influenceIndices[influenceIndex];
                        assert(controlPointIndex < (int)influences.size());//"Invalid skin control point index"
                        influences[controlPointIndex].m_index.push_back(joint_indices.find(joint->GetLink())->second);
                        influences[controlPointIndex].m_weight.push_back((float)influenceWeights[influenceIndex]);
                    }
                }
            }

            auto indices = mesh->GetPolygonVertices();
            std::vector<Weight4> blend_weights;
            for (auto triangle = 0; triangle < triangle_indices.size(); ++triangle)
            {
                auto triange_to_fetch = triangle_indices[triangle];
                auto i0 = indices[triange_to_fetch * 3];
                auto i1 = indices[triange_to_fetch * 3 + 1];
                auto i2 = indices[triange_to_fetch * 3 + 2];

                auto w0 = influences[i0];
                auto w1 = influences[i1];
                auto w2 = influences[i2];

                auto wp0 = get_blend_weight(w0.m_weight);
                auto wp1 = get_blend_weight(w1.m_weight);
                auto wp2 = get_blend_weight(w2.m_weight);

                blend_weights.push_back(wp0);
                blend_weights.push_back(wp1);
                blend_weights.push_back(wp2);
            }

            return blend_weights;
        }

        inline Byte4 get_blend_index(const std::vector<uint32_t>& v)
        {
            Byte4 r = {};
            uint8_t* as_float = &r.m_x;
            auto j = 0U;

            for (auto&& i : v)
            {
                if (j >= 4)
                {
                    break;
                }
                as_float[j++] = static_cast<uint8_t>(i);
            }

            return r;
        }

        std::vector<int32_t> make_triangle_indices(const fbxsdk::FbxMesh* m)
        {
            std::vector<int32_t> r;

            for (auto i = 0; i < m->GetPolygonCount(); ++i)
            {
                r.push_back(i);
            }
            return r;
        }

        std::vector<Byte4> get_blend_indices(const fbxsdk::FbxMesh* mesh, const std::vector<int32_t>& triangle_indices)
        {
            struct joint_influence
            {
                std::vector<float>    m_weight;
                std::vector<uint32_t> m_index;
            };

            std::vector<joint_influence> influences;
            influences.resize(mesh->GetControlPointsCount());

            auto joint_indices = get_joint_indices(mesh->GetScene()->GetRootNode());

            int skinCount = mesh->GetDeformerCount(fbxsdk::FbxDeformer::eSkin);
            for (int skinIndex = 0; skinIndex < skinCount; skinIndex++)
            {
                FbxSkin* skin = (FbxSkin*)mesh->GetDeformer(skinIndex, fbxsdk::FbxDeformer::eSkin);
                int jointsCount = skin->GetClusterCount();
                for (int jointIndex = 0; jointIndex < jointsCount; jointIndex++)
                {
                    fbxsdk::FbxCluster* joint = skin->GetCluster(jointIndex);

                    int influencedCount = joint->GetControlPointIndicesCount();

                    int* influenceIndices = joint->GetControlPointIndices();
                    double* influenceWeights = joint->GetControlPointWeights();

                    for (int influenceIndex = 0; influenceIndex < influencedCount; influenceIndex++)
                    {
                        int controlPointIndex = influenceIndices[influenceIndex];
                        assert(controlPointIndex < (int)influences.size());//"Invalid skin control point index"
                        influences[controlPointIndex].m_index.push_back(joint_indices.find(joint->GetLink())->second);
                        influences[controlPointIndex].m_weight.push_back((float)influenceWeights[influenceIndex]);
                    }
                }
            }

            auto indices = mesh->GetPolygonVertices();
            std::vector<Byte4> blend_indices;
            for (auto triangle = 0; triangle < triangle_indices.size(); ++triangle)
            {
                auto triange_to_fetch = triangle_indices[triangle];
                auto i0 = indices[triange_to_fetch * 3];
                auto i1 = indices[triange_to_fetch * 3 + 1];
                auto i2 = indices[triange_to_fetch * 3 + 2];

                auto w0 = influences[i0];
                auto w1 = influences[i1];
                auto w2 = influences[i2];

                auto ip0 = get_blend_index(w0.m_index);
                auto ip1 = get_blend_index(w1.m_index);
                auto ip2 = get_blend_index(w2.m_index);

                blend_indices.push_back(ip0);
                blend_indices.push_back(ip1);
                blend_indices.push_back(ip2);
            }

            return blend_indices;
        }

        std::vector<Triangle32Bit> get_triangles(const fbxsdk::FbxMesh* mesh, const std::vector<int32_t>& triangle_indices)
        {
            std::vector<Triangle32Bit> r;
            auto indices = mesh->GetPolygonVertices();
            std::vector<Byte4> blend_indices;
            for (auto triangle = 0; triangle < triangle_indices.size(); ++triangle)
            {
                auto triange_to_fetch = triangle_indices[triangle];
                uint32_t i0 = indices[triange_to_fetch * 3];
                uint32_t i1 = indices[triange_to_fetch * 3 + 1];
                uint32_t i2 = indices[triange_to_fetch * 3 + 2];

                r.push_back({ i0,i1,i2 });
            }

            return r;
        }


        void create_skinned_mesh(const fbxsdk::FbxMesh* m, SkinnedMesh* mesh, const fbx::fbx_context* context)
        {
            mesh->m_positions       = get_positions(m);
            mesh->m_uv              = get_uvs(m);
            mesh->m_normals         = get_normals(m);
            mesh->m_tangents        = get_tangents(m);
            mesh->m_blend_weights   = get_blend_weights(m, make_triangle_indices(m));
            mesh->m_blend_indices   = get_blend_indices(m, make_triangle_indices(m));
            mesh->m_triangles       = get_triangles(m, make_triangle_indices(m) );
        }
    }

    DLL_API  ApiResult ImportFbxSkinnedMesh(const char* file_name, SkinnedMesh*& mesh)
    {
        std::unique_ptr<SkinnedMesh> m = std::make_unique<SkinnedMesh>();
        mesh = m.release();

        using namespace fbx;

        auto context = load_fbx_file(file_name);
        auto scene = context->m_scene.get();

        std::vector<fbxsdk::FbxMesh*> meshes;
        meshes = get_meshes(scene->GetRootNode(), meshes);

        for (auto& m : meshes)
        {
            m->RemoveBadPolygons();
            m->ComputeBBox();

            if (!has_normals(m))
            {
                m->GenerateNormals();
            }

            if (!has_tangents(m))
            {
                m->GenerateTangentsDataForAllUVSets(true);
            }
        }

        for (auto& m : meshes)
        {
            //skip meshes without skin and import only the first one
            if (is_skinned_mesh(m) && has_triangles(m) )
            {
                create_skinned_mesh(m, mesh, context.get());
                break;
            }
        }

        return ApiResult::Success;
    }
}




