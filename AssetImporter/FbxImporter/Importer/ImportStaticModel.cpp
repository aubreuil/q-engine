// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <Importer.h>
#include <algorithm>
#include <map>
#include <set>
#include <filesystem>

#include <fbxsdk.h>
#include "fbx_common.h"
#include "fbx_transform_helper.h"

namespace Importer
{
    DLL_API  ApiResult ImportFbxMesh(const char* file_name, Mesh*& mesh)
    {
        std::unique_ptr<Mesh> m = std::make_unique<Mesh>();
        mesh = m.release();
        return ApiResult::Success;
    }

    namespace
    {
		static inline uint32_t murmur_32_scramble(uint32_t k) {
			k *= 0xcc9e2d51;
			k = (k << 15) | (k >> 17);
			k *= 0x1b873593;
			return k;
		}

		uint32_t murmur3_32(const uint8_t* key, size_t len, uint32_t seed)
		{
			uint32_t h = seed;
			uint32_t k;
			/* Read in groups of 4. */
			for (size_t i = len >> 2; i; i--) {
				// Here is a source of differing results across endiannesses.
				// A swap here has no effects on hash properties though.
				memcpy(&k, key, sizeof(uint32_t));
				key += sizeof(uint32_t);
				h ^= murmur_32_scramble(k);
				h = (h << 13) | (h >> 19);
				h = h * 5 + 0xe6546b64;
			}
			/* Read the rest. */
			k = 0;
			for (size_t i = len & 3; i; i--) {
				k <<= 8;
				k |= key[i - 1];
			}
			// A swap is *not* necessary here because the preceding loop already
			// places the low bytes in the low places according to whatever endianness
			// we use. Swaps only apply when the memory is copied in a chunk.
			h ^= murmur_32_scramble(k);
			/* Finalize. */
			h ^= len;
			h ^= h >> 16;
			h *= 0x85ebca6b;
			h ^= h >> 13;
			h *= 0xc2b2ae35;
			h ^= h >> 16;
			return h;
		}


        inline bool has_normals(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementNormal(0) != nullptr;
        }

        inline bool has_tangents(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementTangent(0) != nullptr;
        }

        inline bool has_bitangents(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementBinormal(0) != nullptr;
        }

        inline bool has_uv(const fbxsdk::FbxMesh* m)
        {
            return  m->GetElementUV(0) != nullptr;
        }

        inline bool has_triangles(const fbxsdk::FbxMesh* m)
        {
            return m->GetPolygonSize(0) && m->IsTriangleMesh();
        }

        //returns which materials, which polygons affect
        struct polygon_index
        {
            uint32_t m_v;
        };

        polygon_index index(int32_t v)
        {
            polygon_index r;
            r.m_v = v;
            return r;
        }

        std::vector< std::vector<polygon_index> > get_material_indices(const fbxsdk::FbxMesh* mesh)
        {
            auto material_count = mesh->GetElementMaterialCount();

            std::vector< std::vector<polygon_index> > materials_indices;

            materials_indices.resize(material_count);

            //assign polygons to materials
            for (auto i = 0; i < material_count; ++i)
            {
                auto&& element_material = mesh->GetElementMaterial(i);
                auto&& material_indices = element_material->GetIndexArray();
                auto count = material_indices.GetCount();

                if (element_material->GetMappingMode() == fbxsdk::FbxLayerElement::eAllSame)
                {
                    auto polygon_count              = mesh->GetPolygonCount();
                    auto material_index_for_polygon = material_indices.GetAt(0);

                    materials_indices[material_index_for_polygon].reserve(polygon_count);

                    for (auto k = 0; k < polygon_count; ++k)
                    {
                        materials_indices[material_index_for_polygon].push_back(index ( k ) );
                    }
                }
                else
                {
                    for (auto j = 0; j < count; ++j)
                    {
                        auto material_index_for_polygon = material_indices.GetAt(j);
                        materials_indices[material_index_for_polygon].push_back(index(j));
                    }
                }
            }

            return materials_indices;
        }

        //returns all positions of an fbx sdk mesh
        std::vector<Point3>   get_positions(const fbxsdk::FbxMesh* mesh)
        {
            const auto& points         = mesh->GetControlPoints();
            const auto& indices        = mesh->GetPolygonVertices();
            const auto& triangle_count = mesh->GetPolygonCount();

            std::vector<Point3> positions;
            auto node_transform = XMMatrixTranspose(fbx::world_transform(mesh->GetNode()));

            for (auto triangle = 0; triangle < triangle_count; ++triangle)
            {
                using namespace DirectX;

                auto i0 = indices[triangle * 3];
                auto i1 = indices[triangle * 3 + 1];
                auto i2 = indices[triangle * 3 + 2];

                //positions
                {
                    const double* v0    = points[i0];
                    const double* v1    = points[i1];
                    const double* v2    = points[i2];

                    const XMVECTOR  vr0 = XMVectorSet(static_cast<float>(v0[0]), static_cast<float>(v0[1]), static_cast<float>(v0[2]), 1.0f);
                    const XMVECTOR  vr1 = XMVectorSet(static_cast<float>(v1[0]), static_cast<float>(v1[1]), static_cast<float>(v1[2]), 1.0f);
                    const XMVECTOR  vr2 = XMVectorSet(static_cast<float>(v2[0]), static_cast<float>(v2[1]), static_cast<float>(v2[2]), 1.0f);

                    const XMVECTOR  vq0 = fbx::transform_vector_from_dcc(XMVector4Transform(vr0, node_transform));
                    const XMVECTOR  vq1 = fbx::transform_vector_from_dcc(XMVector4Transform(vr1, node_transform));
                    const XMVECTOR  vq2 = fbx::transform_vector_from_dcc(XMVector4Transform(vr2, node_transform));
                    
                    Point3 vp0;
                    Point3 vp1;
                    Point3 vp2;

                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp0), vq0);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp1), vq1);
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp2), vq2);

                    positions.push_back(vp0);
                    positions.push_back(vp1);
                    positions.push_back(vp2);
                }
            }

            return positions;
            
            /*
            const auto& points          = mesh->GetControlPoints();
            const int32_t points_count  = mesh->GetControlPointsCount();

            std::vector<Point3> positions;
            auto node_transform         = XMMatrixTranspose(fbx::world_transform(mesh->GetNode()));

            for (auto point = 0; point < points_count; ++point)
            {
                using namespace DirectX;

                FbxVector4 p            = points[point];
                const XMVECTOR  vr0     = XMVectorSet(static_cast<float>(p[0]), static_cast<float>(p[1]), static_cast<float>(p[2]), 1.0f);
                const XMVECTOR  vq0     = fbx::transform_vector_from_dcc(XMVector4Transform(vr0, node_transform));

                Point3 vp0;
                XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&vp0), vq0);
                positions.push_back(vp0);
            }

            return positions;
            */
        }

        struct get_vector4_element
        {
            virtual fbxsdk::FbxVector4 get_element(uint32_t index) const = 0;
            virtual int32_t get_size() const = 0;
        };

        struct get_normal_element_direct : public get_vector4_element
        {
            const fbxsdk::FbxGeometryElementNormal* m_normal;

        public:

            get_normal_element_direct(const FbxGeometryElementNormal* normal) : m_normal(normal)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                return m_normal->GetDirectArray().GetAt(control_point_index);
            }

            virtual int32_t get_size() const override
            {
                return m_normal->GetDirectArray().GetCount();
            }
        };

        struct get_normal_element_index_to_direct : public get_vector4_element
        {
            const fbxsdk::FbxGeometryElementNormal* m_normal;

        public:

            get_normal_element_index_to_direct(const FbxGeometryElementNormal* normal) : m_normal(normal)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                auto id = m_normal->GetIndexArray().GetAt(control_point_index);
                return m_normal->GetDirectArray().GetAt(id);
            }

			virtual int32_t get_size() const override
			{
                return m_normal->GetIndexArray().GetCount();
			}
        };

        struct get_basic_point_index
        {
            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const = 0;
        };


        template <typename fbx_geometry_element_t>
        struct get_typed_element_direct : public get_vector4_element
        {
            const fbx_geometry_element_t* m_element;

        public:

            get_typed_element_direct(const fbx_geometry_element_t* element) : m_element(element)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                return m_element->GetDirectArray().GetAt(control_point_index);
            }

			virtual int32_t get_size() const override
			{
				return m_element->GetDirectArray().GetCount();
			}
        };

        template <typename fbx_geometry_element_t>
        struct get_typed_element_index_to_direct : public get_vector4_element
        {
            const fbx_geometry_element_t* m_element;

        public:

            get_typed_element_index_to_direct(const fbx_geometry_element_t* element) : m_element(element)
            {

            }

            virtual fbxsdk::FbxVector4 get_element(uint32_t control_point_index) const override
            {
                auto id = m_element->GetIndexArray().GetAt(control_point_index);
                return m_element->GetDirectArray().GetAt(id);
            }

			virtual int32_t get_size() const override
			{
				return m_element->GetIndexArray().GetCount();
			}
        };

        struct get_control_point_index : public get_basic_point_index
        {
            int* m_control_points;

        public:
            get_control_point_index(int* control_points) : m_control_points(control_points)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                return m_control_points[triangle_index * 3 + triangle_vertex];
            }
        };

        struct get_point_index : public get_basic_point_index
        {
            const fbxsdk::FbxMesh* m_mesh;
        public:

            get_point_index(const fbxsdk::FbxMesh* m) : m_mesh(m)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                return 3 * triangle_index + triangle_vertex;
            }
        };


        struct get_uv_element
        {
            virtual fbxsdk::FbxVector2 get_element(uint32_t index) const = 0;
        };

        struct get_uv_element_direct : public get_uv_element
        {
            const fbxsdk::FbxGeometryElementUV* m_uv;

        public:

            get_uv_element_direct(const FbxGeometryElementUV* uv) : m_uv(uv)
            {

            }

            virtual fbxsdk::FbxVector2 get_element(uint32_t control_point_index) const override
            {
                return m_uv->GetDirectArray().GetAt(control_point_index);
            }
        };

        struct get_uv_element_index_to_direct : public get_uv_element
        {
            const fbxsdk::FbxGeometryElementUV* m_uv;

        public:

            get_uv_element_index_to_direct(const FbxGeometryElementUV* uv) : m_uv(uv)
            {

            }

            virtual fbxsdk::FbxVector2 get_element(uint32_t control_point_index) const override
            {
                auto id = m_uv->GetIndexArray().GetAt(control_point_index);
                return m_uv->GetDirectArray().GetAt(id);
            }
        };

        struct get_uv_control_point_index : public get_basic_point_index
        {
            int* m_control_points;

        public:
            get_uv_control_point_index(int* control_points) : m_control_points(control_points)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                assert(triangle_vertex == 0 || triangle_vertex == 1 || triangle_vertex == 2);
                return m_control_points[triangle_index * 3 + triangle_vertex];
            }
        };

        struct get_uv_texture_index : public get_basic_point_index
        {
            const fbxsdk::FbxMesh* m_mesh;
        public:

            get_uv_texture_index(const fbxsdk::FbxMesh* m) : m_mesh(m)
            {

            }

            virtual uint32_t get_element(uint32_t triangle_index, uint32_t triangle_vertex) const override
            {
                auto m = const_cast<fbxsdk::FbxMesh*>(m_mesh);
                return m->GetTextureUVIndex(triangle_index, triangle_vertex);
            }
        };


        std::vector<Point2> get_uvs(const fbxsdk::FbxMesh* mesh)
        {
            auto indices = mesh->GetPolygonVertices();
            auto triangle_count = mesh->GetPolygonCount();
            auto uv = mesh->GetElementUV(0);

            std::vector<Point2>             uvs;

            get_uv_element* get_uv          = nullptr;
            get_uv_element_direct           get_uv_0(uv);
            get_uv_element_index_to_direct  get_uv_1(uv);

            if (uv->GetReferenceMode() == fbxsdk::FbxGeometryElement::eDirect)
            {
                get_uv = &get_uv_0;
            }
            else
            {
                assert(uv->GetReferenceMode() == fbxsdk::FbxGeometryElement::eIndexToDirect);
                get_uv = &get_uv_1;
            }

            get_basic_point_index* gpi = nullptr;
            get_uv_texture_index        get_texture0(mesh);
            get_uv_control_point_index  get_texture1(indices);

            if (uv->GetMappingMode() == fbxsdk::FbxGeometryElement::eByControlPoint)
            {
                gpi = &get_texture1;
            }
            else
            {
                assert(uv->GetMappingMode() == fbxsdk::FbxGeometryElement::eByPolygonVertex);
                gpi = &get_texture0;
                get_uv = &get_uv_0; //todo; check this, produces different uvs? the file data indicates otherwise
            }

            for (auto triangle = 0; triangle < triangle_count; ++triangle)
            {
                //uv
                {
                    auto uvi0 = gpi->get_element(triangle, 0);
                    auto uvi1 = gpi->get_element(triangle, 1);
                    auto uvi2 = gpi->get_element(triangle, 2);

                    double* uv0 = get_uv->get_element(uvi0);
                    double* uv1 = get_uv->get_element(uvi1);
                    double* uv2 = get_uv->get_element(uvi2);

                    Point2 uvp0 = { static_cast<float>(uv0[0]), static_cast<float>(uv0[1]) };
                    Point2 uvp1 = { static_cast<float>(uv1[0]), static_cast<float>(uv1[1]) };
                    Point2 uvp2 = { static_cast<float>(uv2[0]), static_cast<float>(uv2[1]) };

                    //transform to directx
                    uvp0.m_y = 1.0f - uvp0.m_y;
                    uvp1.m_y = 1.0f - uvp1.m_y;
                    uvp2.m_y = 1.0f - uvp2.m_y;

                    uvs.push_back(uvp0);
                    uvs.push_back(uvp1);
                    uvs.push_back(uvp2);
                }
            }

            return uvs;
        }

        template <typename vector_element_type_t, typename return_vectors_type_t>
        void get_vectors_typed(const fbxsdk::FbxMesh* mesh, const vector_element_type_t* vector_element, return_vectors_type_t& vectors)
        {
            auto element       = vector_element;

            get_vector4_element* gv = nullptr;
            get_typed_element_direct<vector_element_type_t>             gv_0(element);
            get_typed_element_index_to_direct<vector_element_type_t>    gv_1(element);

            if (element->GetReferenceMode() == fbxsdk::FbxGeometryElement::eDirect)
            {
                gv = &gv_0;
            }
            else
            {
                assert(element->GetReferenceMode() == fbxsdk::FbxGeometryElement::eIndexToDirect);
                gv = &gv_1;
            }

            auto element_count = gv->get_size();

            for (auto element = 0; element < element_count; ++element)
            {
                using namespace DirectX;
                //vector
                {
                    double* normal0 = gv->get_element(element);

                    using vector_type_t = typename return_vectors_type_t::value_type;

                    vector_type_t normalp0;

                    XMVECTOR  vr0 = XMVectorSet(static_cast<float>(normal0[0]), static_cast<float>(normal0[1]), static_cast<float>(normal0[2]), 0.0f);
                    XMVECTOR  vq0 = vr0;

                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&normalp0), vq0);

                    vectors.push_back(normalp0);
                }
            }
        }

        std::vector<Vector3> transform_normals(const DirectX::XMMATRIX m, const std::vector<Vector3>& s)
        {
            using namespace DirectX;

            std::vector<Vector3> r;
            r.resize(s.size());
            std::transform(s.cbegin(), s.cend(), r.begin(),
                [&m](const Vector3 n)
                {
                    XMVECTOR   r0 = XMLoadFloat3(reinterpret_cast<const XMFLOAT3*>(&n));
                    XMVECTOR   r1 = XMVector3Normalize(XMVector3Transform(r0, m));

                    Vector3 r = {};
                    XMStoreFloat3(reinterpret_cast<XMFLOAT3*>(&r), r1);
                    return r;
                });

            return r;
        }


        std::vector<Vector3> get_normals(const fbxsdk::FbxMesh* mesh)
        {
            using namespace DirectX;
            using namespace fbx;

            std::vector<Vector3>  vectors;
            get_vectors_typed(mesh, mesh->GetElementNormal(0), vectors);

            const auto normal_node_transform = XMMatrixTranspose(XMMatrixInverse(nullptr, world_transform(mesh->GetNode())));
            return transform_normals(normal_node_transform, vectors);
        }

        std::vector<Vector3> get_tangents(const fbxsdk::FbxMesh* mesh)
        {
            using namespace DirectX;
            using namespace fbx;

            std::vector<Vector3>  vectors;

            get_vectors_typed(mesh, mesh->GetElementTangent(0), vectors);

            const auto normal_node_transform = XMMatrixTranspose(XMMatrixInverse(nullptr, world_transform(mesh->GetNode())));
            return transform_normals(normal_node_transform, vectors);
        }


        std::vector<Vector3> get_bitangents(const fbxsdk::FbxMesh* mesh)
        {
            using namespace DirectX;
            using namespace fbx;

            std::vector<Vector3>  vectors;

            get_vectors_typed(mesh, mesh->GetElementBinormal(0), vectors);

            const auto normal_node_transform = XMMatrixTranspose(XMMatrixInverse(nullptr, world_transform(mesh->GetNode())));
            return transform_normals(normal_node_transform, vectors);
        }

        std::vector<Triangle32Bit> get_triangles(const fbxsdk::FbxMesh* mesh)
        {
            /*
            std::vector<Triangle32Bit> r;
            auto indices    = mesh->GetPolygonVertices();
            auto triangles  = mesh->GetPolygonVertexCount() / 3;

            for (auto triangle = 0; triangle < triangles; ++triangle)
            {
                auto triange_to_fetch = triangle;

                uint32_t i0 = indices[triange_to_fetch * 3];
                uint32_t i1 = indices[triange_to_fetch * 3 + 2];
                uint32_t i2 = indices[triange_to_fetch * 3 + 1];    //swap triangle

                r.push_back({ i0,i1,i2 });
            }

            return r;
            */

            /*
            std::vector<Triangle32Bit> r;
            auto indices    = mesh->GetPolygonVertices();
            auto triangles  = mesh->GetPolygonVertexCount() / 3;

            for (auto triangle = 0; triangle < triangles; ++triangle)
            {
                auto triange_to_fetch = triangle;

                uint32_t i0 = indices[triange_to_fetch * 3];
                uint32_t i1 = indices[triange_to_fetch * 3 + 2];
                uint32_t i2 = indices[triange_to_fetch * 3 + 1];    //swap triangle

                r.push_back({ i0,i1,i2 });
            }

            return r;
            */

            std::vector<Triangle32Bit> r;

            auto indices        = mesh->GetPolygonVertices();
            auto indices_size   = mesh->GetPolygonVertexCount();
            auto triangles = indices_size / 3;

            for (auto triangle = 0; triangle < triangles; ++triangle)
            {
                auto triange_to_fetch = triangle;

                uint32_t i0 = triange_to_fetch * 3;
                uint32_t i1 = triange_to_fetch * 3 + 1;
                uint32_t i2 = triange_to_fetch * 3 + 2;    //swap triangle

                r.push_back({ i0,i1,i2 });
            }

            return r;
        }

        std::string normalize_texture_name(const std::string& s)
        {
            std::string r = s;

            if (r == ".")
            {
                r = "Default";
            }

            size_t pos = r.rfind(".");
            if (pos != std::string::npos)
            {
                r = r.substr(0, pos);
            }

            return r;
        }

        void  extract_material(const fbxsdk::FbxMesh* m, Material* material)
        {
            fbxsdk::FbxNode* node                                       = m->GetNode();
            int32_t materialCount                                       = node->GetMaterialCount();
            fbxsdk::FbxSurfaceMaterial* mtl_                            = node->GetMaterial(0);

            const fbxsdk::FbxLayer* lLayer                              = m->GetLayer(0);
            const fbxsdk::FbxLayerElementMaterial* lMaterialElement     = lLayer->GetMaterials();
            const fbxsdk::FbxLayerElement::EMappingMode mapping_mode    = lMaterialElement->GetMappingMode();

            //no materials assigned by polygons for now, 1 mesh, 1 material
            if (mapping_mode != fbxsdk::FbxLayerElement::eAllSame)
            {
                __debugbreak();
            }

            FbxProperty df = mtl_->FindProperty("3dsMax").Find("main").Find("base_color_map"); 
            FbxProperty nm = mtl_->FindProperty("3dsMax").Find("main").Find("norm_map");
            FbxProperty sp = mtl_->FindProperty("3dsMax").Find("main").Find("specular_map");
            FbxProperty gs = mtl_->FindProperty("3dsMax").Find("main").Find("glossiness_map");

            const fbxsdk::FbxFileTexture* dt = static_cast<const fbxsdk::FbxFileTexture* >(df.GetSrcObject<fbxsdk::FbxTexture>(0));
            const fbxsdk::FbxFileTexture* nt = static_cast<const fbxsdk::FbxFileTexture*> (nm.GetSrcObject<fbxsdk::FbxTexture>(0));
            const fbxsdk::FbxFileTexture* st = static_cast<const fbxsdk::FbxFileTexture*> (sp.GetSrcObject<fbxsdk::FbxTexture>(0));
            const fbxsdk::FbxFileTexture* gt = static_cast<const fbxsdk::FbxFileTexture*> (gs.GetSrcObject<fbxsdk::FbxTexture>(0));

            std::string dt_name = std::filesystem::path(dt->GetFileName()).filename().string();
            std::string nt_name = std::filesystem::path(nt->GetFileName()).filename().string();
            std::string st_name = std::filesystem::path(st->GetFileName()).filename().string();
            std::string gt_name = std::filesystem::path(gt->GetFileName()).filename().string();

            material->m_diffuse     = normalize_texture_name(dt_name);
            material->m_normal      = normalize_texture_name(nt_name);
            material->m_specular    = normalize_texture_name(st_name);
            material->m_glossiness  = normalize_texture_name(gt_name);
        }

        void create_static_model(const fbxsdk::FbxMesh* m, StaticMesh* mesh, Material* material, const fbx::fbx_context* context)
        {
            mesh->m_positions       = get_positions(m);
            mesh->m_triangles       = get_triangles(m);

            std::vector<Point2>     uv;
            std::vector<Vector3>    normal;

            uv.resize(mesh->m_positions.size());
            normal.resize(mesh->m_positions.size());

            uint32_t i = 0;
            for (auto t : mesh->m_triangles)
            {
                bool unmapped;
                FbxVector2 v0;
                FbxVector2 v1;
                FbxVector2 v2;

                m->GetPolygonVertexUV(i, 0, "UVChannel_1", v0, unmapped);
				m->GetPolygonVertexUV(i, 1, "UVChannel_1", v1, unmapped);
				m->GetPolygonVertexUV(i, 2, "UVChannel_1", v2, unmapped);

                auto tx = m->GetPolygonSize(i);

                Point2     uv0 = { static_cast<float>(v0[0]), static_cast<float>(v0[1]) };
                Point2     uv1 = { static_cast<float>(v1[0]), static_cast<float>(v1[1]) };
                Point2     uv2 = { static_cast<float>(v2[0]), static_cast<float>(v2[1]) };

                uv0.m_y = 1.0f - uv0.m_y;
                uv1.m_y = 1.0f - uv1.m_y;
                uv2.m_y = 1.0f - uv2.m_y;

                uv[t.m_i0] = uv0;
                uv[t.m_i1] = uv1;
                uv[t.m_i2] = uv2;
            
                i++;
            }

            mesh->m_uv              = std::move(uv);
            mesh->m_normals         = get_normals(m);
            mesh->m_tangents        = get_tangents(m);
            mesh->m_bitangents      = get_bitangents(m);

            //swap triangles
            for (auto& t : mesh->m_triangles)
            {
                auto tx = t.m_i0;
                t.m_i0  = t.m_i1;
                t.m_i1  = tx;
            }

            //re-index, merge attributes
            
            /*
            std::vector<uint32_t> position_hash(mesh->m_uv.size());
            std::set<uint32_t>    unique_positions;

            for (auto i = 0U; i < position_hash.size(); ++i)
            {
                position_hash[i] = murmur3_32(reinterpret_cast<const uint8_t*>(&mesh->m_uv[i]), sizeof(Point2), 5);
            }

            for (auto h : position_hash)
            {
                unique_positions.insert(h);
            }
            */

            extract_material(m, material);
        }
    }

    namespace
    {
        bool operator ==(const Material& a, const Material& b)
        {
            return a.m_diffuse == b.m_diffuse && a.m_glossiness == b.m_glossiness && a.m_normal == b.m_normal && a.m_specular == b.m_specular;
        }
        struct sorter_material
        {
            bool operator()(const MaterialMeshTable& a, const MaterialMeshTable& b)
            {
                return a.m_material < b.m_material;
            }
        };

        struct sorter_mesh
        {
            bool operator()(const MeshMaterialTable & a, const MeshMaterialTable & b)
            {
                return a.m_mesh < b.m_mesh;
            }
        };


        void bake_transforms(fbxsdk::FbxNode* n)
        {
            fbx::bake_node_transforms(n);

            for (auto i = 0; i < n->GetChildCount(); ++i)
            {
                bake_transforms(n->GetChild(i));
            }
        }
    }

    DLL_API  ApiResult ImportFbxStaticModel(const char* file_name, StaticModel*& model)
    {
        std::unique_ptr<StaticModel> m = std::make_unique<StaticModel>();
        model = m.release();

        using namespace fbx;

        auto context = load_fbx_file(file_name);
        auto scene = context->m_scene.get();

        bake_transforms(scene->GetRootNode());

        std::vector<fbxsdk::FbxMesh*> meshes;
        meshes = get_meshes(scene->GetRootNode(), meshes);

        for (auto& m : meshes)
        {
            //m->RemoveBadPolygons();
            //m->ComputeBBox();

            if (!has_normals(m))
            {
                //m->GenerateNormals();
            }

            if (!has_tangents(m))
            {
                //m->GenerateTangentsDataForAllUVSets(true);
            }
        }

        for (auto& m : meshes)
        {
            if (has_triangles(m) )
            {
                auto mesh        = std::make_unique<StaticMesh>();
                auto material    = std::make_unique<Material>();

                create_static_model(m, mesh.get(), material.get(), context.get());

                model->m_mesh.push_back(std::move(mesh));

                size_t  mesh_index  = model->m_mesh.size() - 1;
                size_t  mat_index   = -1;

                for (auto i = 0; i < model->m_material.size(); ++i)
                {
                    if (*model->m_material[i] == *material)
                    {
                        mat_index = i;
                        break;
                    }
                }

                if (mat_index == -1)
                {
                    model->m_material.push_back(std::move(material));
                    mat_index = (model->m_material.size() - 1);
                }

                model->m_material_mesh_table.push_back({ (uint16_t)mat_index,  (uint16_t)mesh_index });
                model->m_mesh_material_table.push_back({ (uint16_t)mesh_index, (uint16_t)mat_index });
            }
        }

        std::sort(model->m_material_mesh_table.begin(), model->m_material_mesh_table.end(), sorter_material());
        std::sort(model->m_mesh_material_table.begin(), model->m_mesh_material_table.end(), sorter_mesh());

        //compute bounds
        for (auto&& m : model->m_mesh)
        {
            constexpr float plus_inf  =  std::numeric_limits<float>::infinity();
            
            Point3 min_aabb = {  plus_inf, plus_inf, plus_inf };
            Point3 max_aabb = { -plus_inf, -plus_inf, -plus_inf };

            for (auto p : m->m_positions)
            {
                min_aabb.m_x = std::min<float>(p.m_x, min_aabb.m_x);
                min_aabb.m_y = std::min<float>(p.m_y, min_aabb.m_y);
                min_aabb.m_z = std::min<float>(p.m_z, min_aabb.m_z);

                max_aabb.m_x = std::max<float>(p.m_x, min_aabb.m_x);
                max_aabb.m_y = std::max<float>(p.m_y, min_aabb.m_y);
                max_aabb.m_z = std::max<float>(p.m_z, min_aabb.m_z);
            }

            StaticMeshBounds b = {};
            b.m_aabb_min   = min_aabb;
            b.m_aabb_max   = max_aabb;
            model->m_mesh_bounds.push_back(b);
        }

        return ApiResult::Success;
    }
}




