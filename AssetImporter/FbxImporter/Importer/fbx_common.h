#pragma once

#include <fbxsdk.h>
#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include <set>

#include <DirectXMath.h>

namespace Importer
{

    namespace fbx
    {
        using namespace DirectX;

        class exception : std::exception
        {
            using base = std::exception;

        public:
            exception(const char* message) : base(message)
            {

            }
        };

        void throw_if_faled(FbxStatus status);

        template <typename t>
        struct fbxsdk_object_deleter
        {
            inline void operator()(t* m) const
            {
                m->Destroy();
            }
        };

        using fbxmanager_deleter = fbxsdk_object_deleter < FbxManager >;
        using fbxscene_deleter = fbxsdk_object_deleter < FbxScene >;
        using fbximporter_deleter = fbxsdk_object_deleter < FbxImporter >;

        /// Do this setup for each node(FbxNode).
        // We set up what we want to bake via ConvertPivotAnimationRecursive.
        // When the destination is set to 0, baking will occur.
        // When the destination value is set to the source�s value, the source values will be retained and not baked.
        void bake_node_transforms(fbxsdk::FbxNode* node);

        template <typename f> inline void transform_node_recursive(const fbxsdk::FbxNode* root, f op)
        {
            if (root)
            {
                op(root);
                auto children = root->GetChildCount();

                for (auto i = 0; i < children; ++i)
                {
                    auto child = root->GetChild(i);
                    transform_node_recursive(child, op);
                }
            }
        }

        XMMATRIX world_transform(fbxsdk::FbxNode* node);

        std::tuple<fbxsdk::FbxMesh*, fbxsdk::FbxNode*> get_mesh(fbxsdk::FbxNode* node);
        std::vector<fbxsdk::FbxMesh*> get_meshes(fbxsdk::FbxNode* node, std::vector<fbxsdk::FbxMesh*>& meshes);

        struct fbx_context
        {
            std::unique_ptr<fbxsdk::FbxManager, fbxmanager_deleter>     m_manager;
            std::unique_ptr<fbxsdk::FbxScene, fbxscene_deleter>         m_scene;
            std::unique_ptr<fbxsdk::FbxImporter, fbximporter_deleter>   m_importer;
            bool                                                        m_coordinate_system_swap_y_z;
            bool                                                        m_invert_handness;
        };

        std::unique_ptr<fbx_context> load_fbx_file(const std::string& file_name);
    }
}
