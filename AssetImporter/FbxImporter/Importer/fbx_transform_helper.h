#pragma once

#include <tuple>
#include <array>
#include "fbx_common.h"
#include "fbx_transform.h"

namespace Importer
{
        namespace fbx
        {
            using namespace DirectX;

            inline DirectX::XMMATRIX transform_from_dcc(DirectX::XMMATRIX m, const bool swap_y_z_ = true)
            {
                if (swap_y_z_)
                {
                    auto m0 = swap_y_z();
                    auto m1 = negate_x();
                    auto m2 = negate_y();
                    auto m3 = negate_z();
                    auto m4 = DirectX::XMMatrixIdentity();

                    return XMMatrixMultiply(XMMatrixMultiply(XMMatrixMultiply(m, m3), m2), m1);
                }
                else
                {
                    auto m0 = swap_y_z();
                    auto m1 = negate_x();
                    auto m2 = negate_y();
                    auto m3 = negate_z();
                    auto m4 = DirectX::XMMatrixIdentity();
                    return XMMatrixMultiply(XMMatrixMultiply(XMMatrixMultiply(m, m0), m3), m4);
                }
            }

            inline DirectX::XMVECTOR transform_vector_from_dcc(DirectX::XMVECTOR m, const bool swap_y_z_ = true)
            {
                if (swap_y_z_)
                {
                    auto m0 = swap_y_z();
                    auto m1 = negate_x();
                    auto m2 = negate_y();
                    auto m3 = negate_z();
                    auto m4 = DirectX::XMMatrixIdentity();
                    return transform_vector(transform_vector(transform_vector(m, m3), m2), m1);
                }
                else
                {
                    auto m0 = swap_y_z();
                    auto m1 = negate_x();
                    auto m2 = negate_y();
                    auto m3 = negate_z();
                    auto m4 = DirectX::XMMatrixIdentity();
                    return transform_vector(transform_vector(transform_vector(m, m0), m3), m4);
                }
            }

            inline std::array<int32_t, 3> triangle_permutaion(const bool swap_y_z_ = true)
            {
                if (swap_y_z_)
                {
                    std::array<int32_t, 3> r = { 0, 1, 2 };
                    return r;
                }
                else
                {
                    std::array<int32_t, 3> r = { 0, 2, 1 };
                    return r;
                }
            }

        }
}


