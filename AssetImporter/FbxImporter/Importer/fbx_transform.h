#pragma once

#include <limits>
#include <DirectXMath.h>
#include <fbxsdk.h>

namespace Importer
{
    namespace fbx
    {
        /*
        inline fbxsdk::FbxMatrix negate_y_fbx()
        {
            fbxsdk::FbxMatrix m;

            m.SetColumn(0, fbxsdk::FbxVector4(1, 0, 0, 0));
            m.SetColumn(1, fbxsdk::FbxVector4(0,1, 0, 0));
            m.SetColumn(2, fbxsdk::FbxVector4(0, 0, -1, 0));
            m.SetColumn(3, fbxsdk::FbxVector4(0, 0, 0, 1));

            return m;
        }

        inline math::float4x4 negate_y_math()
        {
            math::float4x4 m;

            m.r[0] = math::identity_r0();
            m.r[1] = math::identity_r1();
            m.r[2] = math::negate(math::identity_r2());
            m.r[3] = math::identity_r3();

            return m;
        }

        inline fbxsdk::FbxMatrix negate_x_fbx()
        {
            fbxsdk::FbxMatrix m;

            m.SetColumn(0, fbxsdk::FbxVector4(-1, 0, 0, 0));
            m.SetColumn(1, fbxsdk::FbxVector4(0, 1, 0, 0));
            m.SetColumn(2, fbxsdk::FbxVector4(0, 0, 1, 0));
            m.SetColumn(3, fbxsdk::FbxVector4(0, 0, 0, 1));

            return m;
        }

        inline math::float4x4 negate_x_math()
        {
            math::float4x4 m;

            m.r[0] = math::negate(math::identity_r0());
            m.r[1] = math::identity_r1();
            m.r[2] = math::identity_r2();
            m.r[3] = math::identity_r3();

            return m;
        }

        inline fbxsdk::FbxMatrix swap_y_z_fbx()
        {
            fbxsdk::FbxMatrix m;

            m.SetColumn(0, fbxsdk::FbxVector4(1, 0, 0, 0));
            m.SetColumn(1, fbxsdk::FbxVector4(0, 1, 0, 0));
            m.SetColumn(2, fbxsdk::FbxVector4(0, 0, 1, 0));
            m.SetColumn(3, fbxsdk::FbxVector4(0, 0, 0, 1));

            return m;
        }

        inline math::float4x4 swap_y_z_math()
        {
            math::float4x4 m;

            m.r[0] = math::identity_r0();
            m.r[1] = math::identity_r1();
            m.r[2] = math::identity_r2();
            m.r[3] = math::identity_r3();

            return m;
        }


        inline fbxsdk::FbxVector4 swap_y_z_vector(fbxsdk::FbxVector4 v)
        {
            auto swap = swap_y_z_fbx();
            auto t0 = swap.MultNormalize(v);
            return t0;
        }

        inline fbxsdk::FbxVector4 swap_y_z_point(fbxsdk::FbxVector4 v)
        {
            auto swap = swap_y_z_fbx();
            auto t0 = swap.MultNormalize(v);
            return t0;
        }

        inline fbxsdk::FbxAMatrix swap_y_z_matrix( const fbxsdk::FbxAMatrix v )
        {
            fbxsdk::FbxAMatrix  m  = v;
            fbxsdk::FbxVector4  tr = v.GetT();

            m.SetT(fbxsdk::FbxVector4(0.0, 0.0, 0.0, 1.f));
            fbxsdk::FbxMatrix   rot = m;

            auto swap  = swap_y_z_fbx();
            auto neg_y = negate_y_fbx();

            auto t0 = swap.MultNormalize(tr);
            auto t1 = neg_y.MultNormalize(t0);

            auto r0 = swap.Transpose()   * rot * swap;
            auto r1 = neg_y.Transpose()  * r0  * neg_y;

            fbxsdk::FbxAMatrix r;
            auto dst = static_cast<double*>(r);
            auto src = static_cast<double*>(r1);

            std::memcpy(dst, src, sizeof(fbxsdk::FbxAMatrix));

            r.SetT(t1);

            return r;
        }

        /*
        inline math::float4 swap_y_z_vector(math::afloat4 v)
        {
            auto swap   = swap_y_z_math();
            auto t0     = math::mul(v,swap);
            return t0;
        }


        inline math::float4 swap_y_z_point(math::afloat4 v)
        {
            auto swap   = swap_y_z_math();
            auto neg_y  = negate_y_math();
            auto t0     = math::mul(v, swap);
            auto t1     = math::mul(t0, neg_y);
            return      t1;
        }

        inline math::float4x4 swap_y_z_matrix(math::afloat4x4 v)
        {
            auto m  = v;
            auto tr = v.r[3];

            m.r[3] = math::identity_r3();

            auto   rot = v;

            auto swap  = swap_y_z_math();
            auto neg_y = negate_y_math();

            auto t0 = math::mul(tr, swap);
            auto t1 = t0;// neg_y.MultNormalize(t0);

            auto r0 = math::mul( math::mul( math::transpose(swap), rot ), swap);
            auto r1 = r0;// neg_y.Transpose()  * rot * neg_y;

            auto r = r0;
            r.r[3] = t1;

            return r;
        }
        */

        inline double flush_denormalized_double_to_zero(double d)
        {
            if (std::abs(d) < std::numeric_limits<float>::min())
            {
                return 0.0f;
            }
            else
            {
                return d;
            }
        }

        inline DirectX::XMFLOAT4 to_float4(fbxsdk::FbxVector4 v)
        {
            DirectX::XMFLOAT4 r;
            const double* d = v;
            r.x = static_cast<float>(flush_denormalized_double_to_zero(d[0]));
            r.y = static_cast<float>(flush_denormalized_double_to_zero(d[1]));
            r.z = static_cast<float>(flush_denormalized_double_to_zero(d[2]));
            r.w = static_cast<float>(flush_denormalized_double_to_zero(d[3]));
            return r;
        }

        inline DirectX::XMVECTOR to_float4(fbxsdk::FbxQuaternion v)
        {
            float r[4];
            const double* d = v;
            r[0] = static_cast<float>(flush_denormalized_double_to_zero(d[0]));
            r[1] = static_cast<float>(flush_denormalized_double_to_zero(d[1]));
            r[2] = static_cast<float>(flush_denormalized_double_to_zero(d[2]));
            r[3] = static_cast<float>(flush_denormalized_double_to_zero(d[3]));
            return DirectX::XMLoadFloat4(reinterpret_cast<const DirectX::XMFLOAT4*>(&r[0]));
        }

        inline DirectX::XMMATRIX to_float4x4(const fbxsdk::FbxAMatrix v)
        {
            DirectX::XMFLOAT4 r[4];

            r[0] = to_float4(v.GetRow(0));
            r[1] = to_float4(v.GetRow(1));
            r[2] = to_float4(v.GetRow(2));
            r[3] = to_float4(v.GetRow(3));
            return DirectX::XMLoadFloat4x4(reinterpret_cast<const DirectX::XMFLOAT4X4*>(&r[0]));
        }

        inline DirectX::XMMATRIX negate_z()
        {
            DirectX::XMVECTORF32 m[4];

            m[0] = DirectX::g_XMIdentityR0;
            m[1] = DirectX::g_XMIdentityR1;
            m[2] = DirectX::g_XMNegIdentityR2;
            m[3] = DirectX::g_XMIdentityR3;

            return DirectX::XMLoadFloat4x4A(reinterpret_cast<const DirectX::XMFLOAT4X4A*>(&m[0]));
        }

        inline DirectX::XMMATRIX negate_y()
        {
            DirectX::XMVECTORF32 m[4];

            m[0] = DirectX::g_XMIdentityR0;
            m[1] = DirectX::g_XMNegIdentityR1;
            m[2] = DirectX::g_XMIdentityR2;
            m[3] = DirectX::g_XMIdentityR3;

            return DirectX::XMLoadFloat4x4A(reinterpret_cast<const DirectX::XMFLOAT4X4A*>(&m[0]));
        }

        inline DirectX::XMMATRIX negate_x()
        {
            DirectX::XMVECTORF32 m[4];

            m[0] = DirectX::g_XMNegIdentityR0;
            m[1] = DirectX::g_XMIdentityR1;
            m[2] = DirectX::g_XMIdentityR2;
            m[3] = DirectX::g_XMIdentityR3;

            return DirectX::XMLoadFloat4x4A(reinterpret_cast<const DirectX::XMFLOAT4X4A*>(&m[0]));
        }

        inline DirectX::XMMATRIX swap_y_z()
        {
            DirectX::XMVECTORF32 m[4];

            m[0] = DirectX::g_XMIdentityR0;
            m[1] = DirectX::g_XMIdentityR2;
            m[2] = DirectX::g_XMIdentityR1;
            m[3] = DirectX::g_XMIdentityR3;

            return DirectX::XMLoadFloat4x4A(reinterpret_cast<const DirectX::XMFLOAT4X4A*>(&m[0]));
        }

        inline DirectX::XMMATRIX transform_rotation(DirectX::XMMATRIX rot, DirectX::XMMATRIX t)
        {
            return DirectX::XMMatrixMultiply(t, XMMatrixMultiply(rot, XMMatrixTranspose(t)));
        }

        inline DirectX::XMVECTOR transform_vector(DirectX::XMVECTOR v, DirectX::XMMATRIX t)
        {
            return DirectX::XMVector3Transform(v, t);
        }

        inline DirectX::XMVECTOR transform_point(DirectX::XMVECTOR v, DirectX::XMMATRIX t)
        {
            return DirectX::XMVector3Transform(v, t);
        }

        /*
        inline DirectX::XMMATRIX transform_transform(DirectX::XMMATRIX r, DirectX::XMMATRIX transfo)
        {
            auto rot    = DirectX::Rot::rotation(r);
            auto trans  = math::translation(r);

            auto r0     = DirectX::XMTransformtransform_rotation(rot, transfo);
            auto t0     = transform_vector(trans, transfo);

            auto res    = r0;
            res.r[3]    = math::select(t0, math::identity_r3(), math::mask_w());
            return res;
        }
        */
    }
}


